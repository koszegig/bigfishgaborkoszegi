<?php

namespace Tests\Feature;


use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;


/**
 * Class QueryTest
 * @package Tests\Feature
 */
class QueryTest  extends TestCase
{
    /**
     * @var string
     */
    public $graphQLEndpoint = 'graphql';

    /**
     *Felhasználók listája:
     */
    public function testUsersQuery()
    {


        $response =  $this->query('users', ['limit' => 10,'page'=>1], ['name',
                    'phoneNumber',
                    'email',
                    'dateOfBirth',
                    'isActive']);
        $this->assertArrayHasKey('users', $response['data']);
        $this->validateResponse($response['data']['users']);
    }

    /**
     *Felhasználók listája lapozás és limit paraméter és rendezés név szerint növekvő:
     */
    public function testUsersOrderAscQuery()
    {
        $response =  $this->query('users', ['limit' => 10,'page'=>1,'orderBy'=>'name asc'], ['name',
            'phoneNumber',
            'email',
            'dateOfBirth',
            'isActive']);
        $this->assertArrayHasKey('users', $response['data']);
        $this->validateResponse($response['data']['users']);

    }

    /**
     *Felhasználók listája lapozás és limit paraméter és rendezés név szerint csökkenő
     */
    public function testUsersOrderDescQuery()
    {
        $response =  $this->query('users', ['limit' => 10,'page'=>1,'orderBy'=>'name desc'], ['name',
            'phoneNumber',
            'email',
            'dateOfBirth',
            'isActive']);
        $this->assertArrayHasKey('users', $response['data']);
        $this->validateResponse($response['data']['users']);

    }

    /**
     *Aktív Felhasználók listája lapozás és limit paraméter:
     */
    public function testUserQuery(){
        $response =  $this->query('user', ['limit' => 10,'page'=>1,'isActive'=>true], ['name',
            'phoneNumber',
            'email',
            'dateOfBirth',
            'isActive']);
        $this->assertArrayHasKey('user', $response['data']);
        $this->validateResponse($response['data']['user']);
    }

    /**
     * Egy Felhasználók listája:
     */
    public function testNameUserQuery(){
        $response =  $this->query('user', ['isActive'=>true], ['name',
            'phoneNumber',
            'email',
            'dateOfBirth',
            'isActive']);
        $datas = $response['data'];

        $response =  $this->query('user', ['limit' => 1,'page'=>1,
                                                'name'=>$datas['user'][0]['name'],
                                                'phoneNumber'=>$datas['user'][0]['phoneNumber'],
                                                'email'=>$datas['user'][0]['email'],
                                                'dateOfBirth'=>$datas['user'][0]['dateOfBirth']
        ],
            ['name',
            'phoneNumber',
            'email',
            'dateOfBirth',
            'isActive']);
        $this->assertArrayHasKey('user', $response['data']);
        $this->validateResponse($response['data']['user']);
    }



    /**
     * @param $datas
     */
    public function validateResponse($datas)
    {
        foreach ($datas as $data) {
            $this->assertArrayHasKey('name', $data);
            $this->assertArrayHasKey('phoneNumber', $data);
            $this->assertArrayHasKey('email', $data);
            $this->assertArrayHasKey('dateOfBirth', $data);
            $this->assertArrayHasKey('isActive', $data);
        }
    }
}
