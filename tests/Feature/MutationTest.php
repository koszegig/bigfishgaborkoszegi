<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;


/**
 * Class MutationTest
 * @package Tests\Feature
 */
class MutationTest extends TestCase
{
    /**
     * @var string
     */
    public $graphQLEndpoint = 'graphql';
    /**
     *Új felhasználó
     */
    public function testCreateUser()
    {
        $response = $this->mutation('createUser', ['name'=>'John Doeee book',
                                                'email'=>'johndoe@test.com',
                                                'dateOfBirth'=>'2021.01.02',
                                                'isActive'=>true,
                                                'phoneNumber'=>'+36525825',
        ],['name','email','dateOfBirth','isActive'],
                                            );
        $response
            ->assertStatus(200);
        $this->validateResponse($response['data']['createUser']);

    }

    /**
     *Felhasználó módosítása
     */
    public function testUpdateUser()
    {
        $response =  $this->query('user', ['limit' => 1,'page'=>1,'isActive'=>true], ['name',
            'id',
            'phoneNumber',
            'email',
            'dateOfBirth',
            'isActive']);
        $datas = $response['data'];
        $response = $this->mutation('updateUser', ['id'=>$datas['user'][0]['id'],
            'name'=>'John Doeee book',
            'email'=>'johndoe@test.com',
            'dateOfBirth'=>'2021.01.02',
            'isActive'=>true,
            'phoneNumber'=>'+36525825',
        ],['name','email','dateOfBirth','isActive'],
        );
        $response
            ->assertStatus(200);
        $this->validateResponse($response['data']['updateUser']);

    }

    /**
     *Felhasználó törlése
     */
    public function testDeleteUser()
    {
        $response =  $this->query('user', ['limit' => 1,'page'=>1,'isActive'=>true], ['name',
            'id']);
        $datas = $response['data'];
        $response = $this->mutation('delete', ['id'=>$datas['user'][0]['id']        ],[],
        );
        $response
            ->assertStatus(200);

    }

    /**
     *Felhasználó telefon törlésetörlése
     */
    public function testDeleteUserPhone()
    {
        $response =  $this->query('user', ['limit' => 1,'page'=>1,'isActive'=>true], ['name',
            'name',
            'phoneNumber']);
        $datas = $response['data'];
        $response = $this->mutation('deleteUserphone', ['name'=>$datas['user'][0]['name'],'phoneNumber'=>$datas['user'][0]['phoneNumber']         ],[],
        );
        $response
            ->assertStatus(200);

    }

    /**
     * @param $datas
     */
    public function validateResponse($datas)
    {
        foreach ($datas as $data) {
            $this->assertArrayHasKey('name', $data);
            $this->assertArrayHasKey('email', $data);
            $this->assertArrayHasKey('dateOfBirth', $data);
            $this->assertArrayHasKey('isActive', $data);
        }
    }
}
