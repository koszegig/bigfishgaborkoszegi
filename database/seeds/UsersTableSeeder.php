<?php

use App\Models\Tables\Users;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('hu_HU');
        for ($i = 0; $i <= 99; $i++) {
            $gender = $faker->randomElement(['male', 'female']);
            $_user =['name' =>  $faker->firstName($gender).' '.$faker->lastName($gender),
                     'email' => $faker->unique()->safeEmail,
                    'dateOfBirth' => $faker->dateTimeBetween('-10 years', '+0 days')
            ];
            $user = Users::create($_user);
            for ($j = 0; $j <= 5; $j++) {
                $user->addPhone(['phoneNumber' => $faker->phoneNumber]);
            }
        }

    }


}
