<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Tables\User::class, function (Faker $faker) {
    static $username;
    static $firstname;
    static $lastname;
    static $password;
    static $system;

    $_firstname = $faker->firstName;
    $_lastname = $faker->lastName;

    return [
        'firstname' => $firstname ? $firstname : $_firstname,
        'lastname' => $lastname ? $lastname : $_lastname,
        'username' => $username ? $username : strtolower(\MyString::changeVowelToShort($_firstname))."_".strtolower(\MyString::changeVowelToShort($_lastname))."_".\MyHash::genRequestID(),
        'password' => $password ? $password : bcrypt('secret'),
        'system' => $system ? $system : false,
        'remember_token' => str_random(10),
    ];
});
