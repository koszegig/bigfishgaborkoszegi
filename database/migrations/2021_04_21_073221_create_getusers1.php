<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGetusers1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		  DB::unprepared('DROP TYPE IF EXISTS "01_sys".t_users_1 CASCADE;');
          DB::unprepared('
          CREATE TYPE "01_sys".t_users_1 as (
				  id BIGINT,
				  name VARCHAR(255),
				  email VARCHAR(255),
				   "phoneNumber" VARCHAR(255),
				  "dateOfBirth" DATE,
				  "isActive" BOOLEAN,
				  "createdAt" TIMESTAMP(0),
				  "updatedAt" TIMESTAMP(0)
				)
        ');
		DB::unprepared(" CREATE OR REPLACE FUNCTION \"01_sys\".get_users_1 (
				)
				RETURNS SETOF \"01_sys\".t_users_1 AS
				\$body\$
					 select
							usr.id,
							usr.name,
							usr.email,
							phn.\"phoneNumber\",
							usr.\"dateOfBirth\",
							usr.\"isActive\",
							usr.\"createdAt\",
							usr.\"updatedAt\"
					  from  \"01_sys\".users as usr
					  left join \"01_sys\".userphones as phn on (phn.user_id=usr.id)
				\$body\$
				LANGUAGE 'sql'
				VOLATILE
				CALLED ON NULL INPUT
				SECURITY INVOKER
				PARALLEL UNSAFE
				COST 100 ROWS 1000; ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         DB::unprepared('DROP TYPE IF EXISTS "01_sys".t_users_1 CASCADE;');
    }
}
