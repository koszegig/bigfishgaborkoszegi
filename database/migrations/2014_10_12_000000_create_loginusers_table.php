<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoginUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaName ='01_sys';
        DB::unprepared('
            CREATE SCHEMA IF NOT EXISTS "'.$schemaName.'" ;
        ');
        DB::unprepared('DROP TABLE IF EXISTS "01_sys".'.env('DB_PREFIX','bigfish_').'loginusers CASCADE;');
        Schema::connection('pgsql_01_sys')->create('loginusers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username')->unique()->nullable();
            $table->string('password')->nullable();
            $table->boolean('active')->default(true);
            $table->boolean('system')->default(false);
            $table->timestamp('createdAt');
            $table->timestamp('updatedAt');
            $table->rememberToken();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaName ='01_sys';
        //Schema::dropIfExists('users');
        DB::unprepared('DROP TABLE IF EXISTS "01_sys".'.env('DB_PREFIX','bigfish_').'loginusers CASCADE;');
    }
}
