<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserphonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::unprepared('DROP TABLE IF EXISTS "01_sys".'.env('DB_PREFIX','bigfish_').'userphones CASCADE;');
        Schema::connection('pgsql_01_sys')->create('userphones', function (Blueprint $table) {
            $table->bigInteger('user_id');
            $table->string('phoneNumber');
        });
        DB::unprepared('
          ALTER TABLE "01_sys".'.env('DB_PREFIX','bigfish_').'userphones
            ADD CONSTRAINT userphones_foreign_foreign FOREIGN KEY (user_id)
                REFERENCES "01_sys".'.env('DB_PREFIX','bigfish_').'users(id)
                ON DELETE CASCADE
                ON UPDATE CASCADE
                NOT DEFERRABLE;
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TABLE IF EXISTS "01_sys".'.env('DB_PREFIX','bigfish_').'userphones CASCADE;');
    }
}
