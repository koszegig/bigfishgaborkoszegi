<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIsvalidpassword extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::unprepared('
					CREATE OR REPLACE FUNCTION "01_sys".get_passwordcrypt (
                      password varchar(254)
                    )
                    RETURNS varchar(254) AS
                    $body$
                    DECLARE
                      p_password alias for $1;
                       v_result varchar(254);
                    BEGIN
                      CREATE EXTENSION IF NOT EXISTS pgcrypto;
                      SELECT
                      public.crypt(p_password::text,'." '$2y$10".'$NpiXdWrUm8g5JvR8r1tNy.a9BWrjGBuPyML/NhIy56nlV9ChkbSv2'."'".'::text ) into v_result;
                    return v_result;
                    END;
                    $body$
                    LANGUAGE'." 'plpgsql'".'
                    VOLATILE
                    CALLED ON NULL INPUT
                    SECURITY INVOKER
                    PARALLEL UNSAFE
                    COST 100;

		 ');
        DB::unprepared('
						CREATE OR REPLACE FUNCTION "01_sys".isvalidpassword (
				  username varchar(254),
				  password varchar(254)
				)
				RETURNS boolean AS
				$body$
				DECLARE
				  p_username alias for $1;
				  p_password alias for $2;
				   v_result boolean;
				  p_passwordhash varchar(254);
				   v_id uuid;
				BEGIN
				  v_result = false;
				  select
				   *from
				"01_sys".get_passwordcrypt (
				  p_password::varchar(254)
				) into
					 p_passwordhash;
				  select
				   usr.id
				   from
				   "01_sys".loginusers usr
					where
					 usr.username::varchar(254)=p_username
					 and
					 usr.password::varchar(254)=p_passwordhash
					 limit 1
					  into v_id;
					if  v_id is not null then
					  v_result = true;
					end if;

				return v_result;
				END;
				$body$
				LANGUAGE'." 'plpgsql'".'
				VOLATILE
				CALLED ON NULL INPUT
				SECURITY INVOKER
				PARALLEL UNSAFE
				COST 100;
		 ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
