<?php

namespace App\Helpers;

use App\ModOpt;

/**
 * Class ModOptHelpers
 * @package App\Helpers
 */
class ModOptHelpers {

      /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function showModelField($model,$field)
    {
        return ModOpt::listModelField($model,$field);
    }

}
