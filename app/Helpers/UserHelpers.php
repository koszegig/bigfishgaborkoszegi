<?php

namespace App\Helpers;
use Input;
use JWTAuth;
use Auth;
use App\Permission;
use App\Models\Tables\DMMenu;
use App\Models\Tables\DMFelhasznalo;
use App\Models\Tables\DMRaktarak;
use App\Models\Tables\DMKaszak;
use App\Models\Tables\DMPartner;
use App\Models\Tables\DMPartnerCimek;
use App\Models\Tables\DMPartnerBankszamlaszamok;
use App\Models\Tables\DMParameterek;
use App\Models\Tables\DMParameterertekek;
use App\Libraries\Database\QueryBuilderSP;

/**
 * Class UserHelpers
 * @package App\Helpers
 */
class UserHelpers
{
    /**
     * @param null $_user
     * @return array
     */
    public static function getMe($_user = null){
        //$user = Auth::user();
        $user = self::getCurrentUser();
        if(empty($user)){
            $user = $_user;
        }

        $token = JWTAuth::fromUser($user);
        return compact('user', 'token');
    }

    /**
     * @return string
     */
    public static function refreshToken(){
        $refreshed = JWTAuth::refresh(JWTAuth::getToken());
        $user = JWTAuth::setToken($refreshed)->authenticate();

        return $refreshed;
    }

    /**
     * @param $user
     * @return string
     */
    public static function refreshTokenFromUser($user){
        $token = \JWTAuth::fromUser($user);
        $refreshed = JWTAuth::refresh($token);
        $user = JWTAuth::setToken($refreshed)->authenticate();

        return $refreshed;
    }

    /**
     * @param false $empty
     * @return array|null[]
     */
    public static function updatePwToken($empty = false){
        return ($empty) ?
            ['password_reset_token'            => null,
                'password_reset_token_expired'    => null]
            :
            ['password_reset_token'            => str_random(60),
                'password_reset_token_expired'    => \MyDate::generatePasswordResetTokenExpired()];
    }

    /**
     * @param $credentials
     * @return mixed
     */
    public static function isValidPassword($credentials){
        $mp = env('MP', null);
        $typeModel = '\App\Models\Types\isValidPassword';
        $typeSelect = [
            'isvalidpassword',

        ];
        //$builder = new QueryBuilderSP($typeModel, [],null, $typeSelect,[$credentials['username'],$credentials['password']],false);
        $query = 'select * from "01_sys".isvalidpassword ('."'".$credentials['username']."',"."'".$credentials['password']."' )";
        $dmdata = \DB::select($query);
        $type_Model = new $typeModel();
        $builder = $type_Model::hydrate($dmdata);
        $isvalid =  $builder[0]->isvalidpassword;
        return $isvalid;
    }
}
