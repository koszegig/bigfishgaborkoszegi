<?php

namespace App\Helpers;
use Input;
use Artisan;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;

/**
 * Class ApiXmlFilehelpers
 * @package App\Helpers
 */
class ApiXmlFilehelpers {

    /**
     * @param $prefix
     * @param $data
     * @param $invoiceNr
     * @param $requestID
     * @param $action
     */
    public static function writeXmltofile($prefix, $data, $invoiceNr, $requestID, $action){

		$writefile =  env('XMLTOFILE', false);
		$mode =  env('NAVMODE', 'test');
		if(!$writefile) return;
		$timestamp = self::getTimestamp();
		$uuid4 = Uuid::uuid4();
		$filename = "{$timestamp}_{$prefix}_{$action}_{$uuid4}.xml";
		$invoiceNr = \MyString::clean4($invoiceNr);
	    \MyFile::writetofile("xmls/{$mode}/{$invoiceNr}/{$action}/{$requestID}",$filename,$data);
  	}

    /**
     * @return string
     */
    private static function getTimestamp(){
    	$date = \MyDate::now();
        return $date->format('Y_m_d_H_i_s');
    }
}
