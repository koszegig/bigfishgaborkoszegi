<?php

namespace App\Helpers;

/**
 * Class ArrayHelpers
 * @package App\Helpers
 */
class ArrayHelpers
{

    /**
     * @param $array
     */
    public static function toString($array)
    {
        echo('<pre>');
        print_r($array);
        echo('</pre>');
    }

    /**
     * @param $array
     */
    public static function toStringToLog($array)
    {
        \Log::debug(print_r($array, true));
    }

    /**
     * @param $array
     */
    public static function toStringToLAll($array)
    {
        \Log::debug(print_r($array, true));
        dump($array);
    }


    /**
     * @param $prop
     * @param $propVal
     * @param $objectArray
     * @return false|mixed
     */
    public static function finObjectByProp($prop, $propVal, $objectArray)
    {
        foreach($objectArray as $object){
            if ($propVal == $object->{$prop}) { return $object;
            }
        }
        return false;
    }

    /**
     * @param $key
     * @param $val
     * @param $Array
     * @return false|mixed
     */
    public static function finElementyByElement($key, $val, $Array)
    {
        foreach($Array as $element){
            if ($val == $element[$key]) { return $element;
            }
        }
        return false;
    }

    /**
     * @param $props
     * @param $objectArray
     * @return false|mixed
     */
    public static function findElementBypElements($props, $objectArray)
    {
        foreach($objectArray as $object) {
            $equel = 0;
            foreach ($props as $prop => $propVal) {
                if ($propVal != $object[$prop]) { continue(2);
                } else { $equel++;
                }
            }
            if($equel == count($props)) { return $object;
            }
        }
        return false;
    }

    /**
     * @param $key
     * @param $array
     * @param false $default
     * @return false|mixed
     */
    public static  function element($key, $array, $default = false)
    {
        return (is_array($array) && array_key_exists($key, $array)) ? $array[$key] : $default;
    }

    /**
     * @param $key
     * @param $val
     * @param $array
     * @param false $forceNull
     */
    public static  function addElement($key, $val, &$array, $forceNull = false)
    {
        if(!$forceNull && !is_null($val)) { $data[$key] = $val;
        }
    }

    /**
     * @param $keys
     * @param $array
     * @param false $default
     * @return array
     */
    public static function elements($keys, $array, $default = false)
    {
        $return = [];
        is_array($keys) or $keys = [$keys];

        foreach ($keys as $key) {
            $return[$key] = self::element($key, $array, $default);
        }

        return $return;
    }

    /**
     * @param $array
     * @param $elem
     */
    public static function unset(&$array, $elem)
    {
        if (($key = array_search($elem, $array)) !== false) {
            unset($array[$key]);
        }
    }

    /**
     * @param $array
     * @param $elems
     */
    public static function multiunset(&$array, $elems)
    {
        is_array($elems) or $elems = [$elems];
        foreach($elems as $elem) {
            self::unset($array, $elem);
        }
    }

    /**
     * @param array $arr
     * @return bool
     */
    public static function isAssoc(array $arr)
    {
        if (array() === $arr) { return false;
        }
        return array_keys($arr) !== range(0, count($arr) - 1);
    }

    /**
     * @param $array
     * @return array
     */
    public static function assocToArray($array)
    {
        $arr = [];
        foreach($array as $key => $value){
            if($value == null || $value == '') { continue;
            }
            $arr[] = [$key, $value];
        }
        return $arr;
    }
}
