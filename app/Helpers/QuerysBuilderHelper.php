<?php

namespace App\Helpers;

/**
 * Class QuerysBuilderHelper
 * @package App\Helpers
 */
class QuerysBuilderHelper {

    /**
     * @return string
     */
    public static function generate_where(){
      $where = 'WHERE 1=1';
      $filters = $_GET['filter'] ?? '';
      //if(empty($filters)) return $where;
      if(!empty($filters)) {
              foreach($filters as $key => $value){
                if(empty($value) || $value == '' || is_null($value)) continue;
                  if ($key != 'custom' && $key != 'base_macro') {
                    $value = str_replace(' ','%%',$value);
                    $where .=" AND LOWER({$key}::public.a_vc_254)::public.a_vc_254 LIKE LOWER('%{$value}%')::public.a_vc_254";
                  } else {
                    $where .=" AND {$value}";
                  }
              }
      }
        $where .=' ';
        $where .=$_GET['basemacro'] ?? '';
      return $where;
    }

    /**
     * @return array
     */
    public static function generateCfilter(){
      $filters = array_get($_GET,'cfilter',[]);
    \MyArray::toStringToLog($filters);
      return explode(',',$filters);
    }

    /**
     * @param $fieldname
     * @param $op
     * @param $lop
     * @param $value
     * @param $fieldtype
     * @return array
     */
    public static function createfilter($fieldname, $op, $lop, $value, $fieldtype){
        $field["field"] =$fieldname;
        $field["op"] =$op;
        $field["lop"] ='OR';

        $result['field'][]=$field;
        $result['fieldtype'] =$fieldtype;
        $result['op'] =null;
        $result['lop'] =$lop;
        $result['value'] =$value;

        return $result;
    }

    /**
     * @return CustomrequestClass
     */
    public static function tempfilters(){
        $result= new \App\Helpers\CustomrequestClass();
//        \MyArray::toStringToLog($result->api);
        return $result;
    }

    /**
     * @param $_request
     * @param $filter
     * @return mixed
     */
    public static function Addfilters($_request, $filter){
        $_request->filters[] = $filter;
        return $_request;
    }

    /**
     * @param $_request
     * @param $args
     * @param $Fields
     * @return mixed
     */
    public static function Createfilters($_request, $args, $Fields){
        foreach ($args as $key => $arg){
            $op = '=';
            $fieldtType = 'int4';
            $value = $arg;
            $cast = '';
            $fieldName = '';
            foreach ($Fields as $field) {
                if ($field->getName() == $key){
                    $fieldtType = $field->getType();
                    $cast = $field->getCast();
                    $fieldName = $field->getName();
                }
            }
            if ($fieldtType =='Varchar') {
                $op = 'like';
                $fieldtType = 'varchar';
            }
            if ($cast =='boolean') {
                $value = ($arg == 1) ? 'true::BOOlEAN' : 'false::BOOlEAN';
            }
            if ($fieldName != '') {
                $filter = self::createfilter($key,$op,'AND',$value,$fieldtType);
                $_request = self::Addfilters($_request,$filter);
            }
        }
        return $_request;
    }

    /**
     * @param $selectfields
     * @param $Fields
     * @return array|string[]
     */
    public static function selectfields($selectfields, $Fields)
        {
            $_select = array_map((function($value) {
                return substr($value,strrpos($value,'.')+1);
            }), $selectfields);

            $select = array_intersect($_select, $Fields);
            if (count($select) == 0) {
                $select = [ "*"];
            } else{
                $select = array_map((function($value) {
                    if (strtolower($value) != $value ) {
                        $value ='"'.$value.'"' ;
                    }
                    return $value;
                }), $select);
            }
            return $select;
        }

    /**
     * @param $_request
     * @param $args
     */
    public static function pages($_request, $args)
    {

    }

}
