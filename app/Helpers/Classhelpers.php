<?php

namespace App\Helpers;

/**
 * Class Classhelpers
 * @package App\Helpers
 */
class Classhelpers {

    /**
     * @param $class
     * @return mixed
     */
    public static function getClass($class){
        //$class = get_called_class ();
        $class = explode('\\', $class);
        return array_last($class);
    }
}
