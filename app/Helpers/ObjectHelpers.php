<?php

namespace App\Helpers;

/**
 * Class ObjectHelpers
 * @package App\Helpers
 */
class ObjectHelpers
{

    /**
     * @param $item
     * @param $object
     * @param false $default
     * @return false|mixed
     */
    public static  function property($item, $object, $default = false){
    return (is_object($object) && property_exists($object, $item) && $object->{$item} != "") ? $object->{$item} : $default;
  }

    /**
     * @param $items
     * @param $object
     * @param false $default
     * @return object
     */
    public static function properties($items, $object, $default = false)
  {
    $return = (object) [];
    is_array($items) or $items = [$items];

    foreach ($items as $item)
      $return->{$item} = self::property($item, $object, $default);
    return $return;
  }

    /**
     * @param $items
     * @param $object
     * @param false $default
     * @return array
     */
    public static function propertiesToArray($items, $object, $default = false)
  {
    $return = [];

    is_array($items) or $items = [$items];

    foreach ($items as $item)
      $return[$item] = self::property($item, $object, $default);

    return $return;
  }

    /**
     * @param $items
     * @param $objectArray
     * @param false $default
     * @return array
     */
    public static function filterObjArray($items, $objectArray, $default = false)
  {
    $return = [];

    is_array($items) or $items = [$items];

    is_array($objectArray) or $objectArray = [$objectArray];

    foreach ($objectArray as $object)
      $return[] = self::properties($items, $object, $default);

    return $return;
  }
}
