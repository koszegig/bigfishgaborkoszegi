<?php

namespace App\Exceptions;

use InvalidArgumentException;

class EmailDoesNotExist extends InvalidArgumentException
{
    public static function named(string $email)
    {
        return new static("There is no email named `{$email}`.");
    }

    public static function withId(string $id)
    {
        return new static("There is no email with id `{$id}`.");
    }



    public static function primary()
    {
        return new static("There is no primary email for user.");
    }
}
