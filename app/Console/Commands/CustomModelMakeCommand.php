<?php

namespace App\Console\Commands;

use Illuminate\Foundation\Console\ModelMakeCommand;

//class ModelCommand extends GeneratorCommand {
class CustomModelMakeCommand extends ModelCommand
{

  /**
   * The console command name.
   *
   * @var string
   */
  protected $name = 'make:cmodel';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Create a new custom model class';

  /**
   * The type of class being generated.
   *
   * @var string
   */
  protected $type = 'Model';

  /**
   * The excluded columns for fillable.
   *
   * @var array
   */
  protected $exclude = ['id', 'password'];

  /**
   * Get the stub file for the generator.
   *
   * @return string
   */
  protected function getStub()
  {
    return __DIR__ . '/stubs/model.stub';
  }
}
