<?php


namespace App\GraphQL\Queries;

use App\Libraries\Database\QueryBuilderSP;
use App\Libraries\Database\Where;
use App\Models\Tables\Users;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use App\Helpers\QuerysBuilderHelper;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use App\GraphQL\Queries\UserQuery;

/**
 * Class UsersQuery
 * @package App\GraphQL\Queries
 */
class UsersQuery extends Query
{
    /**
     * @var string[]
     */
    protected $attributes = [
        'name' => 'users',
    ];

    /**
     * @return Type
     */
    public function type(): Type
    {
        return Type::listOf(GraphQL::type('TUser1'));
    }

    /**
     * @return array[]
     */
    public function args(): array
     {
         return [
             'id' => [
                 'name' => 'id',
                 'type' => Type::int()
                 //'rules' => ['required']
             ],
             'name' => [
                 'name' => 'name',
                 'type' => Type::string()
             ],
             'email' => [
                 'name' => 'email',
                 'type' => Type::string()
             ],
             'dateOfBirth' => [
                 'name' => 'dateOfBirth',
                 'type' => Type::string()
             ],
             'isActive' => [
                 'name' => 'isActive',
                 'type' => Type::boolean()
             ],
             'phoneNumber' => [
                 'type' => Type::string(),
                 'description' => 'The User ',
             ],
             'limit' => [
                 'type' => Type::int(),
                 'defaultValue' => 100,
             ],
             'page' => [
                 'type' => Type::int(),
                 'defaultValue' => 1,
             ],
             'orderBy' => [
                 'type' => Type::string(),
                 'defaultValue' => '',
             ],
         ];
     }

    /**
     * @param $root
     * @param $args
     * @param $context
     * @param ResolveInfo $info
     * @param Closure $getSelectFields
     * @return array|\Illuminate\Pagination\LengthAwarePaginator
     */
    public function resolve($root, $args, $context, ResolveInfo $info, Closure $getSelectFields)
    {
        $orderby ='';
        if (isset($args['orderBy']) && trim($args['orderBy']) !='' ) {
            $orderby ='order by '.$args['orderBy'];
        }
       $fields = $getSelectFields();
       $selectfields = array_map((function($value) {
           return substr($value,strrpos($value,'.')+1);
       }), $fields->getSelect());
       if (isset($args['phoneNumber']) || in_array('phoneNumber',$selectfields)  ){
           $model = "App\Models\Types\Tusers1";
       } else {
           $model = "App\Models\Tables\Users";
       }
       $_model = new $model();
       $_request = \App\Helpers\QuerysBuilderHelper::tempfilters();

       $select = ['*'];
       $builder = new QueryBuilderSP($model, [], null, $select,[],false,$orderby);
        if (isset($args['page']) && isset($args['limit'])) {
            $dmdata =$builder->Paginator($args['page'],$args['limit']);
        } else{
            $dmdata =$builder->get();
        }
        return $dmdata;

    }
}
