<?php


namespace App\GraphQL\Queries;
use App\Libraries\Database\QueryBuilderSP;
use App\Libraries\Database\Where;
use App\Models\Tables\Users;
use App\Models\Types\Tusers1;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use App\Helpers\QuerysBuilderHelper;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Class UserQuery
 * @package App\GraphQL\Queries
 */
class UserQuery extends Query
{
    /**
     * @var string[]
     */
    protected $attributes = [
        'name' => 'user',
    ];
    /**
     * @var string[]
     */
    protected $middleware = [
        \App\GraphQL\Middleware\Logstash::class,
        \App\GraphQL\Middleware\ResolvePage::class,
    ];

    /**
     * @return Type
     */
    public function type(): Type
    {
        return Type::listOf(GraphQL::type('TUser1'));
    }

    /**
     * @return array[]
     */
    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::int()
                //'rules' => ['required']
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::string()
            ],
            'email' => [
                'name' => 'email',
                'type' => Type::string()
            ],
            'dateOfBirth' => [
                'name' => 'dateOfBirth',
                'type' => Type::string()
            ],
            'isActive' => [
                'name' => 'isActive',
                'type' => Type::boolean()
            ],
            'phoneNumber' => [
                'type' => Type::string(),
                'description' => 'The User ',
            ],
                'limit' => [
                    'type' => Type::int(),
                    'defaultValue' => 100,
                ],
                'page' => [
                    'type' => Type::int(),
                    'defaultValue' => 1,
                ],
            'orderBy' => [
                'type' => Type::string(),
                'defaultValue' => '',
            ],
        ];
    }

    /**
     * @param $root
     * @param $args
     * @param $context
     * @param ResolveInfo $info
     * @param Closure $getSelectFields
     * @return array|\Illuminate\Pagination\LengthAwarePaginator
     */
    public function resolve($root, $args, $context, ResolveInfo $info, Closure $getSelectFields)
    {
        \MyArray::toStringToLog($args);
        $orderby ='';
        if (isset($args['orderBy']) && trim($args['orderBy']) !='' ) {
            $orderby ='order by '.$args['orderBy'];
        }
        $fields = $getSelectFields();
        $selectfields = array_map((function($value) {
            return substr($value,strrpos($value,'.')+1);
        }), $fields->getSelect());
        if (isset($args['phoneNumber']) || in_array('phoneNumber',$selectfields)  ){
            $model = "App\Models\Types\Tusers1";
        } else {
            $model = "App\Models\Tables\Users";
        }
        $_model = new $model();
        $_request = \App\Helpers\QuerysBuilderHelper::tempfilters();
        $select = \App\Helpers\QuerysBuilderHelper::selectfields($fields->getSelect(),$_model->getFillable());
        $_request =  \App\Helpers\QuerysBuilderHelper::Createfilters($_request,$args,$_model->getFields());
        $builder = new QueryBuilderSP($model, $_request, null, $select,[],false,$orderby);
        if (isset($args['page']) && isset($args['limit'])) {
            $dmdata =$builder->Paginator($args['page'],$args['limit']);
        } else{
            $dmdata =$builder->get();
        }
        $this->getMiddleware();
        return $dmdata;
    }
}
