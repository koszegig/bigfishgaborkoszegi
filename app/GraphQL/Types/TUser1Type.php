<?php


namespace App\GraphQL\Types;

use App\Models\Types\Tusers1;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;
class TUser1Type extends GraphQLType
{
    protected $attributes = [
        'name' => 'TUser1',
        'description' => 'User and phones',
        'model' => Tusers1::class
    ];
    public function fields(): array
    {

        return [

            'id' => [
                'type' => Type::int(),
                'description' => 'The User ',
            ],
            'name' => [
                'type' => Type::string(),
                'description' => '',
            ],
            'email' => [
                'type' => Type::string(),
                'description' => 'The User ',
            ],
            'phoneNumber' => [
                'type' => Type::string(),
                'description' => 'The User ',
            ],
            'dateOfBirth' => [
                'type' => Type::string(),
                'description' => 'The User ',
            ],
            'isActive' => [
                'type' => Type::string(),
                'description' => 'The User ',
            ],
            'createdAt' => [
                'type' => Type::string(),
                'description' => 'The User ',
            ],
            'size' => [
                'type' => Type::string(),
                'description' => 'The User ',
            ],
            'page' => [
                'type' => Type::string(),
                'description' => 'The User ',
            ],
            'totalpage' => [
                'type' => Type::string(),
                'description' => 'The User ',
            ],
            'updatedAt' => [
                'type' => Type::string(),
                'description' => 'The User ',
            ]

        ];
    }
}
