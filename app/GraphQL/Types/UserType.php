<?php


namespace App\GraphQL\Types;

use App\Models\Tables\Users;
use GraphQL\Type\Definition\Type;
use App\GraphQL\Fields\FormattableDate;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

use GraphQL\Type\Definition\ResolveInfo;

class UserType extends GraphQLType
{
    protected $attributes = [
        'name' => 'User',
        'description' => 'User enty',
        'model' => Users::class
    ];
    public function fields(): array
    {

        return [

            'id' => [
                'type' => Type::int(),
                'description' => 'The User ',
            ],
            'name' => [
                'type' => Type::string(),
                'description' => 'The User ',
            ],
            'email' => [
                'type' => Type::string(),
                'description' => 'The User ',
            ],
            'dateOfBirth' => new FormattableDate,
            'isActive' => [
                'type' => Type::string(),
                'description' => 'The User ',
            ],
            'createdAt' => new FormattableDate,
            'size' => [
                'type' => Type::string(),
                'description' => 'The User ',
            ],
            'page' => [
                'type' => Type::string(),
                'description' => 'The User ',
            ],
            'totalpage' => [
                'type' => Type::string(),
                'description' => 'The User ',
            ],
            'updatedAt' => new FormattableDate,


        ];
    }
}
