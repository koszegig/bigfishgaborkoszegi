<?php


namespace App\GraphQL\Types;
use App\Models\Tables\UserPhones;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class UserPhonesType extends GraphQLType
{
    protected $attributes = [
        'name' => 'UserPhones',
        'description' => 'User phones',
        'model' => UserPhones::class
    ];
    public function fields(): array
    {

        return [

            'user_id' => [
                'type' => Type::int(),
                'description' => '',
            ],
            'phoneNumber' => [
                'type' => Type::string(),
                'description' => 'The User Phones',
            ]
        ];
    }
}
