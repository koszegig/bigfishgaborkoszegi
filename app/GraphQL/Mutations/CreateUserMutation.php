<?php


namespace App\GraphQL\Mutations;
use App\Libraries\Database\QueryBuilderSP;
use App\Models\Tables\Users;
use Rebing\GraphQL\Support\Mutation;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Support\Arr;

use App\Libraries\Database\Where;
use Rebing\GraphQL\Support\Query;
use App\Helpers\QuerysBuilderHelper;

/**
 * Class CreateUserMutation
 * @package App\GraphQL\Mutations
 */
class CreateUserMutation extends Mutation
{
    /**
     * @var string[]
     */
    protected $attributes = [
        'name' => 'createUser'
    ];

    /**
     * @return Type
     */
    public function type(): Type
    {
        return Type::listOf(GraphQL::type('User'));
    }

    /**
     * @return array[]
     */
    public function args(): array
    {
        return [
            'name' => [
                'name' => 'name',
                'type' =>  Type::nonNull(Type::string()),
                'rules' => ['required']
            ],
            'email' => [
                'name' => 'email',
                'type' =>  Type::nonNull(Type::string()),
                'rules' => ['required']
            ],
            'dateOfBirth' => [
                'name' => 'dateOfBirth',
                'type' =>  Type::nonNull(Type::string()),
                'rules' => ['required']
            ],
            'isActive' => [
                'name' => 'isActive',
                'type' =>  Type::nonNull(Type::boolean()),
                'rules' => ['required']
            ],
            'phoneNumber' => [
                'name' => 'phoneNumber',
                'type' =>  Type::nonNull(Type::string()),
                'rules' => ['required']
            ],
        ];
    }

    /**
     * @param $root
     * @param $args
     * @return array
     */
    public function resolve($root, $args)
    {
        $model = "App\Models\Tables\Users";
        $_model = new $model();
        $where = [];
        foreach ($args as $key => $arg){
            if (in_array($key,['name','email','dateOfBirth'])){
                $where[$key] =  $arg;
            }
        }
        $_data = [];
        foreach($_model->getFillable() as $field){
            $value = Arr::get($args, $field, null);
            if($value !== null){
                $_data[$field] = $value;
            }
        }
       $user = $model::UpdateOrCreate($where,$_data);
        $user->sync_Phones($args['phoneNumber']);
        $_request = \App\Helpers\QuerysBuilderHelper::tempfilters();
        $select = [ "*" ];
        $Selectmodel = "App\Models\Types\Tusers1";
        $_selectmodel = new $model();
        $_request =  \App\Helpers\QuerysBuilderHelper::Createfilters($_request,$args,$_selectmodel->getFields());

        $builder = new QueryBuilderSP($_selectmodel, $_request, null, $select,[],false,'');
        $dmdata =$builder->get();
        return $dmdata;

    }
}
