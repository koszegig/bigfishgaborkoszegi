<?php


namespace App\GraphQL\Mutations;


use App\Libraries\Database\QueryBuilderSP;
use App\Models\Tables\Users;
use Illuminate\Support\Arr;
use Rebing\GraphQL\Support\Mutation;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;

/**
 * Class DeleteUserPhoneMutation
 * @package App\GraphQL\Mutations
 */
class DeleteUserPhoneMutation extends Mutation
{
    /**
     * @var string[]
     */
    protected $attributes = [
        'name' => 'deleteUser',
        'description' => 'Delete a user '
    ];

    /**
     * @return Type
     */
    public function type(): Type
    {
        return Type::boolean();
    }

    /**
     * @return array[]
     */
    public function args(): array
    {
        return [
            'name' => [
                'name' => 'name',
                'type' => Type::string(),
                'rules' => ['required']
            ],
            'phoneNumber' => [
                'name' => 'phoneNumber',
                'type' => Type::string(),
                'rules' => ['required']
            ]
        ];
    }

    /**
     * @param $root
     * @param $args
     * @return bool
     */
    public function resolve($root, $args)
    {
        //$user = User::findOrFail($args['id']);
        $_request = \App\Helpers\QuerysBuilderHelper::tempfilters();
        $select = ['*'];
        $model = "App\Models\Tables\Users";
        $_model = new $model();
        $_request =  \App\Helpers\QuerysBuilderHelper::Createfilters($_request,['name' => $args['name']],$_model->getFields());
        $builder = new QueryBuilderSP($model, $_request, null, $select,[],false,'');
        $dmdata =$builder->get();
        $user = Users::findOrFail($dmdata[0]->id);
        $user->removePhone($args['phoneNumber']);
        return  true;
    }
}

