<?php


namespace App\GraphQL\Mutations;

use App\Models\Tables\Users;
use Illuminate\Support\Arr;
use Rebing\GraphQL\Support\Mutation;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;

/**
 * Class DeleteUserMutation
 * @package App\GraphQL\Mutations
 */
class DeleteUserMutation  extends Mutation
{
    protected $attributes = [
        'name' => 'deleteUser',
        'description' => 'Delete a user '
    ];

    public function type(): Type
    {
        return Type::boolean();
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::int(),
                'rules' => ['required']
            ]
        ];
    }

    public function resolve($root, $args)
    {
        //$user = User::findOrFail($args['id']);
        $model = "App\Models\Tables\Users";
        $model::destroy($args['id']);
        //return  $user->delete() ? true : false;
        return  true;
    }
}
