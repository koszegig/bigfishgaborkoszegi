<?php


namespace App\GraphQL\Mutations;
use App\Libraries\Database\QueryBuilderSP;
use App\Models\Tables\Users;
use Illuminate\Support\Arr;
use Rebing\GraphQL\Support\Mutation;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;

/**
 * Class UpdateUserMutation
 * @package App\GraphQL\Mutations
 */
class UpdateUserMutation  extends Mutation
{
    /**
     * @var string[]
     */
    protected $attributes = [
        'name' => 'updateUser'
    ];

    /**
     * @return Type
     */
    public function type(): Type
    {
        return Type::listOf(GraphQL::type('User'));
    }

    /**
     * @return array[]
     */
    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' =>  Type::nonNull(Type::int()),
            ],
            'name' => [
                'name' => 'name',
                'type' =>  Type::string(),
            ],
            'email' => [
                'name' => 'email',
                'type' =>  Type::string(),
            ],
            'dateOfBirth' => [
                'name' => 'dateOfBirth',
                'type' =>  Type::string(),
            ],
            'phoneNumber' => [
                'name' => 'phoneNumber',
                'type' =>  Type::string()
            ],
            'isActive' => [
                'name' => 'isActive',
                'type' =>  Type::boolean(),
            ],
        ];
    }

    /**
     * @param array $args
     * @return \string[][]
     */
    protected function rules(array $args = []): array
    {
        return [
            'id' => ['required'],
        ];
    }

    /**
     * @param $root
     * @param $args
     * @return array
     */
    public function resolve($root, $args)
    {
        //\MyArray::toStringToLog($args);
        $model = "App\Models\Tables\Users";
        $_model = new $model();
        $where = [];
        foreach ($args as $key => $arg){
            if (in_array($key,['name','email','dateOfBirth'])){
                $where[$key] =  $arg;
            }
        }
        $_data = [];
        foreach($_model->getFillable() as $field){
            if ($field!='id') {
                $value = Arr::get($args, $field, null);
                if($value !== null){
                    $_data[$field] = $value;
                }
            }
        }


        $user =$model::UpdateOrCreate(['id'=>$args['id']],$args);
        if (isset($args['phoneNumber'])) {
            $user->sync_Phones($args['phoneNumber']);
        }
        $_request = \App\Helpers\QuerysBuilderHelper::tempfilters();
        $select = [ "*"
        ];
        $_request =  \App\Helpers\QuerysBuilderHelper::Createfilters($_request,$args,$_model->getFields());

        $builder = new QueryBuilderSP($model, $_request, null, $select,[],false,'');
        $dmdata =$builder->get();
        return $dmdata;
    }
}
