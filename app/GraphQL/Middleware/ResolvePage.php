<?php



namespace App\GraphQL\Middleware;

use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Pagination\Paginator;
use Rebing\GraphQL\Support\Middleware;

/**
 * Class ResolvePage
 * @package App\GraphQL\Middleware
 */
class ResolvePage extends Middleware
{
    /**
     * @param $root
     * @param $args
     * @param $context
     * @param ResolveInfo $info
     * @param Closure $next
     * @return mixed
     */
    public function handle($root, $args, $context, ResolveInfo $info, Closure $next)
    {

        Paginator::currentPageResolver(function () use ($args) {
            return $args['pagination']['page'] ?? 1;
        });

        return $next($root, $args, $context, $info);
    }

}
