<?php



namespace App\GraphQL\Middleware;

use Countable;
use GraphQL\Language\Printer;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\AbstractPaginator;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Rebing\GraphQL\Support\Middleware;

/**
 * Class Logstash
 * @package App\GraphQL\Middleware
 */
class Logstash extends Middleware
{
    /**
     * @param $root
     * @param $args
     * @param $context
     * @param ResolveInfo $info
     * @param $result
     */
    public function terminate($root, $args, $context, ResolveInfo $info, $result): void
    {

        Log::channel('daily')->info('',
        collect([
            'query' => $info->fieldName,
            'operation' => $info->operation->name->value ?? null,
            'type' => $info->operation->operation,
            'fields' => array_keys(Arr::dot($info->getFieldSelection($depth = PHP_INT_MAX))),
            'schema' => Arr::first(Route::current()->parameters()) ?? config('graphql.default_schema'),
            'vars' => $this->formatVariableDefinitions($info->operation->variableDefinitions),
        ])
            ->when($result instanceof Countable, function ($metadata) use ($result) {
                return $metadata->put('count', $result->count());
            })
            ->when($result instanceof AbstractPaginator, function ($metadata) use ($result) {
                return $metadata->put('per_page', $result->perPage());
            })
            ->when($result instanceof LengthAwarePaginator, function ($metadata) use ($result) {
                return $metadata->put('total', $result->total());
            })
            ->merge($this->formatArguments($args))
            ->toArray()
        );
    }

    /**
     * @param array $args
     * @return array
     */
    private function formatArguments(array $args): array
    {
        return collect($args)
            ->mapWithKeys(function ($value, $key) {
                return ["\${$key}" => $value];
            })
            ->toArray();
    }

    /**
     * @param iterable|array|null $variableDefinitions
     * @return array
     */
    private function formatVariableDefinitions(?iterable $variableDefinitions = []): array
    {
        return collect($variableDefinitions)
            ->map(function ($def) {
                return Printer::doPrint($def);
            })
            ->toArray();
    }
}
