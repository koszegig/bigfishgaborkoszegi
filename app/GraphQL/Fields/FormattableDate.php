<?php


namespace App\GraphQL\Fields;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Field;

/**
 * Class FormattableDate
 * @package App\GraphQL\Fields
 */
class FormattableDate extends Field
{
    /**
     * @var array|string[]
     */
    protected $attributes = [
        'description' => 'A field that can output a date in all sorts of ways.',
    ];

    /**
     * FormattableDate constructor.
     * @param array $settings
     */
    public function __construct(array $settings = [])
    {
        $this->attributes = \array_merge($this->attributes, $settings);
    }

    /**
     * @return Type
     */
    public function type(): Type
    {
        return Type::string();
    }

    /**
     * @return array[]
     */
    public function args(): array
    {
        return [
            'format' => [
                'type' => Type::string(),
                'defaultValue' => 'Y-m-d H:i',
                'description' => 'Defaults to Y-m-d H:i',
            ],
            'relative' => [
                'type' => Type::boolean(),
                'defaultValue' => false,
            ],
        ];
    }

    /**
     * @param $root
     * @param $args
     * @return string|null
     */
    protected function resolve($root, $args): ?string
    {
        $date = $root->{$this->getProperty()};

        if (!$date instanceof Carbon) {
            return null;
        }

        if ($args['relative']) {
            return $date->diffForHumans();
        }

        return $date->format($args['format']);
    }

    /**
     * @return string
     */
    protected function getProperty(): string
    {
        return $this->attributes['alias'] ?? $this->attributes['name'];
    }
}
