<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;
use App\Exceptions\ConnectionTimeoutException;

/**
 * Class ExceptionsServiceProvider - Hacky?!
 * @package App\Providers
 */
class ExceptionsServiceProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function boot(){}

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        app('Dingo\Api\Exception\Handler')->register(function (\PDOException $exception) {
            return response()->error($exception->getMessage(), 500);
            //return response()->error('Could not connect to database', 500);
        });

        app('Dingo\Api\Exception\Handler')->register(function (ConnectionTimeoutException $exception) {
            return response()->error($exception->getMessage(), 500);
        });
    }
}
