<?php

namespace App\Models\Types;
use App\Libraries\Field;
use App\Models\Base\BaseModel01Sys;

/**
 * Class Tusers1
 * @package App\Models\Types
 */
class Tusers1 extends BaseModel01Sys  {
    /**
     * @var string
     */
    public $alias = 'users';
    /**
     * @var string
     */
    protected $table = '01_sys.tusers1';

    /**
     *
     */
    protected function setStoredProcedure(){
        $this->storedProcedure = '  "01_sys".get_users_1 ()';
    }

    /**
     * @return \App\Models\Base\RootModel|void
     */
    public function setFields(){
        $this->fields = collect([
            new Field("id", "Int4", true, true,null),
            new Field("name", "Varchar", true, false,null),
            new Field("email", "Varchar", true, false,null),
            new Field("dateOfBirth", "Date", true, false,null),
            new Field('isActive', "Bool", true, false,'boolean'),
            new Field("phoneNumber", "Varchar", true, false,null),
            new Field("createdAt", "Timestamp", true, false,null),
            new Field("updatedAt", "Timestamp", true, false,null),
        ]);
    }
}
