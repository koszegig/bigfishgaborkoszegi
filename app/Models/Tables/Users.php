<?php

namespace App\Models\Tables;
use App\Libraries\Field;
use App\Models\Base\BaseModel01Sys;
use App\Traits\HasPhones;

/**
 * Class Users
 * @package App\Models\Tables
 */
class Users extends BaseModel01Sys  {
    use HasPhones;

    /**
     * @var string
     */
    public $alias = 'users';
    /**
     * @var string
     */
    protected $table = '01_sys.users';

    /**
     *
     */
    protected function setStoredProcedure(){
        $this->storedProcedure = '  "01_sys".users ';
    }

    /**
     * @return \App\Models\Base\RootModel|void
     */
    public function setFields(){
          $this->fields = collect([
           new Field("id", "Int4", true, true,null),
                      new Field("name", "Varchar", true, false,null),
                      new Field("email", "Varchar", true, false,null),
                      new Field("dateOfBirth", "Date", true, false,null),
                      new Field('isActive', "Bool", true, false,'boolean'),
                      new Field("createdAt", "Timestamp", true, false,null),
                      new Field("updatedAt", "Timestamp", true, false,null),
                  ]);
      }
}
