<?php

namespace App\Models\Tables;
use App\Libraries\Field;
use App\Models\Base\BaseModel01Sys;

/**
 * Class UserPhones
 * @package App\Models\Tables
 */
class UserPhones extends BaseModel01Sys  {

    /**
     * @var array
     */
    protected $dates = [];
    /**
     * @var string
     */
    public $alias = '';
    /**
     * @var string
     */
    protected $table = '01_sys.userphones';

    /**
     *
     */
    protected function setStoredProcedure(){
        $this->storedProcedure = '  "01_sys".get_users ( ?::public.a_vc_250
                                     )';
    }

    /**
     * @return \App\Models\Base\RootModel|void
     */
    public function setFields(){
        $this->fields = collect([
            new Field("user_id", "Int3", true, false,null),
            new Field("phoneNumber", "Varchar", true, false,null),
        ]);
    }

    /**
     * @param $phone
     * @return mixed
     */
    public static function findByPhone($phone)
    {
        $_phone = static::where('phoneNumber', $phone)->first();

        return $_phone;
    }

}
