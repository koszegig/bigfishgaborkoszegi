<?php

namespace App\Models\Tables;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Auth\Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Spatie\Permission\Traits\HasRoles;
//use App\Traits\HasEmails;
//use App\Traits\HasGroups;
//use App\Traits\HasPhones;
use App\Models\Base\BaseModel01Sys;
use Laravel\Passport\HasApiTokens;
use App\Libraries\Field;;

/**
 * Class LoginUser
 * @package App\Models\Tables
 */
class LoginUser  extends BaseModel01Sys implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract,
    JWTSubject

{

    /**
     * @var string
     */
    public $alias = 'loginusers';
    /**
     * @var string
     */
    protected $table = '01_sys.loginusers';
    /**
     * @var string
     */
    public $schema = '01_sys';
    /**
     * @var string
     */
    protected $primaryKey = 'id';
    use Notifiable, Authenticatable, Authorizable, CanResetPassword;
    // HasRoles,HasGroups,HasEmails,HasPhones;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'avatar' => 'image',
    ];

    /**
     *
     */
    public function initialTable(){
        $this->table = 'loginusers';
    }

    /**
     * @return \App\Models\Base\RootModel|void
     */
    public function setFields(){
        $this->fields = [
            new Field("id", "Int4", true, true),
            new Field("username", "varchar", true, false),
            new Field("active", "bool", true, false),
            new Field("system", "bool", true, false),
            new Field("createdAt", "timestamp", false, false),
            new Field("updatedAt", "timestamp", false, false),
        ];
    }

    /**
     *
     */
    public function initialHidden(){
        array_push($this->hidden, 'password', 'remember_token');
    }




    /**
     * The answer that belong to the question.
     */
    public function getPrimaryEmailAttribute()
    {
        /*$email = $this->getPrimaryEmail();
        if(empty($email)) return '';
        return $email->email;**/
        return '';

    }

    /**
     * The answer that belong to the question.
     */
    public function getPrimaryPhoneAttribute()
    {
        /*$phone = $this->getPrimaryPhone();
        if(empty($phone)) return '';
        return $phone->phone;*/
        return '';
    }
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
