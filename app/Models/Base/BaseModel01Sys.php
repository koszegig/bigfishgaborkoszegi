<?php

namespace App\Models\Base;

use App\Models\Base\BaseModel;

/**
 * Class BaseModel01Sys
 * @package App\Models\Base
 */
class BaseModel01Sys extends BaseModel
{
    /**
     * @var string
     */
    public $schema = '01_sys';
    /**
     * @var string
     */
    protected $connection = 'pgsql_01_sys';
}
