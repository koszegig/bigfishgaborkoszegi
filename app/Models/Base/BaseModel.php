<?php

namespace App\Models\Base;

use App\Models\Base\RootModel;


/**
 * Class BaseModel
 * @package App\Models\Base
 */
class BaseModel extends RootModel
{

    /**
     * @var
     */
    protected $storedProcedure;

      /**
     * Create a new Eloquent model instance.
     *
     * @param  array $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        $this->setFields();
        $this->initialTable();
        $this->setStoredProcedure();
        $this->processField();
        $this->initialHidden();
        parent::__construct($attributes);
        $this->initialAppends();
    }

    /**
     *
     */
    public function processField(){
      foreach($this->fields as $field){
        $name = $field->getName();
        if($field->isFillable()){
          array_push($this->fillable, $name);
        }
        if($field->isPrimary()){
          $this->primaryKey = $name;
        }
        $cast = $field->getCast();
        if($cast != null){
          $this->cast[$name] = $cast;
        }
      }
    }

    /**
     * @return array
     */
    public function getFillable(){
      return $this->fillable;
    }

    /**
     *
     */
    public function initialTable(){}

    /**
     *
     */
    protected function setStoredProcedure(){}

    /**
     *
     */
    public function initialHidden(){}

    /**
     *
     */
    public function initialAppends(){}

    /**
     * @param $_active
     * @return $this|false
     */
    public function modifyActive($_active)
    {
        $this->active = $_active;
        if ($this->save()) {
            return $this;
        }
        return false;
    }

    /**
      * Scope a query to only include active users.
      *
      * @param \Illuminate\Database\Eloquent\Builder $query
      * @return \Illuminate\Database\Eloquent\Builder
      */
    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    /**
      * Scope a query to only include active users.
      *
      * @param \Illuminate\Database\Eloquent\Builder $query
      * @return \Illuminate\Database\Eloquent\Builder
      */
    public function scopeOrderbyname($query)
    {
        return $query->orderBy('name');
    }

    /**
     * @param $query
     * @param array $value
     * @return mixed
     */
    public function scopeExclude($query, $value = array())
    {
        return $query->select(array_diff($this->fillable, (array)$value));
    }

    /**
     * @return mixed
     */
    public function getStoredProcedure(){
      return $this->storedProcedure;
    }

    /**
     * @return string
     */
    public function getPrimaryKey(){
      return $this->primaryKey;
    }
}
