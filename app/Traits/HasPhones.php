<?php

namespace App\Traits;

use App\Models\Base\BaseModel;
use Illuminate\Support\Collection;
use App\Models\Tables\UserPhones;

/**
 * Trait HasPhones
 * @package App\Traits
 */
trait HasPhones
{
    /**
     * @param $phone
     * @return Phone
     */
    protected function getStoredPhone($phone): Phone
    {
            return UserPhones::where('phoneNumber', $phone)->first();
    }
      /**
     * A model may have multiple phones.
     */

    public function phones(){
        return $this->hasMany('App\Models\Tables\UserPhones', 'user_id');
    }
    /**
     * Assign the given phone to the model.
     *
     * @param string $phone
     *
     * @return $this
     */
    public function addPhone($phone)
    {
        $related = \MyClass::getClass(get_called_class());
        $_phone = new UserPhones(['phoneNumber' => $phone['phoneNumber'], 'user_id' => $this->id]);
        $this->phones()->save($_phone);

        return $this;
    }

    /**
     * Assign the given phone to the model.
     *
     * @param string $phone
     *
     * @return $this
     */
    public function updatePhone($data)
    {
        $phone = $this->getStoredPhone($data['phoneNumber']);
        UserPhones::UpdateOrCreate(['phoneNumber' => $data['phoneNumber'], 'user_id' => $data['id']],['phoneNumber' => $data['phoneNumber'], 'user_id' => $data['id']]);
        $phone->save();

        return $this;
    }

      /**
     * Assign the given phone to the model.
     *
     * @param array $phones
     *
     * @return $this
     */
    public function addPhones($phones)
    {
        $related = \MyClass::getClass(get_called_class());
        if (is_array($phones)){
            foreach($phones as $phone){
                if ( is_object($phone) &&  property_exists($phone, "phoneNumber")) {
                    $_phones[] = new UserPhones(['phoneNumber' => $phone->phoneNumber, 'user_id' => $this->id]);
                } else {
                    $_phones[] = new UserPhones(['phoneNumber' => $phones, 'user_id' => $this->id]);
                }
            }
        } else{
            $_phones[] = new UserPhones(['phoneNumber' => $phones, 'user_id' => $this->id]);
        }
        //\MyArray::toStringToLog($_phones);
        $this->phones()->save($_phones);

        return $this;
    }
    /**
     * Assign the given phone to the model.
     *
     * @param array $phones
     *
     * @return $this
     */
    public function sync_Phones($phones)
    {
        $model = "App\Models\Tables\UserPhones";
        $_phones = [];
        if (is_array($phones)){
            foreach($phones as $phone){

                if ( is_object($phone) &&  property_exists($phone, "phoneNumber")) {
                    $model::UpdateOrCreate(['phoneNumber' => $phone->phoneNumber, 'user_id' => $this->id],['phoneNumber' => $phone->phoneNumber, 'user_id' => $this->id]);
                } else {
                    $model::UpdateOrCreate(['phoneNumber' => $phone, 'user_id' => $this->id],['phoneNumber' => $phone, 'user_id' => $this->id]);
                }
            }
        } else{
            $model::UpdateOrCreate(['phoneNumber' => $phones, 'user_id' => $this->id],['phoneNumber' =>$phones, 'user_id' => $this->id]);

        }
        return $this;
    }
      /**
     * Revoke the given phone from the model.
     *
     * @param array|string $phone
     */
    public function removePhone($phones)
    {
        if(!is_array($phones))
            $phones = [$phones];
        foreach($phones as $phone){
            $affectedRows = UserPhones::where('phoneNumber', $phone)->delete();
        }

    }

    /**
     * Remove all current phones and set the given ones.
     *
     * @param array|string $phones
     *
     * @return $this
     */
    public function syncPhones($phones)
    {
        foreach($phones as $phone){
            $id = array_get($phone, 'id', null);
            if(is_null($id)){
                $this->addPhone($phone);
            }
            else
                $this->updatePhone($phone);
         }
    }

    /**
     * @return Collection
     */
    public function getPhoneNumbers(): Collection
    {
        return $this->phones->pluck('phoneNumber');
    }
}
