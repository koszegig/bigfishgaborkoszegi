<?php

namespace App\Traits\LaravelGrapHQL;

interface Scalar
{
    public static function match($value): bool;

    public function __toString();
}
