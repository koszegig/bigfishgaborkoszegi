<?php

namespace App\Libraries\Database\Types;

/**
 * Class Int4Type
 * @package App\Libraries\Database\Types
 */
class Int4Type extends QueryFieldType
{
    /**
     * Int4Type constructor.
     */
    public function __construct()
  {
    $this->operators = ['=', '<', '>', '<=', '>=', '<>'];
    $this->quote = false;
    $this->lower = false;
  }
}
