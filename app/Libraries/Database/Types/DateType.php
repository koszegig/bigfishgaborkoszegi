<?php

namespace App\Libraries\Database\Types;

/**
 * Class DateType
 * @package App\Libraries\Database\Types
 */
class DateType extends QueryFieldType
{
    /**
     * DateType constructor.
     */
    public function __construct()
  {
    $this->operators = [ '=', '<', '>', '<=', '>=', '<>', 'between'];
    $this->quote = true;
    $this->lower = false;
  }
}
