<?php

namespace App\Libraries\Database\Types;

/**
 * Class QueryFieldType
 * @package App\Libraries\Database\Types
 */
class QueryFieldType
{
    /**
     * @var
     */
    protected $operators;
    /**
     * @var
     */
    protected $quote;
    /**
     * @var
     */
    protected $lower;

    /**
     * @param $operator
     * @return bool
     */
    public function isValidOperator($operator){
    return in_array($operator, $this->operators);
  }

    /**
     * @return mixed
     */
    public function quoted(){
    return $this->quote;
  }

    /**
     * @return mixed
     */
    public function isLower(){
    return $this->lower;
  }


}
