<?php

namespace App\Libraries\Database\Types;

/**
 * Class VarcharType
 * @package App\Libraries\Database\Types
 */
class VarcharType extends QueryFieldType
{
    /**
     * VarcharType constructor.
     */
    public function __construct()
  {
    $this->operators = ['=', '<>','like', 'not like', 'ilike', 'not ilike'];
    $this->quote = true;
    $this->lower = true;
  }
}
