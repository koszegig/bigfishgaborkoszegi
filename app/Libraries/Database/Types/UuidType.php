<?php

namespace App\Libraries\Database\Types;

/**
 * Class UuidType
 * @package App\Libraries\Database\Types
 */
class UuidType extends QueryFieldType
{
    /**
     * UuidType constructor.
     */
    public function __construct()
  {
    $this->operators = ['=', '<>'];
    $this->quote = true;
    $this->lower = false;
  }
}
