<?php

namespace App\Libraries\Database\Types;

/**
 * Class TimestampType
 * @package App\Libraries\Database\Types
 */
class TimestampType extends QueryFieldType
{
    /**
     * TimestampType constructor.
     */
    public function __construct()
  {
    $this->operators = [ '=', '<', '>', '<=', '>=', '<>', 'between'];
    $this->quote = false;
    $this->lower = false;
  }
}
