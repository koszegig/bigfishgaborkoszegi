<?php

namespace App\Libraries\Database\Types;

/**
 * Class TextType
 * @package App\Libraries\Database\Types
 */
class TextType extends QueryFieldType
{
    /**
     * TextType constructor.
     */
    public function __construct()
  {
    $this->operators = ['=', '<>','like', 'not like', 'ilike', 'not ilike'];
    $this->quote = true;
    $this->lower = true;
  }
}
