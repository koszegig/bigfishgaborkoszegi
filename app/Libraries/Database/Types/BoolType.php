<?php

namespace App\Libraries\Database\Types;

/**
 * Class BoolType
 * @package App\Libraries\Database\Types
 */
class BoolType extends QueryFieldType
{
    /**
     * BoolType constructor.
     */
    public function __construct()
  {
    $this->operators = ['=', '<>'];
    $this->quote = false;
    $this->lower = false;
  }
}
