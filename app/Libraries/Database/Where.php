<?php

namespace App\Libraries\Database;

/**
 * Class Where
 * @package App\Libraries\Database
 */
class Where
{
    /**
     * @var mixed|null
     */
    private $field;
    /**
     * @var mixed|null
     */
    private $value;
    /**
     * @var array
     */
    private $sub;
    /**
     * @var mixed|null
     */
    private $op;
    /**
     * @var mixed|null
     */
    private $lop;
    /**
     * @var false|mixed
     */
    private $lower;
    /**
     * @var false|mixed
     */
    private $quoted;

    /**
     * Where constructor.
     * @param null $field
     * @param null $value
     * @param null $op
     * @param null $lop
     * @param false $lower
     * @param false $quoted
     */
    public function __construct($field = null, $value = null, $op = null, $lop = null, $lower = false, $quoted = false)
  {
    $this->field = $field;
    $this->value = $value;
    $this->sub = [];
    $this->op = $op;
    $this->lop = $lop;
    $this->lower = $lower;
    $this->quoted = $quoted;
  }

  /**
   * Get the value of field
   */
  public function getField()
  {
    return $this->field;
  }

  /**
   * Set the value of field
   *
   * @return  self
   */
  public function setField($field)
  {
    $this->field = $field;

    return $this;
  }

  /**
   * Get the value of value
   */
  public function getValue()
  {
    return $this->value;
  }

  /**
   * Set the value of value
   *
   * @return  self
   */
  public function setValue($value)
  {
    $this->value = $value;

    return $this;
  }

  /**
   * Get the value of sub
   */
  public function getSub()
  {
    return $this->sub;
  }

  /**
   * Set the value of sub
   *
   * @return  self
   */
  public function setSub($sub)
  {
    $this->sub = $sub;

    return $this;
  }

  /**
   * Get the value of op
   */
  public function getOp()
  {
    return $this->op;
  }

  /**
   * Set the value of op
   *
   * @return  self
   */
  public function setOp($op)
  {
    $this->op = $op;

    return $this;
  }

  /**
   * Get the value of op
   */
  public function getLop()
  {
    return $this->lop;
  }

  /**
   * Set the value of op
   *
   * @return  self
   */
  public function setLop($lop)
  {
    $this->lop = $lop;

    return $this;
  }



  /**
   * Get the value of op
   */
  public function getLower()
  {
    return $this->lower;
  }

  /**
   * Set the value of op
   *
   * @return  self
   */
  public function setLower($lower)
  {
    $this->lower = $lower;

    return $this;
  }

    /**
     * @return string
     */
    public function build(){
    if(!empty($this->sub)) return $this->buildSub();
    if($this->op == 'between')
        return " $this->field $this->op $this->value[0] AND $this->value[1]";
    $where = "";
    $where .= $this->_buildCondition();
    if($this->lop != null) $where .= " {$this->lop} ";
    return $where;
  }

    /**
     * @return string
     */
    private function _buildCondition(){
    $where = $this->buildCondition('field');
    $where.= " $this->op ";
    $condval = $this->buildCondition('value');
    $where.= $condval;
    return $where;
  }

    /**
     * @param $_prop
     * @return mixed|string
     */
    private function buildCondition($_prop){
    $prop = $this->$_prop;
    if($_prop == 'value'){
      $prop = $this->setPSign($prop);
      if (isset($prop['year']))
      {
          $prop= implode('.', $prop);
          $this->quoted =true;
      }
      $prop = $this->setQuoted($prop);
    }
      if($_prop == 'field'){
          if (strtolower($prop) != $prop ) {
              $prop ='"'.$prop.'"' ;
          }
      }
    return ($this->lower) ? "LOWER(".$prop.")" : $prop;
  }

    /**
     * @param $prop
     * @return mixed|string
     */
    private function setQuoted($prop){
    return ($this->quoted) ? "'".$prop."'" : $prop;
  }

    /**
     * @param $prop
     * @return mixed|string
     */
    private function setPSign($prop){
    return (in_array($this->op, ['like', 'not like'])) ? "%".str_replace(' ','%%', $prop)."%" : $prop;
  }

    /**
     * @return string
     */
    private function buildSub(){
    $where = "( ";
    foreach($this->sub as $index => $sub){
        if($index == (count($this->sub)-1)) $sub->setLop(null);
        $where .= $sub->build();
    }
    $where .= ") {$this->lop} ";
    return $where;
  }

    /**
     * @param $field
     * @param $value
     * @param $op
     * @param $lop
     * @param $lower
     * @param $quoted
     * @return $this
     */
    public function pushSub($field, $value, $op, $lop, $lower, $quoted){
      array_push($this->sub, new self($field, $value, $op, $lop, $lower, $quoted));
      return $this;
  }
}
