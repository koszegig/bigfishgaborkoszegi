<?php

namespace App\Libraries\Database;

use App\Libraries\Database\Where;
use App\Libraries\Database\From;
use App\Libraries\Database\Select;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class QueryBuilderSP
 * @package App\Libraries\Database
 */
class QueryBuilderSP
{
    /**
     * @var
     */
    private $query;
    /**
     * @var array
     */
    private $condition;
    /**
     * @var mixed
     */
    private $model;
    /**
     * @var
     */
    private $filters;
    /**
     * @var
     */
    private $paginate;
    /**
     * @var \App\Libraries\Database\Select
     */
    private $select;
    /**
     * @var
     */
    private $condstring;
    /**
     * @var \App\Libraries\Database\From
     */
    private $from;
    /**
     * @var array|mixed
     */
    private $params;
    /**
     * @var
     */
    private $size;
    /**
     * @var
     */
    private $offset;
    /**
     * @var
     */
    private $pageto;
    /**
     * @var
     */
    private $basemacro;
    /**
     * @var
     */
    private $current_page;
    /**
     * @var
     */
    private $pagefrom;
    /**
     * @var mixed|string
     */
    private $orderBy;

    /**
     * @var string[]
     */
    public $types = [
    'uuid',
    'Varchar',
    'Date',
    'Bool',
    'timestamp',
    'Int4',
  ];

    /**
     * QueryBuilderSP constructor.
     * @param $model
     * @param $request
     * @param $paginate
     * @param $select
     * @param array $params
     * @param bool $isAddId
     * @param string $orderBy
     */
    public function __construct($model, $request, $paginate, $select, $params = [], $isAddId= true, $orderBy= '')
  {
    $this->model = new $model();
    $this->paginate = $paginate;
    $this->params = $params;
    $this->orderBy = $orderBy;
    if ($isAddId) {
        $select[] = $this->model->getPrimaryKey().' as id';
    }
    $this->select = new Select($select);
    $this->condition = [];
    $this->buildlop = true;
    $this->from = new From($this->model);
    $this->initialFilters($request);
    $this->initialOffset($request);
    $this->processFilters();
    array_push($this->condition, new where('1', '1', '='));
  }

    /**
     * @param int $size
     * @return $this
     */
    public function paginate($size = 25){
    $this->paginate = $size;
    return $this;
  }

    /**
     * @param $request
     */
    private function initialOffset($request){
    $this->size = '' ;
    $this->offset = '' ;
    $this->pagefrom = 0;
    if (isset($request->api)) {
        if ( $request->api['size'] != 0) {
              $this->pagefrom = (int)$request->api['offset'];
              $this->pageto = (int)$request->api['offset']+(int)$request->api['size'];
              $this->current_page = $this->pageto/(int)$request->api['size'];
              $this->size = ' limit '. $request->api['size'];
              $this->offset = ' offset '. $request->api['offset'];
        }
    }
  }

    /**
     * @param $request
     */
    private function initialFilters($request){
    $this->filters =  isset($request->filters) ? $request->filters : [];

  }

    /**
     *
     */
    private function processFilters(){
    /*\MyArray::toStringToLog('processFilters----');
    \MyArray::toStringToLog($this->filters);
    \MyArray::toStringToLog('----');*/
    if(empty($this->filters)) return;
    foreach($this->filters as $filter){
      $this->processFilter($filter);
    }
  }

    /**
     * @param $filter
     */
    private function processFilter($filter){
    $this->where($filter);
  }

    /**
     * @param $filter
     */
    private function where($filter){

    $where = (is_array($filter['field'])) ? $this->subwhere($filter) : $this->__where($filter);
    array_push($this->condition, $where);
  }

    /**
     * @param $filter
     * @return \App\Libraries\Database\Where
     */
    private function  subwhere($filter){
    $this->basemacro = '';
    $where = new Where(null, null, null, $filter['lop']);
    foreach($filter['field'] as $field){
      $type = $this->getType($field['field'],$filter);
     /*\MyArray::toStringToLog('if(!$this->isValidFilter($field, $type))----');
        \MyArray::toStringToLog($field);
        \MyArray::toStringToLog($type);
        \MyArray::toStringToLog('----');*/
      if ($field['field'] == 'basemacro') {
        $this->basemacro =  $filter['value'];
        //\MyArray::toStringToLog($this->basemacro);
      } else {
        if(!$this->isValidFilter($field, $type)) continue;
         $where->pushSub($field['field'], $filter['value'], $field['op'], $field['lop'], $type->isLower(), $type->quoted());
      }
    }
    return $where;
  }

    /**
     * @param $_field
     * @param $filter
     * @return false|mixed
     */
    private function getType($_field, $filter){
    $field = $this->field($_field)->first();

    $type = ($field == null) ? $filter['fieldtype'] : $field->getType();
    return $this->initialTypeClass($type);
  }

    /**
     * @param $filter
     * @param $type
     * @return bool
     */
    private function isValidFilter($filter, $type){
      return ($type !== false && $type->isValidOperator($filter['op']));
  }

    /**
     * @param $filter
     * @return \App\Libraries\Database\Where|void
     */
    private function __where($filter){
      $type = $this->getType($filter['field']);
      if(!$this->isValidFilter($filter, $type)) return;

    return new Where($filter['field'], $filter['value'], $filter['op'], $filter['lop'], $type->isLower(), $type->quoted());
  }

    /**
     * @param $_field
     * @return mixed
     */
    private function field($_field){
     return $this->model->getFields()->filter(function ($field) use ($_field) {
      return $field->getName() == $_field;
    });
  }

    /**
     * @param $type
     * @return false|mixed
     */
    private function initialTypeClass($type){
    /*\MyArray::toStringToLog('initialTypeClass---Start-');
    \MyArray::toStringToLog($type);*/
      if(!$this->isValidType($type)) return false;
    $__type = 'App\Libraries\Database\Types\\'.ucfirst($type).'Type';
    return new $__type();
  }


    /**
     * @return array
     */
    public function get(){
    $this->build();
    $dmdata = \DB::select($this->query, $this->generateparams());
    $dmdata = $this->model::hydrate($dmdata);
    if(!is_null($this->paginate)){
      $dmdata = \MyPage::paginateWithoutKey($dmdata, $this->paginate);
      $dmdata['from'] =$this->pagefrom;
      $dmdata['current_page'] =$this->current_page;
      $dmdata['to'] =$this->pageto;
    }
    return $dmdata;
  }

    /**
     * @param $page
     * @param $size
     * @return LengthAwarePaginator
     */
    public function Paginator($page, $size){
        $this->select->setSelectsize($page,$size);
        $this->build();
        \MyArray::toStringToLog($this->query);
        $dmdata = \DB::select($this->query, $this->generateparams());
        $dmdata = $this->model::hydrate($dmdata);
        $collect = collect($dmdata);

        $paginationData = new LengthAwarePaginator(
            $collect->forPage($page, $size),
            $collect->count(),
            $size,
            $page
        );
        return $paginationData;
    }

    /**
     * @return array
     */
    private function generateparams(){
    $array = [];
    if(!empty($this->params)) $array = array_merge($array, $this->params);
    return $array;
  }


    /**
     *
     */
    private function build(){
    $this->query = $this->select->build();
    $this->query .= $this->from->build();
    $this->condstring = $this->buildCond();
    IF (trim($this->condstring) =='AND 1 = 1') {
      $this->query .=  ' where 1 = 1 '.$this->condstring;
    }
     else {
      IF ($this->condstring !='1 = 1') {
       // $this->condstring = ' 1 = 1 '.$this->condstring;
      }
        $this->query .=  ' where '.$this->condstring;
    }
    $this->query .=' '.$this->basemacro;
     $this->query .= $this->orderBy;
    $this->query .= $this->size;
    $this->query .= $this->offset;
  }


    /**
     * @return string
     */
    private function buildCond(){
    $where = '';
    foreach($this->condition as $condition){
      $where.= $condition->build();
    }
      return $where;
  }

    /**
     * @param $type
     * @return bool
     */
    private function isValidType($type){
    return in_array($type, $this->types);
  }
}
