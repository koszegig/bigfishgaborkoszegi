<?php

namespace App\Libraries;

/**
 * Class Field
 * @package App\Libraries
 */
class Field
{
    /**
     * @var
     */
    private $name;
    /**
     * @var
     */
    private $fillable;
    /**
     * @var
     */
    private $primary;
    /**
     * @var
     */
    private $type;
    /**
     * @var mixed|null
     */
    private $cast;

    /**
     * Field constructor.
     * @param $name
     * @param $type
     * @param $fillable
     * @param $primary
     * @param null $cast
     */
    public function __construct($name, $type, $fillable, $primary, $cast=null)
  {
    $this->name = $name;
    $this->fillable = $fillable;
    $this->primary = $primary;
    $this->type = $type;
    $this->cast = $cast;
  }

    /**
     * @return mixed
     */
    public function getName() {
    return $this->name;
  }

    /**
     * @param $name
     */
    public function setName($name) {
      $this->name = $name;
  }

    /**
     * @return mixed
     */
    public function getFillable() {
      return $this->fillable;
  }

    /**
     * @param $fillable
     */
    public function setFillable($fillable) {
      $this->fillable = $fillable;
  }

    /**
     * @return mixed
     */
    public function getPrimary() {
      return $this->primary;
  }

    /**
     * @param $primary
     */
    public function setPrimary($primary) {
      $this->primary = $primary;
  }

    /**
     * @return mixed
     */
    public function getType() {
      return $this->type;
  }

    /**
     * @param $type
     */
    public function setType($type) {
      $this->type = $type;
  }

    /**
     * @return bool
     */
    public function isPrimary() {
    return $this->type == 'a_id';
    //return $this->primary == true;
  }

    /**
     * @return bool
     */
    public function isFillable() {
    return $this->fillable == true;
  }

    /**
     * @return mixed|null
     */
    public function getCast() {
    return $this->cast;
  }

    /**
     * @param $cast
     */
    public function setCast($cast) {
      $this->cast = $cast;
  }
}
