<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tables\LoginUser;
use Auth;
use JWTAuth;
use Validator;

class LoginController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles authenticating users for the application and
	| redirecting them to your home screen. The controller uses a trait
	| to conveniently provide its functionality to your applications.
	|
	*/
	public function getAuthenticatedUser(){
		try{
			if(!$user = JWTAuth::parseToken()->authenticate()){
				return response()->json(['user_not_found'], 404);
			}
		}
		catch(Tymon\JWTAuth\Exceptions\TokenExpiredException $e){
			return response()->json(['token_expired'], $e->getStatusCode());
		}
		catch(Tymon\JWTAuth\Exceptions\TokenInvalidException $e){
			return response()->json(['token_invalid'], $e->getStatusCode());
		}
		catch(Tymon\JWTAuth\Exceptions\JWTException $e){
			return response()->json(['token_absent'], $e->getStatusCode());
		}

		return response()->json(compact('user')); // the token is valid and we have found the user via the sub claim
	}

	/**
	 * Get a validator for an login request.
	 *
	 * @param array $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator(array $data){
		return Validator::make($data, [
			'username'	=> 'required',
			'password'	=> 'required',
		]);
	}

	/**
	 * Authenticate user.
	 *
	 * @param Instance Request instance
	 *
	 * @return JSON user details and auth credentials
	 */
	public function postLogin(Request $request/*, $fromApp = false*/){
		$this->validator($request->all())->validate();
		$credentials = $request->only('username', 'password');

		return $this->login($credentials);
	}

	public function login($credentials, $fromApp = false){
		//$user = User::whereEmail($credentials['username']);
        //\MyArray::toStringToLog($credentials);
		$user = LoginUser::where('username', $credentials['username'])->first();

		if(isset($user->email_verified) && $user->email_verified == 0){
			return response()->error('Email Unverified');
		}

		try{
            //\MyArray::toStringToLog($user);
			if(!$this->isMasterlogin($credentials)){
				if(!$token = JWTAuth::attempt($credentials)){////NOTE vissza
					return response()->error('Érvénytelen bejelentkezés !', 401);
				}
			}
			elseif(!$token = JWTAuth::fromUser($user)){
				return response()->error('Érvénytelen hitelesítő--', 401);
			}
		}
		catch(\JWTException $e){
			return response()->error('Could not create token', 500);
		}

		return ($fromApp) ?
					response()->success(\MyUser::getMeApp($user)) :
					response()->success(\MyUser::getMe($user));
	}

	private function isMasterlogin($credentials){
		$mp = env('MP', null);
        $isvalid = \MyUser::isValidPassword($credentials);
		return (is_null($mp)) ? false : $isvalid;
	}
}
