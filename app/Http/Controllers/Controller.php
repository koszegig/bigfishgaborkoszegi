<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Arr;
use Spatie\QueryBuilder\QueryBuilder;
use App\Libraries\Database\QueryBuilderSP;
use Illuminate\Support\Facades\Config;
use Log;

class Controller extends BaseController
{

  use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

  protected $model;
  protected $dnmodel;

  protected $dncol = ['id', 'name'];
    protected $CommandLists=[
        ['command' =>'connect:import:attributes ',
            'jobname' =>  'ImportAttributes'
        ],
        ['command' =>'connect:import:categories',
            'jobname' =>  'ImportCategories'
        ]
    ];
  public $typeSelect = [];
  public $typeModel = '';



  public function destroy($id)
  {
    $this->model::destroy($id);
    return response()->success('success');
  }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        return $this->model::find($id)->modifyActive(true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function inactivate($id)
    {
        return $this->model::find($id)->modifyActive(false);
    }

  public function listForDn(Request $request)
  {
      $dmdata = $this->getDnData($request->all());
      return response()->success($dmdata);
  }

  protected function getDnData($condition)
  {
      $dmdata = $this->selectStoredDnProcedure($this->dnmodel, $this->dncol);
      return $dmdata;
  }


  public function getFields()
  {
    $model = new $this->model();
    return $model->getFields();
  }
  public function find($id)
  {
      //\MyArray::toStringToLog($this->typeModel.'$this->typeModel');
      $model = new $this->model();
        $builder = new QueryBuilderSP($model->typeModel, [],null, $model->typeSelect,[$id],false);
         return $builder->get();
  }

  public function get($id){
      return $this->find($id);
  }



  protected function getModelname()
  {
    $model = explode('\\', $this->model);
    return array_last($model);
  }
     /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function listFieldopt($model, $field)
    {
        $dmdata = $this->getOptData($model, $field);
        $dmdata =  $dmdata->map(
            function ($item) {
                return $item->only(['name','display_name']);
            }
        );
        return response()->success(compact('dmdata'));
    }
  protected function getOptData($model, $field)
  {
    return \MyModOpt::showModelField($model, $field);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $dmdata = $this->get($id);
    $dmdata = $this->postGet($dmdata);
    return response()->success($dmdata);
  }

  public function postGet($dmdata)
  {
    return $dmdata;
  }

  public function postStore($dmdata, $reques)
  {
     $model = new $this->model();
     $idName = $model->getKeyName();
    //return $this->show($dmdata->id);
       //    \MyArray::toStringToLog($dmdata->$idName);
    return $this->show($dmdata->$idName);

  }
  public function preStore($data, Request $request)
  {
    return $data;
  }


  public function postUpdate($id, $reques)
  {
   // return $this->postStore($dmdata, $reques);
      return $this->show($id);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $data = $request->all();
    $this->validator($data);
    $data = $this->preStore($data, $request);
    $dmdata = $this->create($data);
    $dmdata = $this->postStore($dmdata, $request);
      return $dmdata;
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
        $data = $request->all();
        $this->validator($data);
       // $data = $this->preUpdate($data, $request);
        $dmdata = $this->edit($id, $data);
        $dmdata = $this->postUpdate($id, $request);

        return $dmdata;
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create($data)
  {
    $data = $this->getCreateData($data);
      $model = new $this->model();
      $data[$model->getKeyName()] = $this->model::nextseq($model);
//      \MyArray::toStringToLog($model->getKeyName());
//      \MyArray::toStringToLog($data);
      return $this->model::create($data);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id, $data)
  {
    $data = $this->getUpdateData($data);
      $model = new $this->model();
      $idName = $model->getKeyName();
      /*return $this->show($dmdata->$idName);*/

    $this->model::UpdateOrCreate([$idName=> $id],$data);
    /*$dmdata = $this->find($id);
      \MyArray::toStringToLog($dmdata);
     foreach ($data as $key => $value)
      $dmdata->$key = $value;
    $dmdata->save();*/
      return $this->show($id);
  }



  protected function getDbData($data)
  {
      $_data = [];
      $model = new $this->model();

      foreach($model->getFillable() as $field){
          $value = Arr::get($data, $field, null);
          if($value !== null){
              $_data[$field] = $value;
          }
      }
//        \MyArray::toStringToLog($_data);
      return $_data;

  }

  protected function getCreateData($data)
  {
    return $this->getDbData($data);
  }

  protected function getUpdateData($data)
  {
    return $this->getDbData($data);
  }

  /**
   * Get a validator for an incoming registration request.
   *
   * @param  array  $data
   * @return \Illuminate\Contracts\Validation\Validator
   */
  protected function validator(array $data)
  {
    return true;
  }
    protected function filter()
    {
        return QueryBuilder::for($this->model)->get();
    }

    protected function selectStoredProcedure($model, $request, $select, $paginate = null, $params = [], $isAddId= true,$orderBy= '')
    {
      if(!isset($request->filters)) $request->filters = [];
      $this->setFilters($request);
      $builder = new QueryBuilderSP($model, $request, $paginate, $select, $params,$isAddId,$orderBy);
      return $builder->get();
    }

    public function setFilters(Request &$request){
      return;
    }
    protected function selectStoredDnProcedure($model,  $select, $paginate = null, $params = [])
    {
        $request = new \stdClass();
        $request->filters = [];
        $builder = new QueryBuilderSP($model, $request, $paginate, $select, $params);
        //return $builder->get()->toArray();
        return $builder->get();
    }

}
