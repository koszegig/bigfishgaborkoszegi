<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WelcomeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('welcome');
    }

    public function generateuuid($nr = 1){
        $result = [];
        for ($i=0; $i < $nr; $i++) {
            $result[] = (string) \Uuid::generate(4);;
        }
        \MyArray::ToString($result);
    }

    public function generatehash($string){
        $string = 'Aq1Sw2De3';
        echo $string."<br>" ;
        $hash = \Hash::make($string);
        echo $hash ;
    }
}
