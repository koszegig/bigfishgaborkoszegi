<?php

namespace App\Http\Controllers;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use App\Enums\InvoiceOperationEnum as operations;
use App\ExternallyModels\threeiworks\SzmSzla;
use App\Models\Tables\User;

class TestController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // $test = SzmSzla::take(30)->orderBy('SZMSZ_L_MOD_DT','desc')->get()->toArray();
        $User = User::all()->toArray();
        \MyArray::toString($User);
    }

    public function generateuuid($nr = 1){
        $result = [];
        for ($i=0; $i < $nr; $i++) {
            $result[] = (string) \Uuid::generate(4);;
        }
        \MyArray::ToString($result);
    }

    public function generatehash($string){
        $string = 'yYubSg8`a~mQ{,*Pd!xj]9$W_$a.!';
        echo $string."<br>" ;
        $hash = \Hash::make($string);
        echo $hash ;
    }
}
