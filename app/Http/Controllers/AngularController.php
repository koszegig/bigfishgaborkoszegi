<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
 * Class AngularController
 * @package App\Http\Controllers
 */
class AngularController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('angular');
    }
}
