<?php

return [

    'title' => 'Unauthorized',
    'content' => 'You do not have permission to do this.',
    'button-home' => 'Take Me Home',
    'button-support' => 'Contact Support',
];
