<?php

return [

    'register' => 'Register',
    'name' => 'Name',
    'email' => 'Email Address',
    'password' => 'Password',
    'password-confirm' => 'Confirm Password',
    'button-register' => 'Register',

];
