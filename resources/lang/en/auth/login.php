<?php

return [

    'email' => 'Email Address',
    'password' => 'Password',
    'login' => 'Login',
    'remember' => 'Remember Me',
    'forgot' => 'Forgot Your Password?',

];
