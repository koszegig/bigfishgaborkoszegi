<?php

return [

    'title' => 'Reset Password',
    'email' => 'E-Mail Address',
    'button-reset' => 'Send Password Reset Link',

];
