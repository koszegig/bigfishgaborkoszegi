<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Role Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'user_manager' => 'User manager',
    'role_manager' => 'Role manager',
    'admin' => 'Administrator',
    'superuser' => 'Superuser',
    'permission_manager' => 'Permission manager',

];
