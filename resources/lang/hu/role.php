<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Role Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */


    'superuser' => 'Szuper felhasználó',
    'admin' => 'Adminisztrátor',
    'stage_admin' => 'Szint adminisztrátor',
    'stage_reader' => 'Szint betekintő',
    'section_admin' => 'Szekció adminisztrátor',
    'section_reader' => 'Szekció betekintő',
    'user_admin' => 'Felhasználó adminisztrátor',
    'user_reader' => 'Felhasználó betekintő',
    'position_admin' => 'Pozíció adminisztrátor',
    'position_reader' => 'Pozíció betekintő',
    'organisation_admin' => 'Szervezet adminisztrátor',
    'organisation_reader' => 'Szervezet betekintő',
    'permission_admin' => 'Engedély adminisztrátor',
    'role_admin' => 'Szerepkör adminisztrátor',
    'university_admin' => 'Egyetem adminisztrátor',
    'university_reader' => 'Egyetem betekintő',
    'faculty_admin' => 'Kar adminisztrátor',
    'faculty_reader' => 'Kar betekintő',

];
