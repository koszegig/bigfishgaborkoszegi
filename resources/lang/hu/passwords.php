<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'A jelszavad legalább 6 karakter hosszú kell legyen, és meg kell egyezzen az újra beírttal.',
    'reset' => 'Sikeresen új jelszót kértél!',
    'sent' => 'Elküldtük az új jelszó beállításához szükséges linket, nézd meg az emailed!',
    'token' => 'Érvénytelen token, ezzel nem tudunk új jelszót beállítani!',
    'user' => 'Ezzel az emailcímmel még nem regisztrált senki.',

];
