<?php

return [

    'title' => 'Nem lesz ez így jó.',
    'content' => 'Nincs meg a megfelelő jogosultságod ehhez.',
    'button-home' => 'Vissza a főoldalra',
    'button-support' => 'Írok az ügyfélszolgálatnak',
];
