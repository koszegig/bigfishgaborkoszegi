<?php

return [

    'title' => 'Hoppá - az oldal nem található!',
    'content' => 'Sajnáljuk, valami nem stimmel - térj vissza a főoldalra,
    vagy ha biztos vagy benne, hogy jó helyen kerestél, vedd fel a kapcsolatot ügyfélszolgálatunkkal!',
    'button-home' => 'Vissza a főoldalra',
    'button-support' => 'Írok az ügyfélszolgálatnak',
];
