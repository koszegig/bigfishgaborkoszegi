<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Helytelen felhasználónév/jelszó - próbáld újra!',
    'throttle' => 'Túl sokszor rontottad el a jelszavadat vagy a felhasználónevedet, :seconds másodperc múlva próbálkozhatsz újra',

];
