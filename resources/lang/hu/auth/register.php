<?php

return [

    'register' => 'Regisztráció',
    'name' => 'Név',
    'email' => 'Email cím',
    'password' => 'Jelszó',
    'password-confirm' => 'Jelszó újra',
    'button-register' => 'Regisztrálok',

];
