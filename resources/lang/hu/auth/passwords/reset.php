<?php

return [

    'title' => 'Új jelszó beállítása',
    'email' => 'Email cím',
    'password' => 'Jelszó',
    'password-confirm' => 'Jelszó újra',
    'button-reset' => 'Jelszó beállítása',

];
