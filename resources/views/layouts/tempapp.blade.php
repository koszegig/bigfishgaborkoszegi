<div role="tabpanel">
	<div  id="tempTab" class="komplexcontainer">
	  <ul class="nav nav-pills" >
		<li role="presentation" class="active"><a href="#grid" aria-controls="Táblázat" role="tab" data-toggle="tab">Adat</a></li>
		<li role="presentation"><a href="#input" aria-controls="Adat" role="tab" data-toggle="tab">Bevitel</a></li>
	  </ul>
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" data-toggle="tab" id="grid">
			@yield('tempgridcontent')
		</div>
		<div role="tabpanel" class="tab-pane" data-toggle="tab" id="input">
			@yield('tempinputcontent')
		</div>
	</div>


</div>
</div>
