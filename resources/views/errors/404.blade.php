@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 offset-md-2 text-center jumbotron">
            <h1 style="font-family: Josefin Sans,sans-serif;">
                404
            </h1>
            <h2>
                Hoppá - az oldal nem található!
            </h2>
            <br>
            <p>
                <small>
                    Sajnáljuk, valami nem stimmel - térj vissza a főoldalra,
                    vagy ha biztos vagy benne, hogy jó helyen kerestél, vedd fel a kapcsolatot ügyfélszolgálatunkkal!
                </small>
            </p>
            <hr>
            <a href="/" class="btn btn-primary">
                <span class="glyphicon glyphicon-home"></span>
                &nbsp;Vissza a főoldalra
            </a>
            <a href="mailto:404@antara.hu" class="btn btn-default">
                <span class="glyphicon glyphicon-envelope"></span>
                &nbsp;Írok az ügyfélszolgálatnak
            </a>
        </div>
    </div>
</div>
@endsection
