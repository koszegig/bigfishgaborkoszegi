@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 offset-md-2 text-center jumbotron">
            <h1 style="font-family: Josefin Sans,sans-serif;">
                403
            </h1>
            <h2>
                @lang('errors/403.title')
            </h2>
            <br>
            <p>
                <small>
                @lang('errors/403.content')
                </small>
            </p>
            <hr>
            <a href="/" class="btn btn-primary">
                <span class="glyphicon glyphicon-home"></span>
                &nbsp;@lang('errors/403.button-home')
            </a>
            <a href="mailto:403@antara.hu" class="btn btn-default">
                <span class="glyphicon glyphicon-envelope"></span>
                &nbsp;@lang('errors/403.button-support')
            </a>
        </div>
    </div>
</div>
@endsection
