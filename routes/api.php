<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$api = app('Dingo\Api\Routing\Router');
// Route::middleware('auth:api')->get('/user', function (Request $request) {
// return $request->user();
// });
$api->version('v1', function ($api) {
  $api->group(['namespace' => 'App\Http\Controllers'], function ($api) {
    $api->group(['middleware' => ['cors']], function ($api) {
      $api->group(['namespace' => 'Auth'], function ($api) {
        $api->group(['prefix' => 'auth'], function ($api) {
          $api->post('/login', 'LoginController@postLogin');
          $api->post('/login/app', 'LoginController@postLoginApp');
        });
        $api->group(['prefix' => 'user'], function ($api) {
          $api->post('/registration', 'RegisterController@register');
        });
      });
    });
// user custom apies
      $api->group(['prefix' => 'user'], function ($api) {
          $api->get('/refresh_token', 'UserController@refreshToken');
      });
    // $api->group(['middleware' => ['cors','jwt-auth']], function ($api) {
    //
    $api->group(['prefix' => 'user'], function ($api) {
      $api->get('/refresh_token', 'UserController@refresh_token');
    });


    //  });
  });
});
