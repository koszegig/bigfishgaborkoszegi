$(function(){
    var dxMenu = $("#menu").dxMenu({
        dataSource: menuData,
        hideSubmenuOnMouseLeave: false,
        displayExpr: "name",
        onItemClick: function (data) {
            var item = data.itemData;
           /* if(item.price) {
                $("#product-details").removeClass("hidden");
                $("#product-details > img").attr("src", item.iconSrc);
                $("#product-details > .price").text("$" + item.price);
                $("#product-details > .name").text(item.name);
            }*/
        }
    }).dxMenu("instance");

    var showSubmenuModes = [{
        name: "onHover",
        delay: { show: 0, hide: 500 }
    }, {
        name: "onClick",
        delay: { show: 0, hide: 300 }
    }];

    $("#show-submenu-mode").dxSelectBox({
        items: showSubmenuModes,
        value: showSubmenuModes[1],
        displayExpr: "name",
        onValueChanged: function(data) {
            dxMenu.option("showFirstSubmenuMode", data.value);
        }
    });

    $("#orientation").dxSelectBox({
        items: ["horizontal", "vertical"],
        value: "horizontal",
        onValueChanged: function(data) {
            dxMenu.option("orientation", data.value);
        }
    });

    $("#submenu-direction").dxSelectBox({
        items: ["auto", "rightOrBottom", "leftOrTop"],
        value: "auto",
        onValueChanged: function(data) {
            dxMenu.option("submenuDirection", data.value);
        }
    });

    $("#mouse-leave").dxCheckBox({
        value: false,
        text: "Hide Submenu on Mouse Leave",
        onValueChanged: function(data) {
            dxMenu.option("hideSubmenuOnMouseLeave", data.value);
        }
    });
});

var menuData = [{
    id: "1",
    name: "Video Players",
    items: [{
        id: "1_1",
        name: "HD Videto Player",
        price: 220,
        iconSrc: "https://js.devexpress.com/Demos/WidgetsGallery/JSDemos/images/products/1.png"
    }, {
        id: "1_2",
        name: "SuperHD Video Player",
        iconSrc: "https://js.devexpress.com/Demos/WidgetsGallery/JSDemos/images/products/2.png",
        price: 270
    }]
}, {
    id: "2",
    name: "Televisions",
    items: [{
        id: "2_1",
        name: "SuperLCD 42",
        iconSrc: "https://js.devexpress.com/Demos/WidgetsGallery/JSDemos/images/products/7.png",
        price: 1200
    }, {
        id: "2_2",
        name: "SuperLED 42",
        iconSrc: "https://js.devexpress.com/Demos/WidgetsGallery/JSDemos/images/products/5.png",
        price: 1450
    }, {
        id: "2_3",
        name: "SuperLED 50",
        iconSrc: "https://js.devexpress.com/Demos/WidgetsGallery/JSDemos/images/products/4.png",
        price: 1600
    }, {
        id: "2_4",
        name: "SuperLCD 55 (Not available)",
        iconSrc: "https://js.devexpress.com/Demos/WidgetsGallery/JSDemos/images/products/6.png",
        price: 1350,
        disabled: true
    }, {
        id: "2_5",
        name: "SuperLCD 70",
        iconSrc: "https://js.devexpress.com/Demos/WidgetsGallery/JSDemos/images/products/9.png",
        price: 4000
    }]
}, {
    id: "3",
    name: "Monitors",
    items: [{
        id: "3_1",
        name: "19\"",
        items: [{
            id: "3_1_1",
            name: "DesktopLCD 19",
            iconSrc: "https://js.devexpress.com/Demos/WidgetsGallery/JSDemos/images/products/10.png",
            price: 160
        }]
    }, {
        id: "3_2",
        name: "21\"",
        items: [{
            id: "3_2_1",
            name: "DesktopLCD 21",
            iconSrc: "https://js.devexpress.com/Demos/WidgetsGallery/JSDemos/images/products/12.png",
            price: 170
        }, {
            id: "3_2_2",
            name: "DesktopLED 21",
            iconSrc: "https://js.devexpress.com/Demos/WidgetsGallery/JSDemos/images/products/13.png",
            price: 175
        }]
    }]
}, {
    id: "4",
    name: "Projectors",
    items: [{
        id: "4_1",
        name: "Projector Plus",
        iconSrc: "https://js.devexpress.com/Demos/WidgetsGallery/JSDemos/images/products/14.png",
        price: 550
    }, {
        id: "4_2",
        name: "Projector PlusHD",
        iconSrc: "https://js.devexpress.com/Demos/WidgetsGallery/JSDemos/images/products/15.png",
        price: 750
    }]
}];
