var komplexgrid = (function () {
/*		var initEvents = [
		//Add rule
		{
			  event	: "click"
			, target: ".toggleClassActivity"
			, fn	: function () { toggleClassActivity($(this).data('taskid')); }
		},
		//Tab click
		{
			  event	: "click"
			, target: ".Polaris-Tabs__Tab"
			, fn	: function () { syncee.tab($(this)); }
		}

	];*/
    var  masterGridHeight=500;
    var  masterGridFormHeight=130;
    var  gridView = null;
	return {
		  init					: init
		, post					: post
		, get					: get
        , loaded                : loaded
        , loading               : loading
		, getadatok				: getadatok
		, getgridorders			: getgridorders
		, setgridDataSource		: setgridDataSource
		, setmastergrid			: setmastergrid
		, setmasterdetailgrid	: setmasterdetailgrid
        , setmasterGridHeight   : setmasterGridHeight
        , setFormHeight         : setFormHeight
		, getProcessFilter		: getProcessFilter
		, settempgrid			: settempgrid
		, actclose			    : actclose
		, gridView			    : gridView
		, getGridView			: getGridView
	}
		function init () {

/*		$(document).ready(function () {
			setTimeout(function() {
				syncee.capture();
			}, 1000);
			$('[tipsy]').tipsy({fade: true, gravity: 's', title: 'tipsy'});
		});
*/
		/*** SET LISTENERS ***/
	/*	initEvents.forEach(function (ev) {
			$(document).on(ev.event, ev.target, ev.fn);
		});*/
		/*** SET LISTENERS ***/
        loaded();
	}
	function actclose () {
	 	$("#menusor").show();
        window.location.href = '/';
	}

	function destruct () {
	 /*	initEvents.forEach(function (ev) {
			$(document).off(ev.event, ev.target);
		});*/

	}
	function loaded  () { $('.ajax-loader').hide(); }
	function loading () { $('.ajax-loader').show(); }
    function setmasterGridHeight (GridHeight) {
        masterGridHeight=GridHeight;
    }
    function setFormHeight (FormHeight) {
        masterGridFormHeight=FormHeight;
    }
	function get (endpoint) {

		var ajaxObj = {
			  url		: PAGE_URL + endpoint
			, type		: 'GET'
			, dataType	: 'json'
		};

		return request(ajaxObj)

	}

	function post (endpoint, data) {

		var ajaxObj = {
			  url		: PAGE_URL + endpoint
			, data		: data
			, type		: 'POST'
			, dataType	: 'json'
		};

		return request(ajaxObj);

	}

	function request (ajaxObj) {

		loading();
		var deferred = Q.defer();

		$.ajax(ajaxObj)
		.done(function (data) {
			if (data.isSuccess !== true) {
				if (data.messages && data.messages.length > 0) {
					data.messages.forEach(function(message) {
						toastr_notify('error', message, { timeOut: 5000 });
					});
				}
				deferred.reject(data.messages || []);
			}
			else {
				deferred.resolve(data.data, data);
			}
			loaded();
		})
		.fail(function (e, msg) {
			loaded();
			if (msg && msg === 'parsererror') {
				toastr_notify('error', 'PHP error! Malformed JSON response!', { timeOut: 10000 });
			}
			deferred.reject(['PHP error! Malformed JSON response!']);
		});

		return deferred.promise;
	}

 function getColumnFieldName(dataGridInstance, getter) {
    var column,
        i;

    if($.isFunction(getter)) {
        for(i = 0;i < dataGridInstance.columnCount() ;i++) {
            column = dataGridInstance.columnOption(i);
            if(column.selector === getter) {
                return column.dataField;
            }
        }
    }
    else {
        return getter;
    }
};

	function loaded  () { $('.ajax-loader').hide(); }
	function loading () { $('.ajax-loader').show(); }
	function escape (str) {
		if (typeof str === 'string') {
			return str.replace(/"/g, '&quot;');
		}
		else return str;
	}

	function escapeHTML(html) {
		var encode = function(tag) {
			var charsToReplace = {'&': '&amp;',
								  '<': '&lt;',
								  '>': '&gt;',
								  '"': '&#34;'};

			return charsToReplace[tag] || tag;
		}

		return html.replace(/[&<>"]/g, encode);
	}
	function isJson(str) {
		try {
			JSON.parse(str);
		} catch (e) {
			return false;
		}
		return true;
	}
	function stripHTML (html) {

		var tmp = document.createElement("DIV");
		tmp.innerHTML = html;
		tmp = tmp.textContent || tmp.innerText || "";

		return tmp;

	}
 function getColumnFieldtype(dataGridInstance, getter) {
    var column,
        i;

    if($.isFunction(getter)) {
        for(i = 0;i < dataGridInstance.columnCount() ;i++) {
            column = dataGridInstance.columnOption(i);
            if(column.selector === getter) {
                return column.dataType;
            }
        }
    }
    else {

        return getter;
    }
};
function getProcessFilter(gridContainer) {
    var dataGrid = $(gridContainer).dxDataGrid("instance");
    var filter = dataGrid.getCombinedFilter();
    var gridFilterText = '';
    var prefix = '';
    var soffix = '';
    var filtersort = 0;

    function processFilter(dataGridInstance, filter) {
        if (!($.isArray(filter))) {

                 if (filter != null) {
                         prefix = "lower(trim('%";
                        soffix = "::public.a_vc_254))";
                     if (filtersort==2) {
                         filter = prefix+filter.replace(' ','%')+"%'" + soffix;
                         filtersort=3;
                     }
                     if (filtersort==1) {
                         if (filter == 'contains' || filter == 'startswith' || filter == 'endswith' ) {
                             filter=' LIKE ';
                         }

                         if (filter == 'notcontains') {
                             filter=' NOT LIKE ';
                         }

                         filtersort=2;
                     }

                     if (filtersort==0) {
                         filter = "lower(trim("+filter + soffix;
                         filtersort=1;
                     }
                     if (filtersort==3) {

                         filtersort=3;
                     }

                    } else {
                        prefix = '';
                        soffix = '';
                    }

            gridFilterText = gridFilterText + filter;
        }
        else {
            gridFilterText = gridFilterText + '(';
        }
        if ($.isArray(filter)) {
            if ($.isFunction(filter[0])) {

                filter[0] = getColumnFieldName(dataGridInstance, filter[0]);
            }
            else {
                for (var i = 0; i < filter.length; i++) {
                    processFilter(dataGridInstance, filter[i]);
                    }
                }
            }
        if (($.isArray(filter))) {
            //alert( getColumnFieldtype(dataGridInstance, filter[0]));

            gridFilterText =gridFilterText+ ')';
        }
    }

    processFilter(dataGrid, filter);
//alert(gridFilterText);
    return gridFilterText;
}
function getadatok(mezolista,columns) {

    jQuery.ajaxSetup({async:false});
     $.get("/mezoleiras/"+ mezolista+"/json")
          .done(function( datas ) {

            var fields =JSON.parse(datas)
            var column;
            var data;
            for (column of columns) {
                for (data of fields) {
                 if (data.datafield===column.dataField) {
                     column.caption=data.caption;
                 }

                }
            }
            return columns;
            });
    columns.unshift({
            allowFiltering: true,
            visible: false,
            filterType: 'exclude'
        });
    jQuery.ajaxSetup({async:true});

    //alert(JSON.stringify(columns));
    //alert(mezolista);
    //alert(JSON.stringify(columns));
    return columns;
};
function getgridorders(gridContainer,dataurl,gridKey = '') {
    return  new DevExpress.data.CustomStore({
        load: function (loadOptions) {
            var deferred = $.Deferred(),
                args = {};

            var dataGrid = $(gridContainer).dxDataGrid("instance");
            var filter = dataGrid.getCombinedFilter();
            var j = 0;
            //alert(loadOptions.filter);
            //processFilter(dataGrid, filter, fText);

           // console.log(getProcessFilter());

            var filterOptions = loadOptions.filter ? loadOptions.filter.join(",") : "";
            var sortOptions = loadOptions.sort ? JSON.stringify(loadOptions.sort) : "";  //Getting sort options

            if (filterOptions != null) {
              // alert(filterOptions);
            };

            if (loadOptions.sort) {
                var sortOptions = loadOptions.sort; // ? JSON.stringify(loadOptions.sort) : "";

                var orderby = '';
                if (sortOptions != null) {

                    for (var i = 0; i < sortOptions.length; i++) {
                        var obj = sortOptions[i];
                        var order = '';
                       // console.log(obj);

                        order = obj.selector;
                        if (obj.desc)
                            order = order + ' desc';

                        if (orderby.length > 0)
                            orderby = orderby + ', ';
                        orderby = orderby + order;
                    }
                    ;
                    //console.log(orderby);

                    args.orderby = orderby;
                }
                ;
            }

            args.skip = loadOptions.skip || 0;
            args.take = loadOptions.take || 24;
            args.filter = getProcessFilter(gridContainer) || '';

            $.getJSON(dataurl, {
                args : args,
                // ...
            }).done(function(result) {
                deferred.resolve(result.items, {
                    totalCount: result.totalCount
                });
            }).fail(deferred.reject);
            return deferred.promise();
        },
      /* update: function (key, values) {
                //komplexinput.formformput(dataurl + "/" + encodeURIComponent(key), postTetelData, function(result){
                komplexinput.formformput(dataurl + "/" + $(gridKey).val()+'/PUT', values, function(result){
                        //deferred.resolve({next: true});
                    var dataGrid = $(gridContainer).dxDataGrid("instance");
                    dataGrid.refresh();
                        deferred.resolve(key);
                    });

        return deferred.resolve(key);
        }*/
       update: function (key, values) {
        var deferred = $.Deferred();
        $.ajax({
            //url: dataurl + "/" + encodeURIComponent(key),
            url: dataurl + "/" + $(gridKey).val()+'/PUT',
            method: "PUT",
            data: values
        }).done(function(){
            var dataGrid = $(gridContainer).dxDataGrid("instance");
            dataGrid.refresh();
            deferred.resolve(key);
        });
        return deferred.resolve(key);
        },
    insert: function (values) {
        var deferred = $.Deferred();
        /*return $.ajax({
            url: "http://mydomain.com/MyDataService/",
            method: "POST",
            data: values
        })*/
        return deferred.resolve(values);
    },
    remove: function (key) {
        var deferred = $.Deferred();
        /*return $.ajax({
            url: "http://mydomain.com/MyDataService/" + encodeURIComponent(key),
            method: "DELETE",
        })*/
        return deferred.resolve(key);
    }
    });
}
function setgridDataSource(dataurl) {
  return {
        load: function (loadOptions) {
            var d = $.Deferred();
            $.getJSON(dataurl, {
                groupConfig: loadOptions.group,
                // ...
            }).done(function(result) {
                d.resolve(result.items, {
                    totalCount: result.totalCount
                });
            }).fail(d.reject);
            return d.promise();
        }
    };
}
function setmasterdetailgrid(gridContainer,orders,mastercolums,detailcolumns,mastersummery,detailsummary,caninsert,canmodify,candelete,
                              starteditevent = function(selectedItems) {
                                   //         var data = selectedItems.selectedRowsData[0];
            //alert(JSON.stringify(data));
        },selectedevent=function(selectedItems) {
                                          //  var data = selectedItems.selectedRowsData[0];
            //alert(JSON.stringify(data));
        },
        paramcolumnhidding=true,
        insert=function(e) {

                },
        deleted=function(e) {
                    //DevExpress.ui.notify("Delteidn", "info", 2000);
                    // logEvent("RowRemoving");
                    }
        ,masterForm= {}) {
        return     $(gridContainer).dxDataGrid({
        dataSource: {store: orders},
        //dataSource: gridDataSource,
        columnHidingEnabled: paramcolumnhidding,
        remoteOperations: {
            filtering: true,
            sorting: true,
            paging: true
        },
         groupPanel: {
            visible: true
        },
        allowColumnResizing: true,
        allowColumnReordering: true,
        columnAutoWidth: true,
        allowColumnReordering: true,
        showBorders: true,
        grouping: {
            autoExpandAll: true,
        },
        columnChooser: {
            enabled: true,
            emptyPanelText: 'Húzza ide az oszlopokat a csoportosításhoz'
        },
        loadPanel: {
            height: 150,
            width: 400,
            text: 'Adatok betöltése...'
        },
		noDataText: 'Nincs adat',

        paging: {
            pageSize: 24
        },masterDetail: {
            enabled: true,
            template: function(container, options) {
                var currentEmployeeData = options.data;
                container.addClass("internal-grid-container");
                $("<div>").text("' Tételei:").appendTo(container);
                $("<div>")
                    .addClass("internal-grid")
                    .dxDataGrid(
                        {
                                sorting: { mode: 'multiple' },
                        filterRow: {
                            visible: true,
                                applyFilter: "auto"
                        },
                        headerFilter: {
                            visible: true
                        },
                        selection: {
                            mode: 'single'
                        },
                         summary: detailsummary,
                        columnAutoWidth: true,
                        columns:detailcolumns,
                        dataSource: currentEmployeeData.detail
                    }).appendTo(container);
            }
        },
        height: masterGridHeight,
        pager: {
            showNavigationButtons: true,
            //showPageSizeSelector: true,
            //allowedPageSizes: [8, 12, 20]
        },
        sorting: { mode: 'multiple' },
        filterRow: {
            visible: true,
            applyFilter: "auto"
        },
        headerFilter: {
            visible: true
        },
        selection: {
            mode: 'single'
        },editing: {
            mode: "form",
            form:masterForm, /*{
                /*maxColWidth: 170,
                colCount: 5,
                hoverStateEnable: true,
                items: [{
                    itemType: "group",
                    caption: "Megnevezés",
                    items: ["felh_userid", "felh_prt_nev"]
                }, {
                    itemType: "group",
                    caption: "Cím",
                        items: [{
                            itemType: "group",
                            caption: "Helység",
                            items: ["helys_irsz", "helys_nev"]
                        },{
                        dataField: "prt_utca"
                    }]
                    }],
                onFieldDataChanged: function () {
                  //  DevExpress.ui.notify("Az adatok megváltoztak", "info", 2000);
                }

            },*/
            allowAdding: caninsert==1,
            allowDeleting: candelete==1,
            allowUpdating: canmodify==1,
            height: masterGridFormHeight,
            colCount: 3,
            texts: {
                addRow: "Új",
                cancelRowChanges: "Mégse",
                confirmDeleteMessage: "Biztos, hogy törli az adatot?",
                confirmDeleteTitle: "Törli?",
                deleteRow: "Töröl",
                editRow: "Szerkeszt",
                saveRowChanges: "Mentés"
            }
        },
        //columnHidingEnabled: true,
        onSelectionChanged:selectedevent,
        onEditingStart: starteditevent,
        onInitNewRow: function(e) {
            //logEvent("InitNewRow");
        },
        onRowInserting: function(e) {
            //logEvent("RowInserting");
        },
        onRowInserted: function(e) {
            //logEvent("RowInserted");
        },
        onRowUpdating: function(e) {
            //logEvent("RowUpdating");
        },
        onRowUpdated: function(e) {
            //logEvent("RowUpdated");
        },
        onRowRemoving: deleted,
        onRowRemoved: function(e) {
            //logEvent("RowRemoved");
        },
        searchPanel: { visible: true,
                      width: 240,
            placeholder: "Keresés..." },
        columns: mastercolums,
       // summary: mastersummery,
        columnAutoWidth: true,
        hoverStateEnabled: true
    }).dxDataGrid("instance");

}
function setmastergrid(gridContainer,orders,mastercolums,caninsert,canmodify,candelete,starteditevent,selectedevent, paramcolumnhidding=true,insert=function(e) {

                },deleted=function(e) {
                    DevExpress.ui.notify("Delteidn", "info", 2000);
                    // logEvent("RowRemoving");
                    },mastersummery = null,masterForm= {},
                      setonKeyDown =function(e) {
        var selKey = e.component.getSelectedRowKeys();
            if (selKey.length) {
                var currentKey = selKey[0];
                var index = e.component.getRowIndexByKey(currentKey);
                console.log(e.jQueryEvent.keyCode);
                if (e.jQueryEvent.keyCode == 38) {
                    index--;
                    if (index >= 0) {
                        e.component.selectRowsByIndexes([index]);
                        e.jQueryEvent.stopPropagation();
                    }
                }
                else if (e.jQueryEvent.keyCode == 40) {
                    index++;
                    e.component.selectRowsByIndexes([index]);
                    e.jQueryEvent.stopPropagation();
                }
            }
        }) {
    return     $(gridContainer).dxDataGrid({
        dataSource: {store: orders},
        selection: {
            mode: "single"
        },
        columnHidingEnabled: paramcolumnhidding,
        //dataSource: gridDataSource,
        remoteOperations: {
            filtering: true,
            sorting: true,
            paging: true
        },
         groupPanel: {
            visible: true
        },
        "export": {
            enabled: true,
            fileName: "export",
            allowExportSelectedData: true
        },
        allowColumnResizing: true,
        allowColumnReordering: true,
        columnAutoWidth: true,
        allowColumnReordering: true,
        showBorders: true,
        showColumnLines: true,
        showRowLines: true,
        rowAlternationEnabled: true,
        wordWrapEnabled: true,
        activeStateEnabled: true,
        columnHidingEnabled: true,
        focusStateEnabled: true,
        cacheEnabled: true,
        grouping: {
            autoExpandAll: true,
            emptyPanelText: "Húzza ide az oszlopokat a csoportosításhoz",
        },
        columnChooser: {
            "allowSearch": true,
            "emptyPanelText": "Dobd ide amit el tüntetsz",
            "enabled": true,
            "title": "Oszlop ne legyen benne"
        },
        loadPanel: {
            height: 150,
            width: 400,
            text: 'Adatok betöltése...'
        },
		noDataText: 'Nincs adat',
        paging: {
            pageSize: 2000
        },
        height: masterGridHeight,
        pager: {
            //showPageSizeSelector: true,
            //allowedPageSizes: [8, 12, 20]
        },
        sorting: { mode: 'multiple' },
        filterRow: {
            visible: true,
            applyFilter: "auto",
             "applyFilterText": "Szűrő aktualizálása",
                "betweenEndText": "Vége",
                "betweenStartText": "Kezdete",
                "operationDescriptions": {
                   "between": "Kettő között",
                    "contains": "Tartalmazza",
                    "endsWith": "Végével",
                    "equal": "Egyenlő",
                    "greaterThan": "Nagyobb",
                    "greaterThanOrEqual": "Nagyobb egyenlő",
                    "lessThan": "Kisebb",
                    "lessThanOrEqual": "Kisebb egyenlő",
                    "notContains": "Nem tartalmazza",
                    "notEqual": "Nem egyenlő",
                    "startsWith": "Kezdődik"
                },
                "resetOperationText": "Alapállapot (reset)",
                "showAllText": "",
        },
        headerFilter: {
            visible: true
        },
        selection: {
            mode: 'single'
        },editing: {
            mode: "form",
            form:masterForm, /*{
                /*maxColWidth: 170,
                colCount: 5,
                hoverStateEnable: true,
                items: [{
                    itemType: "group",
                    caption: "Megnevezés",
                    items: ["felh_userid", "felh_prt_nev"]
                }, {
                    itemType: "group",
                    caption: "Cím",
                        items: [{
                            itemType: "group",
                            caption: "Helység",
                            items: ["helys_irsz", "helys_nev"]
                        },{
                        dataField: "prt_utca"
                    }]
                    }],
                onFieldDataChanged: function () {
                  //  DevExpress.ui.notify("Az adatok megváltoztak", "info", 2000);
                }

            },*/
            allowAdding: caninsert==1,
            allowDeleting: candelete==1,
            allowUpdating: canmodify==1,
            height: masterGridFormHeight,
            colCount: 3,
            texts: {
                addRow: "Új",
                cancelRowChanges: "Mégse",
                confirmDeleteMessage: "Biztos, hogy törli az adatot?",
                confirmDeleteTitle: "Törli?",
                deleteRow: "Töröl",
                editRow: "Szerkeszt",
                saveRowChanges: "Mentés"
            }
        },
        //columnHidingEnabled: true,
      /*  onSelectionChanged: function (selectedItems) {
            var data = selectedItems.selectedRowsData[0];
            //alert(JSON.stringify(data));
        }*/
        onSelectionChanged:selectedevent,
        /*editing: {
        mode: "cell",
        allowUpdating: true
    },
         onCellClick: function(e){
      if(e.rowType == "data" && e.key)
         e.component.selectRows([e.key]);
    },*/
        onEditingStart:starteditevent,
        onInitNewRow: function(e) {
          //  DevExpress.ui.notify("InitNewRo22w", "info", 2000);
            //logEvent("InitNewRow");
        },
        onRowInserting: function(e) {
            //DevExpress.ui.notify("RowInse11rting", "info", 2000);
            //logEvent("RowInserting");
        },
        onRowInserted: function(e) {
            //DevExpress.ui.notify("RowInserti33ng", "info", 2000);
            //logEvent("RowInserted");
        },
        onRowUpdating: function(e) {
        //    DevExpress.ui.notify("Módosításkor"+e, "info", 2000);
        //    logEvent("RowUpdating");
        },
        onRowUpdated: function(e) {
        //    DevExpress.ui.notify("RowInser55555555555ting", "info", 2000);
        //   logEvent("RowUpdated");
        },
        onRowRemoving: deleted,
        /*onRowRemoving: function(e) {
            DevExpress.ui.notify("Delteidn", "info", 2000);
           // logEvent("RowRemoving");
        },*/
        onKeyDown : setonKeyDown,
        onRowRemoved: function(e) {
         //   DevExpress.ui.notify("onRowRemoved", "info", 2000);
        //    logEvent("RowRemoved");
        },
        searchPanel: { visible: true,
                      width: 240,
            placeholder: "Keresés..." },
        columns: mastercolums,
        columnAutoWidth: true,
        /*onSelectionChanged: function (selectedItems) {
            var data = selectedItems.selectedRowsData[0];
            alert(JSON.stringify(data));
        },*/
        summary: mastersummery,
        /*
        var mastersummery ={
            totalItems: [{
                column: "bizf_id",
                summaryType: "count",
				displayFormat: "Tételszám: {0}"
            }, {
                column: "bizf_netto_ert",
                summaryType: "sum",
				displayFormat: "Összesen: {0}"

            },{
                column: "bizf_fizetendo_brutto_ert",
                summaryType: "sum",
				displayFormat: "Összesen: {0}"

            }]
        };
         summary: {
            groupItems: [{
                column: "OrderNumber",
                summaryType: "count",
                displayFormat: "{0} orders",
            }, {
                column: "SaleAmount",
                summaryType: "max",
                valueFormat: "currency",
                showInGroupFooter: false,
                alignByColumn: true
            }, {
                column: "TotalAmount",
                summaryType: "max",
                valueFormat: "currency",

    'currency'
    'fixedPoint'
    'percent'
    'decimal'
    'exponential'
    'largeNumber'
    'thousands'
    'millions'
    'billions'
    'trillions'


                showInGroupFooter: false,
                alignByColumn: true
            }, {
                column: "TotalAmount",
                summaryType: "sum",
                valueFormat: "currency",
                displayFormat: "Total: {0}",
                showInGroupFooter: true
            }]
        */
        hoverStateEnabled: true
    }).dxDataGrid("instance");
}
function settempgrid(gridContainer,orders,mastercolums,caninsert,canmodify,candelete,seteditmode,starteditevent,editform) {
    return     $(gridContainer).dxDataGrid({
        dataSource: {store: orders},
        //dataSource: gridDataSource,

        remoteOperations: {
            filtering: true,
            sorting: true,
            paging: true
        },
         groupPanel: {
            visible: true
        },
        "export": {
            enabled: true,
            fileName: "export",
            allowExportSelectedData: true
        },
        allowColumnResizing: true,
        allowColumnReordering: true,
        grouping: {
            autoExpandAll: true,
        },
        columnChooser: {
            enabled: true,
            emptyPanelText: 'Húzza ide az oszlopokat a csoportosításhoz'
        },
        loadPanel: {
            height: 150,
            width: 400,
            text: 'Adatok betöltése...'
        },
		noDataText: 'Nincs adat'
		,
        paging: {
            pageSize: 24
        },
        height: masterGridHeight,
        pager: {
            //showPageSizeSelector: true,
            //allowedPageSizes: [8, 12, 20]
        },
        sorting: { mode: 'multiple' },
        filterRow: {
            visible: true,
            applyFilter: "auto"
        },
        headerFilter: {
            visible: true
        },
        selection: {
            mode: 'single'
        },editing: {
            mode: seteditmode,
            form:editform,
            allowAdding: caninsert==1,
            allowDeleting: candelete==1,
            allowUpdating: canmodify==1,
            height: 540,
            colCount: 3,
            texts: {
                addRow: "Új",
                cancelRowChanges: "Mégse",
                confirmDeleteMessage: "Biztos, hogy törli az adatot?",
                confirmDeleteTitle: "Törli?",
                deleteRow: "Töröl",
                editRow: "Szerkeszt",
                saveRowChanges: "Mentés"
            }
        },
        //columnHidingEnabled: true,
        onSelectionChanged: function (selectedItems) {
            var data = selectedItems.selectedRowsData[0];
            //alert(JSON.stringify(data));
        },/*editing: {
        mode: "cell",
        allowUpdating: true
    },
         onCellClick: function(e){
      if(e.rowType == "data" && e.key)
         e.component.selectRows([e.key]);
    },*/
        onEditingStart:starteditevent,
        onInitNewRow: function(e) {
            DevExpress.ui.notify("InitNewRow", "info", 2000);
            //logEvent("InitNewRow");
        },
        onRowInserting: function(e) {
            DevExpress.ui.notify("RowInserting", "info", 2000);
            //logEvent("RowInserting");
        },
        onRowInserted: function(e) {
            DevExpress.ui.notify("RowInserting", "info", 2000);
            //logEvent("RowInserted");
        },
        onRowUpdating: function(e) {
            DevExpress.ui.notify("RowInserting", "info", 2000);
           // logEvent("RowUpdating");
        },
        onRowUpdated: function(e) {
            DevExpress.ui.notify("RowInserting", "info", 2000);
           // logEvent("RowUpdated");
        },
        onRowRemoving: function(e) {
            DevExpress.ui.notify("RowInserting", "info", 2000);
           // logEvent("RowRemoving");
        },
        onRowRemoved: function(e) {
            DevExpress.ui.notify("RowInserting", "info", 2000);
            //logEvent("RowRemoved");
        },

        searchPanel: { visible: true,
                      width: 240,
            placeholder: "Keresés..." },
        columns: mastercolums,
        columnAutoWidth: true,
        onSelectionChanged: function (selectedItems) {
            var data = selectedItems.selectedRowsData[0];
            //alert(JSON.stringify(data));
        },
        hoverStateEnabled: true
    }).dxDataGrid("instance");
}
function getGridView (menu_id) {
         var inputObj = {
             'menu_id'	: menu_id
         };
        jQuery.ajaxSetup({async:false});
		komplexinput.post('/gridview', inputObj, function(result){
                gridView = result;
			});
        jQuery.ajaxSetup({async:true});
        return gridView;
	}
})();

var komplexinput = (function () {
	/*		var initEvents = [
		//Add rule
		{
			  event	: "click"
			, target: ".toggleClassActivity"
			, fn	: function () { toggleClassActivity($(this).data('taskid')); }
		},
		//Tab click
		{
			  event	: "click"
			, target: ".Polaris-Tabs__Tab"
			, fn	: function () { syncee.tab($(this)); }
		}

	];*/
    var alapertekek = null;
	return {
		  init					: init
		, destruct				: destruct
		, loaded				: loaded
		, loading				: loading
		, post					: post
		, formpost				: formpost
		, formput				: formput
		, getPostData			: getPostData
		, get					: get
		, deleted				: deleted
        , alert					: alert
        , loaded                : loaded
        , loading               : loading
        , isUndefined           : isUndefined
        , actclose			    : actclose
        , in_array			    : in_array
        , getalapertek          : getalapertek
        , array_search			: array_search

	}
		function init () {

/*		$(document).ready(function () {
			setTimeout(function() {
				syncee.capture();
			}, 1000);
			$('[tipsy]').tipsy({fade: true, gravity: 's', title: 'tipsy'});
		});
*/
		/*** SET LISTENERS ***/
	/*	initEvents.forEach(function (ev) {
			$(document).on(ev.event, ev.target, ev.fn);
		});*/
            $('.form_datetime').datetimepicker({
                        language:  'hu',
                        autoclose: true,
                        pickerPosition: "bottom-right",
                        weekStart: 1,
                        todayBtn:  1,
                        autoclose: 1,
                        todayHighlight: 1,
                        startView: 2,
                        forceParse: 0,
                        showMeridian: 1

                });
            $('.form_date').datetimepicker({
                    language:  'hu',
                    autoclose: true,
                    pickerPosition: "bottom-right",
                    weekStart: 1,
                    todayBtn:  1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    minView: 2,
                    forceParse: 0

                });
            $('.form_time').datetimepicker({
                language:  'hu',
                weekStart: 1,
                todayBtn:  1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 1,
                minView: 0,
                maxView: 1,
                forceParse: 0

                });


		/*** SET LISTENERS ***/
 //       loaded();
	}
	function getalapertek () {
         var inputObj = {};
        jQuery.ajaxSetup({async:false});
		komplexinput.post('/alapertekek', inputObj, function(result){
                alapertekek = result;
               // console.log(alapertekek.jovedekitermekkezeles);
			});
        jQuery.ajaxSetup({async:true});
        return alapertekek;
	}

	function destruct () {
	 	initEvents.forEach(function (ev) {
			$(document).off(ev.event, ev.target);
		});

	}
	function actclose () {
	 	$("#menusor").show();
        $('.navbar').show();
        $("#Actclose").hide();
        window.location.href = '/';
	}
	function loaded  () {  $("#Actclose").show();  $('.ajax-loader').hide(); }
	function loading () { $('.ajax-loader').show(); $("#Actclose").show();  }
	function get (endpoint,cb) {

		var ajaxObj = {
			  //url		: PAGE_URL + endpoint
			  url		: endpoint
			, type		: 'GET'
			, dataType	: 'json'
		};

		return request(ajaxObj,cb)

	}
	function deleted (endpoint,cb) {

		var ajaxObj = {
			  //url		: PAGE_URL + endpoint
			  url		: endpoint
			, type		: 'DELETE'
			, dataType	: 'json'
		};

		return request(ajaxObj,cbs)

	}

    function formpost (endpoint, data,cb) {
        var ajaxObj = {
			  //url		: baseurl + endpoint
			  url		: endpoint
			, data		: data
			, type		: 'POST'
			,cache: false
    		,contentType: false
    		,processData: false
			//, dataType	: 'json'
		};
		/*var ajaxObj = {
			  url		: PAGE_URL + endpoint
			, data		: data
			, type		: 'POST'
			, dataType	: 'json'
		};*/

		return request(ajaxObj,cb);

	}
    function formput (endpoint, data,cb) {
        var ajaxObj = {
			  //url		: baseurl + endpoint
			  url		: endpoint
			, data		: data
			, type		: 'PUT'
			,cache: false
    		,contentType: false
    		,processData: false
			//, dataType	: 'json'
		};
		/*var ajaxObj = {
			  url		: PAGE_URL + endpoint
			, data		: data
			, type		: 'POST'
			, dataType	: 'json'
		};*/

		return request(ajaxObj,cb);

	}

	function post (endpoint, data,cb,setloading=true) {
        var ajaxObj = {
			  url		: endpoint
			, data		: data
			, type		: 'POST'
			, dataType	: 'json'
		};

		return request(ajaxObj,cb,setloading);

	}

	function request (ajaxObj,cb,setloading=true) {
        if (setloading) {
            loading();
        }

		//var deferred =  $.Deferred();
		var deferred =  Q.defer();
        jQuery.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })

		$.ajax(ajaxObj)
		.done(function (data) {
			if (data.isSuccess !== true) {
				if (data.messages && data.messages.length > 0) {
					data.messages.forEach(function(message) {
						toastr_notify('error', message, { timeOut: 5000 });
					});
				}
				deferred.reject(data.messages || []);
			}
			else {
                data.messages.forEach(function(message) {
                    toastr_notify('info', message, { timeOut: 5000 });
				});
                if (cb) {
					cb(data.data, data);
				}
                //console.log(data.data);
				deferred.resolve(data);
			}
            if (setloading) {
			 loaded();
            }
		})
		.fail(function (e, msg) {
			loaded();
			if (msg && msg === 'parsererror') {
				toastr_notify('error', 'Error!  response!', { timeOut: 10000 });
			}
            jQuery.ajaxSetup({async:true});
			deferred.reject(['Error! Malformed JSON response!']);
		});
		return deferred.promise;
	}
	function getPostData (formid) {
        var postData = new FormData($(formid)[0]);
		return postData;

	}
    function alert (title, message) {

		swal({
            title: title,
            text: message,
            type: "info",
        });

	}
    function isUndefined(value){
        // Obtain `undefined` value that's
        // guaranteed to not have been re-assigned
        var undefined = void(0);
        return value === undefined;
    }
    function arrayCompare(a1, a2) {
        if (a1.length != a2.length) return false;
        var length = a2.length;
        for (var i = 0; i < length; i++) {
            if (a1[i] !== a2[i]) return false;
        }
        return true;
    }

    function in_array(needle, haystack) {
        var length = haystack.length;
        for(var i = 0; i < length; i++) {
            if(typeof haystack[i] == 'object') {
                if(arrayCompare(haystack[i], needle)) return true;
            } else {
                if(haystack[i] == needle) return true;
            }
        }
        return false;
    }
    function array_search(needle, haystack) {
        for(var i in haystack) {
            if(haystack[i] == needle) return i;
        }
        return false;
    }
	})();
