 angular.module('inputcikkService', [])

.factory('Comment', function($http) {

    return {
        // get all the comments
        get : function() {
            return $http.get('/onecikk');
        },

        // save a comment (pass in comment data)
        save : function(CikData) {
            return $http({
                method: 'POST',
                url: '/api/cikk',
                headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                data: $.param(CikData)
            });
        },

        // destroy a comment
        destroy : function(id) {
            return $http.delete('/api/cikk/' + id);
        }
    }

});
