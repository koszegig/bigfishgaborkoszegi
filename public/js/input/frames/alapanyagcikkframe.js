
$(function(){
//	getMezoadatok('t_cikkek_1');
	var alapanyagcikkMezoadatok = komplexgrid.getadatok('t_tmpalapanyagcikktmp_1',[
            { dataField: 'tblk_tmp_id', caption: 'vonal_tmp_id', enable: false},
            { dataField: 'cikkekalap_cikk_kap_cik_kod', caption: 'kap_cik_kod'},
            { dataField: 'cikkekalap_cikk_kap_cik_nev', caption: 'kap_cik_nev'},
            { dataField: 'cikkekalap_cikk_kap_mennyiseg', caption: 'tblk_kapcsolodo_count'},
            { dataField: 'cikkekalap_cikk_kap_cik_me_rovidites', caption: 'kap_cik_me_rovidites'}
			]);
    		var alapanyagcikkorders = komplexgrid.getgridorders('#gridContainerAlapanyagcikk','/allframecikkalapanyag');
			var gridDataSource = komplexgrid.setgridDataSource('/allframecikkalapanyag');
    var starteditevent=function(e) {
			DevExpress.ui.notify("EditingStart-->"+e.data.vonal_tmp_id, "info", 2000);

            //logEvent("EditingStart");
        }
    var editform = {
                maxColWidth: 170,
                colCount: 5,
                hoverStateEnable: true,
                items: [{
                    itemType: "group",
                    caption: "Meccccgnevezés",
                    items: ["felh_userid", "felh_prt_nev"]
                }, {
                    itemType: "group",
                    caption: "Cím",
                        items: [{
                            itemType: "group",
                            caption: "Helység",
                            items: ["helys_irsz", "helys_nev"]
                        },{
                        dataField: "vonal_azon"
                    }]
                    }],
                onFieldDataChanged: function () {
                  //  DevExpress.ui.notify("Az adatok megváltoztak", "info", 2000);

                }

            };
     var mastercikkcsoportgrid = komplexgrid.settempgrid('#gridContainerAlapanyagcikk',alapanyagcikkorders, alapanyagcikkMezoadatok,1,1,1,"row",starteditevent,editform );
});
