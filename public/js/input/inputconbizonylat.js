var inputconbizonylat = (function () {
    	var initEvents = [
		//Add field
		{
			  event	: "click"
			, target: "#Actszamla"
			, fn	: Actszamla
		},
		{
			  event	: "click"
			, target: "#Actfejszamlapost"
			, fn	: bizfejpost
		},
		{
			  event	: "click"
			, target: "#Actfelvesz"
			, fn	: biztetelpost
		},
		{
			  event	: "click"
			, target: "#Actclose"
			, fn	:  komplexinput.actclose
		},
		{
			  event	: "change"
			, target: "#bizf_fztm_id"
			, fn	: fizf_fztm_idChange
		},
		{
			  event	: "change"
			, target: "#bizf_telj_datum"
			, fn	: fizf_fztm_idChange
		},
		{
			  event	: "change"
			, target: "#bizfteljdatum"
			, fn	: fizf_fztm_idChange
		},
		{
			  event	: "keyup"
			, target: "#bizt_menny"
			, fn	: function(event) {
                    if (event.keyCode === 13) {
                        biztetelpost();
                        selectCikkekGrid.focus();
                        selectCikkekGrid.repaint();

                    }
                }
		},
		{
			  event	: "click"
			, target: "#Actbizfejview"
			, fn	: function() {
                        setBiztopkod ();
                        tabhidde ();
                        next("bizonylatfej");
                }
		},
		{
			  event	: "click"
			, target: "#Actbizfejdeleted"
			, fn	: bizfejdelete
		}

	];
    var alapertekek         = null;
    var biztGrid            = null;
    var selectCikkekGrid    = null;
    var azonositokGrid      = null;
    var atadopartnerGrid    = null;
	var bizonylattetelekMezoadatok =  null;
	var selectcikkekMezoadatok =  null;

	return {
		  init			: init
        , Actszamla     : Actszamla
        , bizfejpost    : bizfejpost
        , biztetelpost  : biztetelpost
		, destruct		: destruct

	}
		function init () {
            komplexinput.loading ();
             $("#frmInputBizonylatmennyiseg").on('shown.bs.modal',  function (){
                  $("#bizt_menny").focus();
                  $("#bizt_menny").select();
              });
            alapertekek = komplexinput.getalapertek();
            komplexgrid.setmasterGridHeight(450);

                initEvents.forEach(function(ev){
                    $(document).on(ev.event, ev.target, ev.fn);
                });
            if ( $("#ParamFormType").val()=='ftInputInsert' || $("#ParamFormType").val()=='ftContinueInputInsert') {
             //console.log('insertt');

            //            activaTab('atadopartnercimei');
            //            $('#inputconbizonylatokTab').hide();
                    /*** SET LISTENERS ***/

                        var oneazonositourl = $("#oneazonositourl").val();
                        var bizf_azon_id = $("#bizf_azon_id").val();
                        var biztip_kod = $("#biztip_kod").val();
                        var azonositourl = $("#azonositourl").val()
                        if (bizf_azon_id!=0) {
                            azonositourl=oneazonositourl+bizf_azon_id;
                        }
                        if (bizf_azon_id==0) {
                            azonositourl=azonositourl+biztip_kod;
                        }

                        //console.log(azonositourl);
                        if (azonositokGrid) {
                            azonositokGrid.refresh();
                        } else {
                            azonositok (azonositourl, biztip_kod);
                            if (bizf_azon_id!=0) {
                                nextAzonositok();
                            }
                        }


            }
            $("#Actfelvesz").hide();
            $("#menusor").hide();
            $('.navbar').hide();
            if ( $("#ParamFormType").val()=='ftInputUpdate') {
                //console.log('Update');
                        //atadopartner ();
                        //atvevopartner ();
                         //getbizonylatfej();
                      setVisible ();
                        tabhidde ();
                        next("bizonylattetelek");
                        bizonylattetelek ();
                        //biztGrid.option('columnHidingEnabled',false);
                        //biztGrid.refresh();
                        getatvevopartner($("#bizf_atvevo_prt_id").val());
                        selectcikkek ();
                /*            partnercime ($("#bizf_atado_prt_id").val() ,'1', '#atadopartnercimeigridContainer',function(selectedItems) {
                                 var data = selectedItems.selectedRowsData[0];
                                if(data) {
                                   // console.log(data.prt_id);
                                    $("#bizf_atado_prt_cim_id").val(data.prt_cim_id);
                                    $("#bizf_atado_prt_teljescime").val(data.prt_teljescim);
                                    toastr_notify('info', 'Átadó címe:'+data.prt_teljescim, { timeOut: 5000 });
                                }

                            })
                            partnerbankszamlaszamai ($("#bizf_atado_prt_id").val(), '#atadopartnerbankszamlaszamgridContainer',function(selectedItems) {
                                 var data = selectedItems.selectedRowsData[0];
                                if(data) {
                                   // console.log(data.prt_id);
                                    $("#bizf_atado_bsz_id").val(data.bsz_id);
                                    $("#bizf_atado_prt_bsz_bankszamlaszam").val(data.bsz_view_bankszamlaszam);
                                    toastr_notify('info', 'Átadó bankszáma:'+data.bsz_view_bankszamlaszam, { timeOut: 5000 });
                                }

                            })
                          var atvevoszallitasicimei = partnercime ($("#bizf_atvevo_prt_id").val(),'2', '#atvevopartnerszalitasicimeieigridContainer',function(selectedItems) {
                             var data = selectedItems.selectedRowsData[0];
                            if(data) {
                               // console.log(data.prt_cim_id);
                                $("#bizf_atvevo_szalitasicim_prt_cim_id").val(data.prt_cim_id);
                                $("#bizf_atvevo_szallaitasi_prt_teljescime").val(data.prt_teljescim);
                                $("#itembizf_atvevo_szallaitasi_prt_teljescime").show();
                                if (alapertekek.jovedekitermekkezeles == '1') {
                                    if ($("#jovedekitermek").val()=='I'){
                                        if (data.prt_telephelyszam) {
                                            toastr_notify('info', 'Átvevő jövedéki engedélyszám:'+data.prt_telephelyszam, { timeOut: 5000 });
                                            $("#bizf_atvevo_prt_jovedeki_engedelyszam").val(data.prt_telephelyszam);
                                        }
                                    }
                                }
                                toastr_notify('info', 'Átvevő szallitasi címe:'+data.prt_teljescim, { timeOut: 5000 });
                                getOneatvevoPartnerCimeCount (bizf_atvevo_prt_id);

                            }

                        },false );
                       var atvevocimei =  partnercime ($("#bizf_atvevo_prt_id").val(),'1', '#atvevopartnercimeigridContainer',function(selectedItems) {
                             var data = selectedItems.selectedRowsData[0];
                            if(data) {
                             //   console.log(data.prt_cim_id);
                                $("#bizf_atvevo_prt_cim_id").val(data.prt_cim_id);
                                $("#bizf_atvevo_prt_teljescime").val(data.prt_teljescim);
                                toastr_notify('info', 'Átvevő címe:'+data.prt_teljescim, { timeOut: 5000 });
                                 //atvevopartner ();
                                next ();
                            }

                        },false )
                       partnerbankszamlaszamai ($("#bizf_atvevo_prt_id").val(), '#atvevopartnerbankszamlaszamgridContainer',function(selectedItems) {
                             var data = selectedItems.selectedRowsData[0];
                            if(data) {
                               // console.log(data.prt_id);
                                $("#bizf_atvevo_bsz_id").val(data.bsz_id);
                                $("#bizf_atvevo_prt_bsz_bankszamlaszam").val(data.bsz_view_bankszamlaszam);
                                toastr_notify('info', 'Átvevő bankszáma:'+data.bsz_view_bankszamlaszam, { timeOut: 5000 });
                                tabhidde ();
                                next("bizonylatfej");

                            }

                        },false )
                        getatvevopartner($("#bizf_atvevo_prt_id").val());
                        setVisible ();
                        bizonylattetelek ();
                        //biztGrid.option('columnHidingEnabled',false);
                        biztGrid.refresh();
                        selectcikkek ();
                */
            }

            //console.log(komplexinput.alapertekek.jovedekitermekkezeles);
		/*** SET LISTENERS ***/

             $('[data-toggle="tooltip"]').tooltip();
        komplexinput.loaded();

	}
	function destruct () {
	 	initEvents.forEach(function (ev) {
			$(document).off(ev.event, ev.target);
		});
	}
	function bizfejdelete () {
        if ($("#bizf_feldolg_status_kod").val()=='0') {
            if ($("#bizf_id").val()) {
                swal({
                  title: "Biztos hogy törli ?",
                  text: "Törölni akarja a bevitt adatokat",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Igen, Törlöm!",
                  cancelButtonText: "Nem, Nem akarom",
                  closeOnConfirm: false,
                },
                function(){
                        var inputbizf = {
                                     'bizf_id'	: $("#bizf_id").val()
                             };
                         komplexinput.post('/bizfejdeleted', inputbizf, function(result){
                          komplexinput.actclose();
                        });
                        swal("Törlés!", "A bizonylat törlésre került", "success");
                });
            }
        }
	}




    function atadopartner (){
//	getMezoadatok('t_cikkek _1');

         var allUrl = '/whereallpartner/'+$("#vevotipuskod").val();
         var bizf_atado_prt_id = $("#bizf_atado_prt_id").val();
        if (bizf_atado_prt_id!=0) {
            var allUrl = '/onegridpartner/'+bizf_atado_prt_id;
              var obj = {
                          'prt_id'	: bizf_atado_prt_id
                     };

            komplexinput.post('/getOnePartner', obj, function(data){
                        //console.log(data.prt_nev);
                      if (data) {
                            $("#bizf_atado_prt_id").val(data.prt_id);
                            $("#bizf_atado_prt_nev").val(data.prt_nev);
                            $("#bizf_atado_prt_adoszam").val(data.prt_adoszam);
                                toastr_notify('info', 'Átadó partner:'+data.prt_nev, { timeOut: 5000 });
                                    if (alapertekek.jovedekitermekkezeles == '1') {
                                        if ($("#jovedekitermek").val()=='I'){
                                            toastr_notify('info', 'Átadó jövedéki engedélyszám:'+data.prt_jovedeki_engedelyszam, { timeOut: 5000 });
                                            $("#bizf_atado_prt_jovedeki_engedelyszam").val(data.prt_jovedeki_engedelyszam);
                                        }
                                    }

                            getOneatadoPartnerCimeCount (bizf_atado_prt_id);
                          //
                      }
                });
        }else{
            tabhidde ();
            next("atadopartner");
        }
                var partnergridContainer = '#atadopartnergridContainer';
                var partnerMezoadatok =  komplexgrid.getadatok('t_partnerek_1',[
                        { dataField: 'prt_id', caption: 'prt_id',visible: false, enable: false},
                        { dataField: 'prt_nev', caption: 'prt_nev'},
                        { dataField: 'prt_adoszam', caption: 'prt_adoszam'},
                        { dataField: 'prt_alap_teljescime', caption: 'prt_alap_teljescime'}, //sortOrder: 'asc', sortIndex: 0},
                        { dataField: 'prt_fztm_megnevezes', caption: 'prt_fztm_megnevezes'},
                        { dataField: 'prt_utalasi_nap', caption: 'prt_utalasi_nap'},
                        { dataField: 'prt_bsz_bankszamlaszam', caption: 'prt_bsz_bankszamlaszam'},
                        { dataField: 'prt_artbl_arkategoria_kod', caption: 'prt_artbl_arkategoria_kod',visible: false},
                        { dataField: 'prt_jovedeki_engedelyszam', caption: 'prt_jovedeki_engedelyszam'},
                        { dataField: 'prt_prtelhtsg_mobilszam', caption: 'prt_prtelhtsg_mobilszam'}
                        ]);
                        var orders = komplexgrid.getgridorders(partnergridContainer,allUrl );
                        var gridDataSource = komplexgrid.setgridDataSource(allUrl );
                var starteditevent=function(e) {

                    }
                var selectedevent=function(selectedItems) {
                         var data = selectedItems.selectedRowsData[0];
                        if(data) {
                            $("#bizf_atado_prt_id").val(data.prt_id);
                            $("#bizf_atado_prt_nev").val(data.prt_nev);
                            $("#bizf_atado_prt_adoszam").val(data.prt_adoszam);
                            toastr_notify('info', 'Átadó partner:'+data.prt_nev, { timeOut: 5000 });
                                if (alapertekek.jovedekitermekkezeles == '1') {
                                    if ($("#jovedekitermek").val()=='I'){
                                        toastr_notify('info', 'Átadó jövedéki engedélyszám:'+data.prt_jovedeki_engedelyszam, { timeOut: 5000 });
                                        $("#bizf_atado_prt_jovedeki_engedelyszam").val(data.prt_jovedeki_engedelyszam);
                                    }
                                }
                            partnercime (data.prt_id,'1', '#atadopartnercimeigridContainer',function(selectedItems) {
                                 var data = selectedItems.selectedRowsData[0];
                                if(data) {
                                   // console.log(data.prt_id);
                                    $("#bizf_atado_prt_cim_id").val(data.prt_cim_id);
                                    $("#bizf_atado_prt_teljescime").val(data.prt_teljescim);
                                    toastr_notify('info', 'Átadó címe:'+data.prt_teljescim, { timeOut: 5000 });
                                    tabhidde ();
                                     atvevopartner ();
                                  //  next ();
                                }

                            })
                            partnerbankszamlaszamai (data.prt_id, '#atadopartnerbankszamlaszamgridContainer',function(selectedItems) {
                                 var data = selectedItems.selectedRowsData[0];
                                if(data) {
                                   // console.log(data.prt_id);
                                    $("#bizf_atado_bsz_id").val(data.bsz_id);
                                    $("#bizf_atado_prt_bsz_bankszamlaszam").val(data.bsz_view_bankszamlaszam);
                                    toastr_notify('info', 'Átadó bankszáma:'+data.bsz_view_bankszamlaszam, { timeOut: 5000 });
                                    tabhidde ();

                                }

                            })

                            //next ();
                        }

                    }

                 var mastergrid = komplexgrid.setmastergrid(partnergridContainer,orders,partnerMezoadatok,0,0,0,starteditevent,selectedevent );
                  atadopartnerGrid = $(partnergridContainer).dxDataGrid('instance');

    };
function getOneatadoPartnerCime (bizf_atado_prt_id,tipuskod='1') {
      var obj = {
            'prt_id'	: bizf_atado_prt_id,
            'tipuskod'  : tipuskod
        };
    komplexinput.post('/getOnePartnerCime', obj, function(data){
       if(data) {
       // console.log(data.prt_id);
        $("#bizf_atado_prt_cim_id").val(data.prt_cim_id);
        $("#bizf_atado_prt_teljescime").val(data.prt_teljescim);
        toastr_notify('info', 'Átadó címe:'+data.prt_teljescim, { timeOut: 5000 });
         atvevopartner ();
      }
     });

};
function getOneatadoPartnerCimeCount (bizf_atado_prt_id,tipuskod='1') {
                        var obj = {
                            'prt_id'	: bizf_atado_prt_id,
                            'tipuskod'  : tipuskod
                        };
                      komplexinput.post('/getOnePartnerCimeiCount', obj, function(data){
                     //   console.log(data);
                              if (data) {
                                  if (data.totalcount==1) {
                                      getOneatadoPartnerCime (bizf_atado_prt_id);
                                      partnercime (bizf_atado_prt_id,'1', '#atadopartnercimeigridContainer',function(selectedItems) {
                                        var data = selectedItems.selectedRowsData[0];
                                            if(data) {
                                               // console.log(data.prt_id);
                                                $("#bizf_atado_prt_cim_id").val(data.prt_cim_id);
                                                $("#bizf_atado_prt_teljescime").val(data.prt_teljescim);
                                                toastr_notify('info', 'Átadó címe:'+data.prt_teljescim, { timeOut: 5000 });
                                                 getOneatadoPartnerbankszamCount (bizf_atado_prt_id);
                                            }

                                        });
                                        getOneatadoPartnerbankszamCount (bizf_atado_prt_id);
                                  }
                                  if (data.totalcount==0) {
                                       getOneatadoPartnerbankszamCount (bizf_atado_prt_id);
                                  }
                                  if (data.totalcount>1) {
                                      partnercime (bizf_atado_prt_id,'1', '#atadopartnercimeigridContainer',function(selectedItems) {
                                        var data = selectedItems.selectedRowsData[0];
                                            if(data) {
                                               // console.log(data.prt_id);
                                                $("#bizf_atado_prt_cim_id").val(data.prt_cim_id);
                                                $("#bizf_atado_prt_teljescime").val(data.prt_teljescim);
                                                toastr_notify('info', 'Átadó címe:'+data.prt_teljescim, { timeOut: 5000 });
                                                 getOneatadoPartnerbankszamCount (bizf_atado_prt_id);
                                            }

                                        });

                                  }

                              }
                      });
}

function getOneatadoPartnerbankszamCount (bizf_atado_prt_id,tipuskod='1') {
                        var obj = {
                            'prt_id'	: bizf_atado_prt_id,
                            'tipuskod'  : tipuskod
                        };
                      komplexinput.post('/getOnePartnerBankszamlaCount', obj, function(data){
                     //   console.log(data);
                              if (data) {
                                  if (data.totalcount==1) {
                                      getOneatadoPartnerOnebankszam (bizf_atado_prt_id);
                                        partnerbankszamlaszamai (bizf_atado_prt_id, '#atadopartnerbankszamlaszamgridContainer',function(selectedItems) {
                                         var data = selectedItems.selectedRowsData[0];
                                        if(data) {
                                           // console.log(data.prt_id);
                                            $("#bizf_atado_bsz_id").val(data.bsz_id);
                                            $("#bizf_atado_prt_bsz_bankszamlaszam").val(data.bsz_view_bankszamlaszam);
                                            toastr_notify('info', 'Átadó bankszáma:'+data.bsz_view_bankszamlaszam, { timeOut: 5000 });
                                            // next ();
                                        }

                                        });
                                  }
                                  if (data.totalcount == 0) {
                                      tabhidde ();
                                      next("atvevopartner");
                                  }
                                  if (data.totalcount>1) {
                                      tabhidde ();
                                      next("atadopartnerbankszamlaszam");

                                        partnerbankszamlaszamai (bizf_atado_prt_id, '#atadopartnerbankszamlaszamgridContainer',function(selectedItems) {
                                         var data = selectedItems.selectedRowsData[0];
                                                if(data) {
                                                   // console.log(data.prt_id);
                                                    $("#bizf_atado_bsz_id").val(data.bsz_id);
                                                    $("#bizf_atado_prt_bsz_bankszamlaszam").val(data.bsz_view_bankszamlaszam);
                                                    toastr_notify('info', 'Átadó bankszáma:'+data.bsz_view_bankszamlaszam, { timeOut: 5000 });
                                                    tabhidde ();
                                                    next("atvevopartner");
                                                    atvevopartner ();
                                                }

                                        });
                                  }
                              }
                      });
}

function getOneatadoPartnerOnebankszam (bizf_atado_prt_id,tipuskod='1') {
      var obj = {
            'prt_id'	: bizf_atado_prt_id,
            'tipuskod'  : tipuskod
        };
    komplexinput.post('/getOnePartnerBankszamla', obj, function(data){
       if(data) {
       // console.log(data.prt_id);
        $("#bizf_atado_bsz_id").val(data.bsz_id);
        $("#bizf_atado_prt_bsz_bankszamlaszam").val(data.bsz_view_bankszamlaszam);
        toastr_notify('info', 'Átadó bankszáma:'+data.bsz_view_bankszamlaszam, { timeOut: 5000 });
        tabhidde ();
        next("atvevopartner");
      }
     });

};
function atvevopartner (){
//	getMezoadatok('t_cikkek _1');
    var allUrl = '/whereallpartner/'+$("#vevotipuskod").val();
    var bizf_atvevo_prt_id = $("#bizf_atvevo_prt_id").val();
    if (bizf_atvevo_prt_id!=0) {
        var allUrl = '/onegridpartner/'+bizf_atvevo_prt_id;
    }
    if (alapertekek.jovedekitermekkezeles == '1') {
            if ($("#jovedekitermek").val()=='I'){
                allUrl = allUrl +' and prt_jovedeki_engedelyszam is not null';
            }
    }
	var partnergridContainer = '#atvevopartnergridContainer';
	var partnerMezoadatok =  komplexgrid.getadatok('t_partnerek_1',[
            { dataField: 'prt_id', caption: 'prt_id',visible: false, enable: false},
            { dataField: 'prt_nev', caption: 'prt_nev'},
			{ dataField: 'prt_adoszam', caption: 'prt_adoszam'},
            { dataField: 'prt_alap_teljescime', caption: 'prt_alap_teljescime'}, //sortOrder: 'asc', sortIndex: 0},
            { dataField: 'prt_fztm_id', caption: 'prt_fztm_id'},
            { dataField: 'prt_fztm_megnevezes', caption: 'prt_fztm_megnevezes'},
            { dataField: 'prt_utalasi_nap', caption: 'prt_utalasi_nap'},
			{ dataField: 'prt_bsz_bankszamlaszam', caption: 'prt_bsz_bankszamlaszam'},
			{ dataField: 'prt_artbl_arkategoria_kod', caption: 'prt_artbl_arkategoria_kod',visible: false},
			{ dataField: 'prt_egyenikedvezmeny', caption: 'prt_egyenikedvezmeny',visible: false},
			{ dataField: 'prt_jovedeki_engedelyszam', caption: 'prt_jovedeki_engedelyszam'},
            { dataField: 'prt_prtelhtsg_mobilszam', caption: 'prt_prtelhtsg_mobilszam'}
			]);
    		var orders = komplexgrid.getgridorders(partnergridContainer,allUrl);
			var gridDataSource = komplexgrid.setgridDataSource(allUrl);
    var starteditevent=function(e) {

        }
    var selectedevent=function(selectedItems) {

             var data = selectedItems.selectedRowsData[0];
            if(data) {
                //console.log(data.prt_id);
                $("#bizf_atvevo_prt_id").val(data.prt_id);
                $("#bizf_atvevo_prt_nev").val(data.prt_nev);
                toastr_notify('info', 'Átvevő partner:'+data.prt_nev, { timeOut: 5000 });
                $("#bizf_atvevo_prt_adoszam").val(data.prt_adoszam);
                $("#bizt_artbl_arkategoria_kod_kod").val(data.prt_artbl_arkategoria_kod);
                $("#prt_egyenikedvezmeny").val(data.prt_egyenikedvezmeny);
                $("#bizf_fztm_id").val(data.prt_fztm_id);
                        if (alapertekek.jovedekitermekkezeles == '1') {
                            if ($("#jovedekitermek").val()=='I'){
                                toastr_notify('info', 'Átvevő jövedéki engedélyszám:'+data.prt_jovedeki_engedelyszam, { timeOut: 5000 });
                                $("#bizf_atvevo_prt_jovedeki_engedelyszam").val(data.prt_jovedeki_engedelyszam);
                            }
                        }
                tabhidde ();
                 //console.log(data.prt_fztm_id);
                fizf_fztm_idChange();
                getOneatvevoPartnerSzallitoCimeCount (data.prt_id)
            }

        }

     var mastergrid = komplexgrid.setmastergrid(partnergridContainer,orders,partnerMezoadatok,0,0,0,starteditevent,selectedevent,false  );
        tabhidde ();
        next("atvevopartner");

    return {
        init				: init,
        partnerMezoadatok	: partnerMezoadatok,
        mastergrid			: mastergrid
    }

    function init () {

    }
    };
function getOneatvevoPartnerSzallitoCimeCount (bizf_atvevo_prt_id) {
                        var obj = {
                            'prt_id'	: bizf_atvevo_prt_id,
                            'tipuskod'  : '2'
                        };
                      komplexinput.post('/getOnePartnerCimeiCount', obj, function(data){
                     //   console.log(data);
                              if (data) {
                                  if (data.totalcount==1) {
                                      console.log('Szállító 1');
                                      getOneatvevoPartnerSzallitoCime (bizf_atvevo_prt_id);

                                  }
                                  if (data.totalcount==0) {
                                      console.log('Szállító 0');
                                      getOneatvevoPartnerCimeCount (bizf_atvevo_prt_id);
                                  }
                                  if (data.totalcount>1) {
                                      console.log('Szállító >1');
                                      next("atvevopartnerszalitasicimei");
                                     var atvevoszallitasicimei = partnercime (bizf_atvevo_prt_id,'2', '#atvevopartnerszalitasicimeieigridContainer',function(selectedItems) {
                                         var data = selectedItems.selectedRowsData[0];
                                        if(data) {
                                           // console.log(data.prt_cim_id);
                                            $("#bizf_atvevo_szalitasicim_prt_cim_id").val(data.prt_cim_id);
                                            $("#bizf_atvevo_szallaitasi_prt_teljescime").val(data.prt_teljescim);
                                            $("#itembizf_atvevo_szallaitasi_prt_teljescime").show();
                                            if (alapertekek.jovedekitermekkezeles == '1') {
                                                if ($("#jovedekitermek").val()=='I'){
                                                    if (data.prt_telephelyszam) {
                                                        toastr_notify('info', 'Átvevő jövedéki engedélyszám:'+data.prt_telephelyszam, { timeOut: 5000 });
                                                        $("#bizf_atvevo_prt_jovedeki_engedelyszam").val(data.prt_telephelyszam);
                                                    }
                                                }
                                            }
                                            toastr_notify('info', 'Átvevő szallitasi címe:'+data.prt_teljescim, { timeOut: 5000 });
                                            tabhidde ();
                                            getOneatvevoPartnerCimeCount (bizf_atvevo_prt_id);
                                        }

                                    },false )
                                    atvevoszallitasicimei.mastergrid.option("columnHidingEnabled", data.value);

                                  }

                              }
                      });
}
function getOneatvevoPartnerSzallitoCime (bizf_atvevo_prt_id) {
             var obj = {
                    'prt_id'	: bizf_atvevo_prt_id,
                    'tipuskod'  : '2'
             };
            komplexinput.post('/getOnePartnerCime', obj, function(data){
               if(data) {
                   $("#bizf_atvevo_szalitasicim_prt_cim_id").val(data.prt_cim_id);
                    $("#bizf_atvevo_szallaitasi_prt_teljescime").val(data.prt_teljescim);
                   $("#itembizf_atvevo_szallaitasi_prt_teljescime").show();
                    if (alapertekek.jovedekitermekkezeles == '1') {
                        if ($("#jovedekitermek").val()=='I'){
                            if (data.prt_telephelyszam) {
                                toastr_notify('info', 'Átvevő jövedéki engedélyszám:'+data.prt_telephelyszam, { timeOut: 5000 });
                                $("#bizf_atvevo_prt_jovedeki_engedelyszam").val(data.prt_telephelyszam);
                            }
                        }
                    }
                    toastr_notify('info', 'Átvevő szallitasi címe:'+data.prt_teljescim, { timeOut: 5000 });
                   getOneatvevoPartnerCimeCount (bizf_atvevo_prt_id);
              }
             });
    }
function getOneatvevoPartnerCimeCount (bizf_atvevo_prt_id) {
                        var obj = {
                            'prt_id'	: bizf_atvevo_prt_id,
                            'tipuskod'  : '1'
                        };
                      komplexinput.post('/getOnePartnerCimeiCount', obj, function(data){
                     //   console.log(data);
                              if (data) {
                                  if (data.totalcount==1) {
                                      console.log('Alap cím = 1 ');
                                      getOneatvevoPartnerCime (bizf_atvevo_prt_id);
                                      //getOneatvevoPartnerbankszamCount (bizf_atvevo_prt_id);
                                  }
                                  if (data.totalcount==0) {
                                      console.log('Alap cím = 0 ');
                                      getOneatvevoPartnerbankszamCount (bizf_atvevo_prt_id);
                                  }
                                  if (data.totalcount>1) {
                                      console.log('Alap cím > 1 ');
                                    next("atvevopartnercimei");
                                    var atvevocimei =  partnercime (bizf_atvevo_prt_id,'1', '#atvevopartnercimeigridContainer',function(selectedItems) {
                                         var data = selectedItems.selectedRowsData[0];
                                        if(data) {
                                         //   console.log(data.prt_cim_id);
                                            $("#bizf_atvevo_prt_cim_id").val(data.prt_cim_id);
                                            $("#bizf_atvevo_prt_teljescime").val(data.prt_teljescim);
                                            toastr_notify('info', 'Átvevő címe:'+data.prt_teljescim, { timeOut: 5000 });
                                            getOneatvevoPartnerbankszamCount (bizf_atvevo_prt_id);
                                        }

                                    },false );
                                  }

                              }
                      });
}
function getOneatvevoPartnerCime (bizf_atvevo_prt_id) {
             var obj = {
                    'prt_id'	: bizf_atvevo_prt_id,
                    'tipuskod'  : '1'
             };
            komplexinput.post('/getOnePartnerCime', obj, function(data){
               if(data) {
                    $("#bizf_atvevo_prt_cim_id").val(data.prt_cim_id);
                    $("#bizf_atvevo_prt_teljescime").val(data.prt_teljescim);
                    toastr_notify('info', 'Átvevő címe:'+data.prt_teljescim, { timeOut: 5000 });
                     //atvevopartner ();
                   getOneatvevoPartnerbankszamCount (bizf_atvevo_prt_id);
              }
             });
    };
function getOneatvevoPartnerbankszamCount (bizf_atvevo_prt_id,tipuskod='1') {
                        var obj = {
                            'prt_id'	: bizf_atvevo_prt_id,
                            'tipuskod'  : tipuskod
                        };
                      komplexinput.post('/getOnePartnerBankszamlaCount', obj, function(data){
                     //   console.log(data);
                              if (data) {
                                  if (data.totalcount==1) {
                                       console.log('atvevoPartnerbankszamCount = 1 ');
                                      getOneatvevoPartnerOnebankszam (bizf_atvevo_prt_id);
                                  }
                                  if (data.totalcount==0) {
                                    //   console.log('atvevoPartnerbankszamCount = 0 ');
                                    //  console.log('Átvevő bankszámlai 0');
                                    setBiztopkod ();
                                    tabhidde ();
                                    next("bizonylatfej");
                                    bizfejpost();
                                  }
                                  if (data.totalcount>1) {
                                       console.log('atvevoPartnerbankszamCount > 1 ');
                                      next("atvevopartnerbankszamlaszam");
                                    partnerbankszamlaszamai (bizf_atvevo_prt_id, '#atvevopartnerbankszamlaszamgridContainer',function(selectedItems) {
                                         var data = selectedItems.selectedRowsData[0];
                                        if(data) {
                                           // console.log(data.prt_id);
                                            $("#bizf_atvevo_bsz_id").val(data.bsz_id);
                                            $("#bizf_atvevo_prt_bsz_bankszamlaszam").val(data.bsz_view_bankszamlaszam);
                                            toastr_notify('info', 'Átvevő bankszáma:'+data.bsz_view_bankszamlaszam, { timeOut: 5000 });
                                            setBiztopkod ();
                                            tabhidde ();
                                            next("bizonylatfej");
                                             bizfejpost();
                                        }

                                    },false )
                                  }
                              }
                      });
}

function getOneatvevoPartnerOnebankszam (bizf_atvevo_prt_id,tipuskod='1') {
      var obj = {
            'prt_id'	: bizf_atvevo_prt_id,
            'tipuskod'  : tipuskod
        };
    komplexinput.post('/getOnePartnerBankszamla', obj, function(data){
       if(data) {
       // console.log(data.prt_id);
        $("#bizf_atvevo_bsz_id").val(data.bsz_id);
        $("#bizf_atvevo_prt_bsz_bankszamlaszam").val(data.bsz_view_bankszamlaszam);
        //   next ();
        toastr_notify('info', 'Átvevő bankszáma:'+data.bsz_view_bankszamlaszam, { timeOut: 5000 });
        setBiztopkod ();
        tabhidde ();
        next("bizonylatfej");
         bizfejpost();
      }
     });

};
    function partnercime (prt_id,tipuskod,partnercimgridContainer,selectedevent){
        //	getMezoadatok('t_cikkek _1');

             var allUrl = '/wherepartnercimei/';
            if (prt_id!=0) {
                allUrl = allUrl+'and prt_cim_prt_id='+prt_id+' and prt_cim_tipus_kod='+"'"+tipuskod+"'";
            }
            var partnerMezoadatok =  komplexgrid.getadatok('t_partnerek_cimei_2',[
                    { dataField: 'prt_cim_id', caption: 'prt_cim_id',visible: false, enable: false},
                    { dataField: 'prt_telephelyszam', caption: 'prt_telephelyszam'},
                    { dataField: 'prt_cim_tipus_kod_nev', caption: 'prt_cim_tipus_kod_nev'},
                    { dataField: 'prt_teljescim', caption: 'prt_teljescim'}
                    ]);
                    var orders = komplexgrid.getgridorders(partnercimgridContainer,allUrl );
                    var gridDataSource = komplexgrid.setgridDataSource(allUrl );
            var starteditevent=function(e) {

                }

             var mastergrid = komplexgrid.setmastergrid(partnercimgridContainer,orders,partnerMezoadatok,0,0,0,starteditevent,selectedevent,false );
            return {
                init				: init,
                partnerMezoadatok	: partnerMezoadatok,
                mastergrid			: mastergrid
            }

            function init () {

            }
    };

    function partnerbankszamlaszamai (prt_id,partnerbankszamlaszamgridContainer,selectedevent,tipuskod){
        //	getMezoadatok('t_cikkek _1');

             var allUrl = '/wherepartnerbankszamlaszamok/';
            if (prt_id!=0) {
                if (komplexinput.isUndefined(tipuskod)) {
                   allUrl = allUrl+'and bsz_prt_id='+prt_id;
                }
                 else {
                   allUrl = allUrl+'and bsz_prt_id='+prt_id;
                  // allUrl = allUrl+'and bsz_prt_id='+prt_id+' and bsz_tipus_kod='+"'"+tipuskod+"'";
                 }


            }
            var partnerBankMezoadatok =  komplexgrid.getadatok('t_bankszamlaszamok_2',[
                    { dataField: 'bsz_id', caption: 'bsz_id',visible: false, enable: false},
                    { dataField: 'bsz_view_bankszamlaszam', caption: 'bsz_view_bankszamlaszam'},
                    { dataField: 'bsz_atfutasi_napok', caption: 'bsz_atfutasi_napok'},
                    { dataField: 'bsz_iban', caption: 'bsz_iban'},
                    { dataField: 'bsz_tipus_kod_nev', caption: 'bsz_tipus_kod_nev'}
                    ]);
                    var orders = komplexgrid.getgridorders(partnerbankszamlaszamgridContainer,allUrl );
                    var gridDataSource = komplexgrid.setgridDataSource(allUrl );
            var starteditevent=function(e) {

                }

             var mastergrid = komplexgrid.setmastergrid(partnerbankszamlaszamgridContainer,orders,partnerBankMezoadatok,0,0,0,starteditevent,selectedevent,false );
            return {
                init				    : init,
                partnerBankMezoadatok	: partnerBankMezoadatok,
                mastergrid			    : mastergrid
            }

            function init () {

            }
    };
    function azonositok (azonositourl, biztip_kod){
            var azonositokgridContainer = '#gridazonositokContainer';
            var azonositokMezoadatok =  komplexgrid.getadatok('t_azonositok_1',[
                    { dataField: 'azon_id', caption: 'azon_id',visible: false, enable: false},
                    { dataField: 'azon_biztip_kod_nev', caption: 'azon_biztip_kod_nev'},
                    { dataField: 'azon_rovidneve', caption: 'azon_rovidneve'},
                    { dataField: 'azon_ev', caption: 'azon_ev'},
                    { dataField: 'azon_nyito', caption: 'azon_nyito'}, //sortOrder: 'asc', sortIndex: 0},
                    { dataField: 'azon_kezdo', caption: 'azon_kezdo'},
                    { dataField: 'azon_befejezo', caption: 'azon_befejezo'},
                    { dataField: 'azon_atadott', caption: 'azon_atadott'},
                    { dataField: 'azon_default',visible:false, caption: 'azon_default'},
                    { dataField: 'azon_nyilvar_hatas_in_kod_nev', caption: 'azon_nyilvar_hatas_in_kod_nev'},
                    { dataField: 'azon_jovedeki_termek',visible:false, caption: 'azon_jovedeki_termek'},
                    { dataField: 'azon_jovedeki_termek_kod_nev', caption: 'azon_jovedeki_termek_kod_nev'}
                    ]);
                    var orders = komplexgrid.getgridorders(azonositokgridContainer,azonositourl);
                    var gridDataSource = komplexgrid.setgridDataSource(azonositourl);
            var starteditevent=function(e) {

                }
            var azonositokselectedevent=function(selectedItems) {
                     var data = selectedItems.selectedRowsData[0];
                 //alert(JSON.stringify(data));
                 //alert(JSON.stringify(data));
                    if(data) {

                       $("#bizf_azon_id").val(data.azon_id);
                       $("#jovedekitermek").val(data.azon_jovedeki_termek);
                       $("#azon_biztip_kod_nev").val(data.azon_biztip_kod_nev);
             /*            atadopartner ();
                        setVisible ();
                        toastr_notify('info', 'Bizonylat tömb:'+data.azon_biztip_kod_nev, { timeOut: 5000 });
                        next ();
                        */
                        nextAzonositok();
                    }

                }
            $("#bizonylatombvalasztas").show();
             var mastergrid = komplexgrid.setmastergrid(azonositokgridContainer,orders,azonositokMezoadatok,0,0,0,starteditevent,azonositokselectedevent  );
              azonositokGrid = $(azonositokgridContainer).dxDataGrid('instance');
        };
    function nextAzonositok(){
        $("#bizonylatombvalasztas").hide();
                        atadopartner ();
                        setVisible ();
                        toastr_notify('info', 'Bizonylat tömb:'+$("#azon_biztip_kod_nev").val(), { timeOut: 5000 });
                        //next ();
    }
	function Actszamla(){
		var deferred	=  $.Deferred();
		var isSuccess	= true;
		//var postData = getPostData (true,'#profiledata');
		var postData = komplexinput.getPostData('#inputconbizonylat');

		komplexinput.formpost('/InputConBizonylatsaveszamla', postData, function(result){
				deferred.resolve({next: true});
			});
		return deferred.promise;
	}
	function bizfejpost(){
		var deferred	=  $.Deferred();
		var isSuccess	= true;
		var postData = komplexinput.getPostData('#inputconbizonylat');
         jQuery.ajaxSetup({async:false});
		komplexinput.formpost('/bizfejpost', postData, function(result){
				//deferred.resolve({next: true});
            if (result.bizf_id) {
                     $("#bizf_id").val(result.bizf_id);
                    $("#bizonylatfej").hide();
                    $("#bizonylattetelek").show();
            }
            tabhidde ();
            next("bizonylattetelek");
            bizonylattetelek ();
            selectcikkek ();
                    //next ();

			});
         jQuery.ajaxSetup({async:true});
        getbizonylatfej();
		return deferred.promise;
	}
	function biztetelpost (){
		var deferred	=  $.Deferred();
		var isSuccess	= true;
		//var postData = getPostData (true,'#profiledata');
		var postTetelData = komplexinput.getPostData('#inputconbizonylattetel');
        if ($("#bizt_menny").val() && $("#bizt_menny").val() !=0) {
               postTetelData.append('bizt_bizf_id'		, $("#bizf_id").val());
               postTetelData.append('biztip_kod'		, $("#biztip_kod").val());
                komplexinput.formpost('/biztetelpost', postTetelData, function(result){
                        //deferred.resolve({next: true});
                    if (result.bizt_id) {
                             $("#bizt_id").val(result.bizt_id);
                             $("#bizt_menny").val(0);
                           if (biztGrid) {
                               $('#frmInputBizonylatmennyiseg').modal('hide');
                                biztGrid.refresh();
                           }

                        getbizonylatfej();
                        selectCikkekGrid.focus();
                        selectCikkekGrid.repaint();
                      }
                    });
        }
        //console.log(selectCikkekGrid.state());
        //selectCikkekGrid.selectRowsByIndexes([0]);;
        selectCikkekGrid.focus();
        selectCikkekGrid.repaint();

		return deferred.promise;
	}
    function tabhidde () {
        $("#bizonylatombvalasztas").hide();
        $("#atadopartner").hide();
        $("#atadopartnercimei").hide();
        $("#atadopartnerbankszamlaszam").hide();
        $("#atvevopartner").hide();
        $("#atvevopartnerszalitasicimei").hide();
        $("#atvevopartnercimei").hide();
        $("#atvevopartnerbankszamlaszam").hide();
        $("#bizonylatfej").hide();
        $("#bizonylattetelek").hide();

    }
    function next (tab = '') {
        if (tab!='') {
            tabhidde ();
            $( "#" + tab ).show();
        }
    }
    function fizf_fztm_idChange() {
        var inputFztmObj = {
				  'fztm_id'	: $("#bizf_fztm_id").val()
			 };

	komplexinput.post('/getazonalifizetes', inputFztmObj, function(result){
               // console.log(result.fztm_azonalifizetes);
              if (result.fztm_azonalifizetes) {
                     $("#bizf_kif_datuma").val($("#bizf_esed_datum").val());
                     $("#bizfkifdatuma").val($("#bizf_esed_datum").val());
                     $("#bizf_telj_datum").val($("#bizf_esed_datum").val());
                     $("#bizfteljdatum").val($("#bizf_esed_datum").val());
              }
		});
        var inputObj = {
				  'bizf_fztm_id'	: $("#bizf_fztm_id").val()
				, 'bizf_telj_datum'	: $("#bizf_telj_datum").val()
				, 'partner_id'		: $("#bizf_atvevo_prt_id").val()
			 };

		komplexinput.post('/getbizfkifdatuma', inputObj, function(result){
               // console.log(result.bizf_kif_datuma);
				 $("#bizf_kif_datuma").val(result.bizf_kif_datuma);
				 $("#bizfkifdatuma").val(result.bizf_kif_datuma);
			});
    }
function selectcikkek (){//NOTE Selectcikk
//	getMezoadatok('t_cikkek _1');
    var clickTimer, lastRowCLickedId;
     var p_prt_id = $("#bizf_atvevo_prt_id").val();
     var p_datum = $("#bizfteljdatum").val();
     var pnzn_id = $("#bizf_penznem_id").val();
     var pnza_arfolyam = $("#bizf_arfolyam").val();
     var artbl_arkategoria_kod = $("#bizt_artbl_arkategoria_kod_kod").val();
     var rakt_id = $("#bizt_rakt_id").val();
     var parameterek = ' and 1=1';
    if (alapertekek.jovedekitermekkezeles == '1') {
        if ($("#jovedekitermek").val()=='I'){
            parameterek = 'and cik_id in (   Select cik_id from  "03_cik".cikkek where cik_jovedeki_termek='+"'I'"+')';
        }
    }

    var allUrl = '/selectcikk/'+p_datum+'/'+p_prt_id+'/'+pnzn_id+'/'+pnza_arfolyam+'/'+artbl_arkategoria_kod+'/'+rakt_id+'/'+parameterek
	var selectcikkekgridContainer = '#selectcikkekgridContainer';
            var orders = komplexgrid.getgridorders(selectcikkekgridContainer,allUrl);
			var gridDataSource = komplexgrid.setgridDataSource(allUrl );



    var cikkstarteditevent=function(e) {

        }
    var cikkselectedevent=function(selectedItems) {
             var data = selectedItems.selectedRowsData[0];
            if(data) {
                $("#biztciknev").text(data.cik_nev);
                $("#bizt_tetel_megnevezes").val(data.cik_nev);
                $("#bizt_cik_id").val(data.cik_id);
                $("#bizt_afa_id").val(data.cik_afa_id);
                $("#bizt_nyilv_ar").val(data.cik_eladasinettoar);
                $("#bizt_brutto_nyilv_ar").val(data.cik_eladasibruttoar);
                $("#bizt_fogy_ar").val(data.cik_beszerzersinettoar);
                $("#bizt_cik_egyseg_ar").val(data.cik_egyseg_ar);
                $("#bizt_cik_beszerzesi_ar").val(data.cik_beszerzersinettoar);
                $("#cik_beszerzersinettoart").val(data.cik_beszerzersinettoar);
                $("#cik_beszerzersibruttoar").val(data.cik_beszerzersibruttoar);
                $("#bizt_alap_ar").val(data.cik_eladasinettoar);
                $("#bizt_brutto_alap_ar").val(data.cik_eladasibruttoar);
                $("#bizt_akcios_ar").val(data.cik_akcios_ar);
                $("#bizt_forditottafa").val(data.cik_forditottafa);
                $("#bizt_me_id").val(data.cik_me_id);
                $("#bizt_me_nev").val(data.cik_me_rovidites);
                $("#cik_me_rovidites").val(data.cik_me_rovidites);
                $("#cik_pnzn_rovidites").val(data.cik_pnzn_rovidites);
                if (data.cik_keszlet_mod_kod=='I') {
                 $("#bizt_jc_id").val($("#biztjcid").val());
                } else {
                  $("#bizt_jc_id").val($("#biztjcidnl").val());
                }

                $("#bizt_prt_egyenikedvezmeny").val($("#prt_egyenikedvezmeny").val());
                if (data.cik_fixaras_kod=='I') {
                    $("#bizt_prt_egyenikedvezmeny").val(0);
                }
                cikkraktariadatok(data.cik_id,$("#bizt_rakt_id").val());
                $("#Actfelvesz").show();
                $("#bizt_menny").val(0);
                $("#bizt_menny").focus();
                $("#bizt_menny").select();
            }

        };
    var CikkKeyDown = function(e) {
        var selKey = e.component.getSelectedRowKeys();
        if (selKey.length) {
            var currentKey = selKey[0];
            var index = e.component.getRowIndexByKey(currentKey);
            if (e.jQueryEvent.keyCode == 38) {
                index--;
                if (index >= 0) {
                    e.component.selectRowsByIndexes([index]);
                    e.jQueryEvent.stopPropagation();
                }
            }
            else if (e.jQueryEvent.keyCode == 40) {
                index++;
                e.component.selectRowsByIndexes([index]);
                e.jQueryEvent.stopPropagation();
            }
            if (e.jQueryEvent.keyCode == 13) {
                $("#frmInputBizonylatmennyiseg").modal();
                 $("#bizt_menny").focus();
                 $("#bizt_menny").select();
            }
        }
        };

    komplexgrid.setmasterGridHeight (500);
     var selectCikkek_grid = komplexgrid.setmastergrid(selectcikkekgridContainer,orders,selectcikkekMezoadatok,0,0,0,cikkstarteditevent,cikkselectedevent,false,
                null,
                null,
                null,
                null,
                CikkKeyDown
                );
        selectCikkekGrid = $(selectcikkekgridContainer).dxDataGrid('instance');
        /*selectCikkekGrid.e.columnOption('cik_nev', {
                    sortOrder: 'asc',
                    sortIndex: 0
            });*/
        selectCikkekGrid.option('onRowPrepared',cikkonRowPrepared);
        selectCikkekGrid.option('onRowClick',  function(e) {
                var component = e.component;
                if (!component.clickCount)
                    component.clickCount = 1;
                else
                    component.clickCount = component.clickCount + 1;
                if (component.clickCount == 1)
                {
                    component.lastClickTime = new Date();
                    setTimeout(function () { component.lastClickTime = 0; component.clickCount = 0; }, 350);
                }
                else if (component.clickCount == 2)
                {
                    if (((new Date()) - component.lastClickTime) < 300)
                    {
                        $("#frmInputBizonylatmennyiseg").modal();
                         $("#bizt_menny").focus();
                        $("#bizt_menny").select();
                    }
                    component.clickCount = 0;
                    component.lastClickTime = 0;
                }
            });
        selectCikkekGrid.option('onCellPrepared', function (e) {
            //if (e.rowType === "filter" && e.columnIndex === 1) {
            var filterindex = parseInt($("#felh_cikk_kereses").val());
            //console.log(filterindex);
            if (e.rowType === "filter" && e.columnIndex === filterindex ) {
                var cellElement = e.cellElement;
                editorElement = cellElement.find(".dx-texteditor-input");
            }
        }
                               );
        selectCikkekGrid.option('onContentReady', function () {
                editorElement.focus();
        });

    };
    function bizonylattetelek (){
//	getMezoadatok('t_cikkek _1');
    setBiztopkod ();

    var allUrl = '/biztetelek/'+$("#bizf_id").val();
	var bizonylattetelekgridContainer = '#bizonylattetelekgridContainer';
     var biztGridkey = '#bizt_id';

            var orders = komplexgrid.getgridorders(bizonylattetelekgridContainer,allUrl,biztGridkey);
			var gridDataSource = komplexgrid.setgridDataSource(allUrl );
    var starteditevent=function(e) {
			if (e.data.bizt_id) {
                $("#bizt_id").val(e.data.bizt_id);
				//if (e.column.dataField != "bizt_menny") {
				/*if (!komplexinput.in_array(e.column.dataField, bizonylattetelekEditfield)) {
						e.cancel = true;
						/*if (value > 10270) {
							e.cancel = true;
						}*/
				//	}
			}
			//DevExpress.ui.notify("EditingStart-->"+e.data.bizt_id, "info", 2000);
			//window.location.href = '/Inputcikkedit/'+e.data.cik_id;
        }
    var selectedevent=function(selectedItems) {
             var data = selectedItems.selectedRowsData[0];
            if(data) {
               $("#bizt_id").val(data.bizt_id);
            }

        }
     var deleted=function(e) {
			//DevExpress.ui.notify("Delete"+e.data.bizt_id, "info", 2000);
         var inputbizt = {
				  'bizt_id'     	: e.data.bizt_id
			 };

	   komplexinput.post('/bizteteldeleted', inputbizt, function(result){
            if (biztGrid) {
                biztGrid.refresh();
            }
            getbizonylatfej();
		});
			//window.location.href = '/Inputcikkedit/'+e.data.cik_id;
        }
     var inserted=function(e) {
			//DevExpress.ui.notify("start edit"+e.data.bizt_id, "info", 2000);
			//window.location.href = '/Inputcikkedit/'+e.data.cik_id;
        }
     var mastersummery ={
                totalItems: [{
                    column: "bizt_cik_nev",
                    summaryType: "count",
                    displayFormat: "Tételszám: {0}"
                }, {
                    column: "bizt_elad_ar",
                    summaryType: "sum",
                    //valueFormat: "currency",
                    valueFormat: "largeNumber",
                    displayFormat: "Összesen: {0}"

                },{
                    column: "bizt_brutto_elad_art",
                    summaryType: "sum",
                    valueFormat: "largeNumber",
                    displayFormat: "Összesen: {0}"

                }]
            };
        komplexgrid.setmasterGridHeight (300);
         var bizonylattetelekgrid = komplexgrid.setmastergrid(bizonylattetelekgridContainer,orders,bizonylattetelekMezoadatok,0,1,1,starteditevent,selectedevent,true,inserted,deleted,mastersummery);
        biztGrid = $(bizonylattetelekgridContainer).dxDataGrid('instance');
        //biztGrid.option('editing.mode', 'cell');
        biztGrid.option('onRowUpdated',  function(e) {
            getbizonylatfej();
        });
        biztGrid.option('onRowPrepared',bizonylattetelRowPrepared);

    };
    function setVisible (){
        $(".jovedeki").hide();
        $("#itembizf_afa_id").hide();
        $("#itembizf_rakt_id").hide();
        $("#itembizf_ervenyeseg").hide();
        $("#itembizf_fkod_id").hide();
        $("#itembizf_penznem_id").hide();
        $("#itembizf_fztm_id").hide();
        $("#itembizf_telj_datum").hide();
        $("#itembizf_esed_datum").hide();
        $("#itembizf_kif_datuma").hide();
        $("#itembizf_szla_szam").hide();
        $("#itembizf_szlev_szam").hide();
        $("#itembizf_atvevo_szallaitasi_prt_teljescime").hide();
        $("#itembizf_azon").hide();
        $("#itembizf_netto_beszerzesi_ert").hide();
        $("#itembizf_brutto_beszerzesi_ert").hide();
        $("#itembizf_netto_ert").hide();
        $("#itebizf_brutto_ert").hide();
        $("#Actszamla").hide();
        if (alapertekek.jovedekitermekkezeles == '1') {
            if ($("#jovedekitermek").val()=='I'){
                $(".jovedeki").show();
            }
            var inputObj = {
				  'jovedekitermek'	: $("#jovedekitermek").val()
			 };

            jQuery.ajaxSetup({async:false});
		    komplexinput.post('/lookupraktarak', inputObj, function(result){
//                console.log(result);
                $('#bizt_rakt_id').empty();
                result.forEach(function(item) {
						$('#bizt_rakt_id').append(
                        $('<option>', {
                            value: item.rakt_id,
                            text: item.rakt_nev
                        }, '</option>'))
                      }
                     );
                $("#bizt_rakt_id option:first").attr('selected','selected');
            });
            jQuery.ajaxSetup({async:true});
        }

    }
    function setBiztopkod (){
        var biztip_kod = $("#biztip_kod").val();
        //console.log(biztip_kod);
        if (biztip_kod=='S') {
            $("#itembizf_telj_datum").show();
            $("#itembizf_fztm_id").show();
            $("#itembizf_esed_datum").show();
            $("#itembizf_kif_datuma").show();
            $("#itembizf_azon").show();
            eladasiarak();
            if (alapertekek.bizonylatfej_afa_am == '1') {
                    $("#itembizf_afa_id").show();
            }

        }
        if (biztip_kod=='E') {
            $("#itembizf_telj_datum").show();
            $("#itembizf_fztm_id").show();
            $("#itembizf_esed_datum").show();
            $("#itembizf_kif_datuma").show();
            $("#itembizf_azon").show();
            eladasiarak();
            if (alapertekek.bizonylatfej_afa_am == '1') {
                    $("#itembizf_afa_id").show();
            }

        }
        if (biztip_kod=='A') {
            $("#itembizf_ervenyeseg").show();
            $("#itembizf_telj_datum").show();
            $("#itembizf_fztm_id").show();
            $("#itembizf_esed_datum").show();
            $("#itembizf_kif_datuma").show();
            $("#itembizf_azon").show();
            eladasiarak();
            if (alapertekek.bizonylatfej_afa_am == '1') {
                    $("#itembizf_afa_id").show();
            }

        }

    }
    function eladasiarak (){
        $("#itembizf_netto_ert").show();
        $("#itembizf_brutto_ert").show();

        bizonylattetelekMezoadatok =  komplexgrid.getadatok('t_biz_tetelek_1',[
            { dataField: 'bizt_id', caption: 'bizt_id',formItem: {
                                            visible: false
                                        },visible: false, enable: false},
			{ dataField: 'bizt_tet_sorszam', caption:'bizt_tet_sorszam',formItem: {
                                            visible: false
                                        }, allowEditing: false ,visible: false},
			{ dataField: 'bizt_cik_kod',width: 100,allowEditing: false,formItem: {
                                            visible: false
                                        }, caption: 'bizt_cik_kod'},
            { dataField: 'bizt_cik_nev',width: 100,allowEditing: false,formItem: {
                                            visible: false
                                        }, caption: 'bizt_cik_nev'}, //sortOrder: 'asc', sortIndex: 0},
            { dataField: 'bizt_nyilv_ar',width: 100, caption: 'bizt_nyilv_ar'},
            { dataField: 'bizt_brutto_nyilv_ar',width: 100,formItem: {
                                            visible: false
                                        }, allowEditing: false,caption: 'bizt_brutto_nyilv_ar'},
            { dataField: 'bizt_menny',width: 100, caption: 'bizt_menny'},
            { dataField: 'bizt_me_rovidites',allowEditing: false, caption: 'bizt_me_rovidites'},
            { dataField: 'bizt_elad_ar',formItem: {
                                            visible: false
                                        }, allowEditing: false, caption: 'bizt_elad_ar'},
            { dataField: 'bizt_brutto_elad_ar',formItem: {
                                                visible: false
                                            }
             , allowEditing: false,caption: 'bizt_brutto_elad_ar'},
            { dataField: 'bizt_afaertek',formItem: {
                                            visible: false
                                        }
             ,allowEditing: false, caption: 'bizt_afaertek'},
            { dataField: 'bizt_prt_egyenikedvezmeny', caption: 'bizt_prt_egyenikedvezmeny'},
            { dataField: 'bizt_jc_id',visible: true,formItem: {
                                                visible: false
                                            }, caption: 'bizt_jc_id'},
            { dataField: 'bizt_rakt_nev',visible: true,formItem: {
                                                visible: false
                                            }, caption: 'bizt_rakt_nev'},
            { dataField: 'bizt_mpraktkeszl_akt_keszl',visible: true,formItem: {
                                                visible: false
                                            }, caption: 'bizt_mpraktkeszl_akt_keszl'},
            { dataField: 'bizt_jc_keszlet_valtozasi_kod',visible: true,formItem: {
                                                visible: false
                                            }, caption: 'bizt_jc_keszlet_valtozasi_kod'}
			]);
	selectcikkekMezoadatok =  komplexgrid.getadatok('t_cikkek_2',[
            { dataField: 'cik_id', caption: 'cik_id',visible: false, enable: false},
			{ dataField: 'cik_cgy_gyariszam', caption: 'cik_cgy_gyariszam',visible: false},
			{ dataField: 'cik_kod', caption: 'Cikkszám'},
            { dataField: 'cik_nev', caption: 'cik_nev', sortOrder: 'asc', sortIndex: 0},
            { dataField: 'cik_eladasinettoar', caption: 'cik_eladasinettoar'},
            { dataField: 'cik_eladasibruttoar', caption: 'cik_eladasibruttoar'},
			{ dataField: 'cik_beszerzersinettoar', caption: 'cik_beszerzersinettoar',visible: false},
            { dataField: 'cik_beszerzersibruttoar', caption: 'cik_beszerzersibruttoar',visible: false},
			{ dataField: 'cik_pnzn_rovidites', caption: 'cik_pnzn_rovidites'},
			{ dataField: 'cik_fixaras_kod', caption: 'cik_fixaras_kod',visible: false},
			{ dataField: 'cik_aktiv', caption: 'cik_aktiv',visible: false},
			{ dataField: 'cik_akcios_ar', caption: 'cik_akcios_ar',visible: false},
			{ dataField: 'cik_vonal_azon', caption: 'cik_vonal_azon'},
			{ dataField: 'cik_mpraktkeszl_akt_keszl', caption: 'cik_mpraktkeszl_akt_keszl',visible: false},
			{ dataField: 'cik_mpraktkeszl_min_keszl', caption: 'cik_mpraktkeszl_min_keszl',visible: false},
			{ dataField: 'cik_min_keszl_alatt', caption: 'cik_min_keszl_alatt',visible: false},
			{ dataField: 'cik_egyseg_ar', caption: 'cik_egyseg_ar',visible: false},
			{ dataField: 'cik_forditottafa', caption: 'cik_forditottafa',visible: false},
			{ dataField: 'cik_keszlet_mod_kod', caption: 'cik_keszlet_mod_kod',visible: false},
			{ dataField: 'cik_csz_cikkszam', caption: 'cik_csz_cikkszam',visible: false},
			{ dataField: 'cik_me_id', caption: 'cik_me_id',visible: false},
			{ dataField: 'cik_afa_id', caption: 'cik_afa_id',visible: false},
			{ dataField: 'cik_me_rovidites', caption: 'cik_me_rovidites',visible: false}
			]);

    }
     function beszerzesiarakarak (){
        $("#itembizf_netto_beszerzesi_ert").show();
        $("#itembizf_brutto_beszerzesi_ert").show();
     }
    function getbizonylatfej() {
        var inputObj = {
				  'bizf_id'	: $("#bizf_id").val()
			 };

        komplexinput.post('/getbizfej', inputObj, function(result){
                    $("#bizf_azon").val(result.bizf_azon);
                    $("#bizf_netto_ert").val(result.bizf_netto_ert);
                    $("#bizf_brutto_ert").val(result.bizf_brutto_ert);
/*                  if (result.fztm_azonalifizetes) {
                         $("#bizf_kif_datuma").val($("#bizf_esed_datum").val());
                         $("#bizfkifdatuma").val($("#bizf_esed_datum").val());
                         $("#bizf_telj_datum").val($("#bizf_esed_datum").val());
                         $("#bizfteljdatum").val($("#bizf_esed_datum").val());
                  }*/
        });
    }
    function getatvevopartner(prt_id) {
        var inputObj = {
				  'prt_id'	: prt_id
			 };
        jQuery.ajaxSetup({async:false});
        komplexinput.post('/getOnePartner', inputObj, function(result){
                $("#bizt_artbl_arkategoria_kod_kod").val(result.prt_artbl_arkategoria_kod);
                $("#prt_egyenikedvezmeny").val(result.prt_egyenikedvezmeny);

        });
        jQuery.ajaxSetup({async:true});
    }
     function cikkonRowPrepared (info) {
            if (info.data) {
//                console.log(info);

                if (info.data.cik_fixaras_kod =='I') {
                    info.rowElement.addClass("fixarras");
                } else if (!info.data.cik_aktiv) {
                    info.rowElement.addClass("cikknemaktiv");
                } else if (info.data.cik_akcios_ar=='I') {
                        info.rowElement.addClass("clakciosar");
                } else if (info.data.cik_min_keszl_alatt) {
                        info.rowElement.addClass("clminimumkeszletalatt");
                }

            }

        };
     function bizonylattetelRowPrepared (info) {
            if (info.data) {
                var BIZTIP_KOD =$("#biztip_kod").val();
                ///console.log(BIZTIP_KOD);
                ///console.log(info.data.bizt_menny);
                //console.log(info.data.bizt_mpraktkeszl_akt_keszl-info.data.bizt_menny);

                    //if (info.data.bizt_jc_id ==100000192 ||info.data.bizt_jc_id ==100000171) {
                if ((BIZTIP_KOD =='S') || (BIZTIP_KOD == 'E') || (BIZTIP_KOD =='B')  || (BIZTIP_KOD =='b')  || (BIZTIP_KOD =='p')) {
                    if (info.data.bizt_jc_keszlet_valtozasi_kod == '0') {
                        info.rowElement.addClass("clnemmozgatkeszletet");
                    }
                }
                if (((BIZTIP_KOD =='S') || (BIZTIP_KOD == 'E') || (BIZTIP_KOD =='B')  || (BIZTIP_KOD =='b')  || (BIZTIP_KOD =='p'))
                 //   && (alapertekek.f_negativ_keszlet_figyeles == '1')
                    &&
                    (info.data.bizt_jc_keszlet_valtozasi_kod == '2' ||info.data.bizt_jc_keszlet_valtozasi_kod == '3')
                    &&
                    (info.data.bizt_mpraktkeszl_akt_keszl-info.data.bizt_menny)<0
                    ){
                    info.rowElement.addClass("clbiztminimumkeszletalatt");
                }
                if ((BIZTIP_KOD =='A') ) {
                    if (info.data.bizt_jc_keszlet_valtozasi_kod == '2' ||info.data.bizt_jc_keszlet_valtozasi_kod == '3') {
                        info.rowElement.addClass("clarajanlatmozgatkeszletet");
                    }

                }
            }

        };
function cikkraktariadatok(cikk_id,rakt_id) {
        var inputObj = {
				    'cikk_id'	: cikk_id,
                    'rakt_id'   : rakt_id
			 };
        komplexinput.post('/quselectcikkraktaradatok', inputObj, function(data){
//                    console.log(data);
                      if (data) {
                        $("#Lb_raktar").val(data.rakt_nev);
                        $("#ME_keszlet").val(data.raktsum);
                        $("#Me_sumkeszlet").val(data.allsum);
                        $("#lb_minkeszlet").val(data.min_keszl);
                      }
        },false);
    }
    /*function activaTab(tab){
        $('.nav-tabs a[href="#' + tab + '"]').tab('show');
    };*/
/*	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#profilkep_kozep').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }    */
	})();
