var app = angular.module('cikkgrid', ['ngRoute', 'dx']);

app.controller('cikkgridCtrl', function($scope, $http, $q) {
    var sqlData = new DevExpress.data.CustomStore({
        load: function (loadOptions) {
            var parameters = {};

            if (loadOptions.sort) {
                parameters.orderby = loadOptions.sort[0].selector;
                if (loadOptions.sort[0].desc)
                    parameters.orderby += " desc";
            }

            parameters.skip = loadOptions.skip || 0,
                parameters.take = loadOptions.take || 12

            var config = {
                params: parameters
            };

            return $http.get('/allcikkek', config)
                .then(function (response) {
                	console.log(parameters);
                    return { data: response.data.items, totalCount: response.data.totalCount };
                });

		}
    });

	$scope.dataGridOptions = {
	    dataSource: sqlData,

	    remoteOperations: {
	    	sorting: true,
	        paging: true
	    },
	    paging: {
	    	pageSize: 12
	    },
	    pager: {
	    	showPageSizeSelector: true,
	        allowedPageSizes: [8, 12, 20, 50],
	        showInfo: true
	    },
	    columns: [
	    	"cik_id", "cik_kod", "cik_nev"
	    ]
	};
});
