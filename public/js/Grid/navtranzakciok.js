var navtranzakciokkGrid = (function () {
    	var initEvents = [
		//Add field
		{
			  event	: "click"
			, target: "#Actclose"
			, fn	:  komplexinput.actclose
		}
	];


	var navtranzakciokkgridContainer = '#gridContainer';
	var navtranzakciofejekMezoadatok = komplexgrid.getadatok('t_navtranzakcio_fejek_1',[
            { dataField: 'nvtrf_id', caption: 'nvtrf_id',allowEditing: false, dataType: 'number', sortOrder: 'desc',visible: false,alignment: 'center', enable: false},
            { dataField: 'nvtrf_xchtkn_id', caption: 'nvtrf_xchtkn_id', dataType: 'string',alignment: 'center'},
            { dataField: 'nvtrf_xchtkn_exchangetoken',allowEditing: false, caption: 'nvtrf_xchtkn_exchangetoken',alignment: 'center'},
			{ dataField: 'nvtrf_xchtkn_validfrom',allowEditing: false, caption: 'nvtrf_xchtkn_validfrom',alignment: 'center'},
			{ dataField: 'nvtrf_xchtkn_validto',allowEditing: false,caption: 'nvtrf_xchtkn_validto',alignment: 'center'},
			{ dataField: 'nvtrf_transactionid',allowEditing: false,caption: 'nvtrf_transactionid',alignment: 'center'},
			{ dataField: 'nvtrf_senddatetime',allowEditing: false, caption: 'nvtrf_senddatetime',alignment: 'center'},
            { dataField: 'nvtrf_recevedatetime',allowEditing: false, caption: 'nvtrf_recevedatetime' ,alignment: 'center'},
            { dataField: 'nvtrf_letre_felh_nev',allowEditing: false, caption: 'nvtrf_letre_felh_nev' ,alignment: 'center'},
            { dataField: 'nvtrf_letre_dat',allowEditing: false, caption: 'nvtrf_letre_dat' ,alignment: 'center'},
            { dataField: 'nvtrf_status_kod',allowEditing: false, caption: 'nvtrf_status_kod' ,visoble: false,alignment: 'center'},
			{ dataField: 'nvtrf_status_kod_nev',allowEditing: false,visible: true, caption: 'nvtrf_status_kod_nev' ,alignment: 'center'},
			{ dataField: 'nvtrf_createdatetime',allowEditing: false,visible: true, caption: 'nvtrf_createdatetime' ,alignment: 'center'}
			]);
	var Navtranzakcio_tetelek_Mezoadatok = komplexgrid.getadatok('t_navtranzakcio_tetelek_1',[
			{ dataField: 'nvtrt_id', caption: 'nvtrt_id',alignment: 'center',visible: false, enable: false},
            { dataField: 'nvtrt_nvtrf_id', caption: 'bizf_id',visible: false,alignment: 'center'},
            { dataField: 'nvtrt_inv_bizf_azon', caption: 'nvtrt_inv_bizf_azon',alignment: 'center'},
            { dataField: 'nvtrt_inv_bizf_esed_datum', caption: 'nvtrt_inv_bizf_esed_datum',alignment: 'center'},
            { dataField: 'nvtrt_inv_bizf_telj_datum', caption: 'nvtrt_inv_bizf_telj_datum',alignment: 'center'},
            { dataField: 'nvtrt_inv_status_kod', caption: 'nvtrt_inv_status_kod',alignment: 'center'},
            { dataField: 'nvtrt_inv_status_kod_nev', caption: 'nvtrt_inv_status_kod_nev',alignment: 'center'},
            { dataField: 'nvtrt_inv_invoicedata', caption: 'nvtrt_inv_invoicedata',alignment: 'center'},
            { dataField: 'nvtrt_inv_filename', caption: 'nvtrt_inv_filename',alignment: 'center'},
            { dataField: 'nvtrt_inv_invoicedata', caption: 'nvtrt_inv_invoicedata',alignment: 'center'},
            { dataField: 'nvtrt_index', caption: 'nvtrt_index',alignment: 'center'},
            { dataField: 'nvtrt_letre_felh_nev', caption: 'nvtrt_letre_felh_nev',alignment: 'center'},
            { dataField: 'nvtrt_letre_dat', caption: 'nvtrt_letre_dat',alignment: 'center'}
			]);
			var orders = komplexgrid.getgridorders(navtranzakciokkgridContainer,'/navtranzakcioAll');
			var gridDataSource = komplexgrid.setgridDataSource('/navtranzakcioAll');
			var mastersummery ={
            totalItems: [{
                column: "nvtrf_id",
                summaryType: "count",
				displayFormat: "Tételszám: {0}"
            }]
        };
		var detailsummary= {
							totalItems: [{
								column: "nvtrt_id",
								summaryType: "count",
								displayFormat: "Tételszám: {0}"
							}]
						};
    var masterdetailgrid =null;
return {
		  init			: init
		, destruct		: destruct

	}
    function init () {
        $("#menusor").hide();
        komplexinput.loading ();
        initEvents.forEach(function(ev){
                $(document).on(ev.event, ev.target, ev.fn);
            });
        komplexgrid.setmasterdetailgrid(navtranzakciokkgridContainer,
                                        orders,
                                        navtranzakciofejekMezoadatok,
                                        Navtranzakcio_tetelek_Mezoadatok,
                                        mastersummery,
                                        detailsummary,
                                        0,1,1);
        masterdetailgrid = $(navtranzakciokkgridContainer).dxDataGrid('instance');

         komplexinput.loaded();
    }
    function destruct () {
	 	initEvents.forEach(function (ev) {
			$(document).off(ev.event, ev.target);
		});
	}
})();
