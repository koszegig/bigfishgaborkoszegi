var Raktarakgrid = (function () {
    	var initEvents = [
		//Add field
		{
			  event	: "click"
			, target: "#Actclose"
			, fn	:  komplexinput.actclose
		}
	];

	var RaktarakgridContainer = '#gridRaktarakContainer';
	var RaktarakMezoadatok =  komplexgrid.getadatok('t_raktarak_1s',[
            { dataField: 'rakt_id', caption: 'rakt_id', enable: false},
            { dataField: 'rakt_prt_nev', caption: 'rakt_prt_nev'},
			{ dataField: 'rakt_nev', caption: 'rakt_nev'}
			]);
    		var orders = komplexgrid.getgridorders(RaktarakgridContainer,'/RaktarakAll');
			var gridDataSource = komplexgrid.setgridDataSource('/RaktarakAll');
     var mastergrid = null;
    return {
        init				: init,
        RaktarakMezoadatok	: RaktarakMezoadatok,
        mastergrid			: mastergrid
    }

    function init () {
       $("#menusor").hide();
          komplexinput.loading ();
        initEvents.forEach(function(ev){
                $(document).on(ev.event, ev.target, ev.fn);
            });
        komplexgrid.setmastergrid(RaktarakgridContainer,orders,RaktarakMezoadatok,1,1,1 );
        mastergrid = $(RaktarakgridContainer).dxDataGrid('instance');
         komplexinput.loaded();
    }
    function destruct () {
	 	initEvents.forEach(function (ev) {
			$(document).off(ev.event, ev.target);
		});
	}
})();

