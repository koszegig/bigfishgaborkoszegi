var LeltarivekGrid = (function () {
    	var initEvents = [
		//Add field
		{
			  event	: "click"
			, target: "#Actclose"
			, fn	:  komplexinput.actclose
		}
	];

	var LeltarivgridContainer = '#gridContainer';
	var LeltarivfejekMezoadatok = komplexgrid.getadatok('t_leltariv_fejek_1',[
            { dataField: 'livf_id', caption: 'livf_id',allowEditing: false, dataType: 'number', sortOrder: 'desc',visible: false,alignment: 'center', enable: false},
            { dataField: 'livf_leltf_datuma', caption: 'leltf_ertekelesi_sorszam', dataType: 'string',alignment: 'center'},
            { dataField: 'livf_leltf_rakt_nev',allowEditing: false, caption: 'leltf_ertekelesi_kod_nev',alignment: 'center'},
			{ dataField: 'livf_iker_sorszam',allowEditing: false, caption: 'leltf_datuma',alignment: 'center'},
			{ dataField: 'livf_ertekelesi_sorszam',allowEditing: false,caption: 'leltf_rakt_nev',alignment: 'center'},
			{ dataField: 'livf_livszam',allowEditing: false,caption: 'leltf_felvetel_eszkoz_kod_nev',alignment: 'center'},
			{ dataField: 'livf_prt_nev',allowEditing: false, caption: 'leltf_teljes_kod_nev',alignment: 'center'},
            { dataField: 'livf_rtmbl_nev',allowEditing: false, caption: 'leltf_status_kod_nev' ,alignment: 'center'},
            { dataField: 'livf_tetel_szam',allowEditing: false, caption: 'leltf_status_kod_nev' ,alignment: 'center'},
            { dataField: 'livf_leltf_status_kod_nev',allowEditing: false, caption: 'leltf_lezaras_idopontja' ,alignment: 'center'},
            { dataField: 'livf_letre_dat',allowEditing: false, caption: 'leltf_netto' ,visoble: false,alignment: 'center'},
			{ dataField: 'livf_leltf_id',allowEditing: false,visible: false, caption: 'leltf_brutto' ,alignment: 'center'},
			{ dataField: 'livf_leltf_rakt_id',allowEditing: false,visible: false, caption: 'leltf_brutto' ,alignment: 'center'},
			{ dataField: 'livf_letre_felh_nev',allowEditing: false, caption: 'leltf_netto_hiany' ,alignment: 'center'},
			{ dataField: 'livf_leltf_ertekelesi_kod_nev',allowEditing: false, caption: 'leltf_brutto_hiany' ,alignment: 'center'},
			{ dataField: 'livf_leltf_felvetel_eszkoz_kod_nev',allowEditing: false, caption: 'leltf_netto_tobblet' ,alignment: 'center'},
			{ dataField: 'livf_statuskod_kod_nev',allowEditing: false, caption: 'livf_statuskod_kod_nev' ,alignment: 'center'}
			]);
	var LeltarivtetelekMezoadatok = komplexgrid.getadatok('t_leltariv_tetelek_1',[
			{ dataField: 'livt_id', caption: 'leltk_id',alignment: 'center',visible: false, enable: false},
            { dataField: 'livt_cik_id', caption: 'bizf_id',visible: false,alignment: 'center'},
            { dataField: 'livt_cik_kod', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'livt_cik_nev', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'livt_menny', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'livt_nyilv_menny', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'livt_cik_me_nev', caption: 'bizf_id',alignment: 'center'}
			]);
			var orders = komplexgrid.getgridorders(LeltarivgridContainer,'/LeltarivekAll');
			var gridDataSource = komplexgrid.setgridDataSource('/LeltarivekAll');
			var mastersummery ={
            totalItems: [{
                column: "livf_id'",
                summaryType: "count",
				displayFormat: "Tételszám: {0}"
            }]
        };
		var detailsummary= {
							totalItems: [{
								column: "livt_id",
								summaryType: "count",
								displayFormat: "Tételszám: {0}"
							}]
						};
    var masterdetailgrid =null;
return {
		  init			: init
		, destruct		: destruct

	}
    function init () {
        $("#menusor").hide();
        komplexinput.loading ();
        initEvents.forEach(function(ev){
                $(document).on(ev.event, ev.target, ev.fn);
            });
        komplexgrid.setmasterdetailgrid(LeltarivgridContainer,
                                        orders,
                                        LeltarivfejekMezoadatok,
                                        LeltarivtetelekMezoadatok,
                                        mastersummery,
                                        detailsummary,
                                        0,1,1);
        masterdetailgrid = $(LeltarivgridContainer).dxDataGrid('instance');

         komplexinput.loaded();
    }
    function destruct () {
	 	initEvents.forEach(function (ev) {
			$(document).off(ev.event, ev.target);
		});
	}
})();
