var fizetesimodokgrid = (function () {
    	var initEvents = [
		//Add field
		{
			  event	: "click"
			, target: "#Actclose"
			, fn	:  komplexinput.actclose
		}
	];

	var fizetesimodokgridContainer = '#gridfizetesimodokContainer';
	var fizetesimodokMezoadatok =  komplexgrid.getadatok('t_fizetesimod_1',[
            { dataField: 'fztm_id', caption: 'fztm_id',visible: false, enable: false},
            { dataField: 'fztm_megnevezes', caption: 'fztm_megnevezes'},
			{ dataField: 'fztm_tipus_kod', caption: 'fztm_tipus_kod'},
            { dataField: 'fztm_kod_nev', caption: 'fztm_kod_nev'},
            { dataField: 'fztm_fizetesinapok', caption: 'fztm_fizetesinapok'},
            { dataField: 'fztm_kassza', caption: 'fztm_kassza'},
			{ dataField: 'fztm_kassza_sorrend', caption: 'fztm_kassza_sorrend'},
            { dataField: 'fztm_alap', caption: 'fztm_alap'},
            { dataField: 'fztm_kerekites', caption: 'fztm_kerekites'},
            { dataField: 'fztm_szamlan_a_megnevezes', caption: 'fztm_szamlan_a_megnevezes'}
			]);
    		var orders = komplexgrid.getgridorders(fizetesimodokgridContainer,'/allfizetesimodok');
			var gridDataSource = komplexgrid.setgridDataSource('/allfizetesimodok');
     var mastergrid = null;
    return {
        init				: init,
        fizetesimodokMezoadatok	: fizetesimodokMezoadatok,
        mastergrid			: mastergrid
    }

    function init () {
        $("#menusor").hide();
         komplexinput.loading ();
        initEvents.forEach(function(ev){
                $(document).on(ev.event, ev.target, ev.fn);
        });
        komplexgrid.setmastergrid(fizetesimodokgridContainer,orders,fizetesimodokMezoadatok,1,1,1 );
        mastergrid = $(fizetesimodokgridContainer).dxDataGrid('instance');
         komplexinput.loaded();
    }
    function destruct () {
	 	initEvents.forEach(function (ev) {
			$(document).off(ev.event, ev.target);
		});
	}
})();
