
var partnergrid = (function () {
    	var initEvents = [
		//Add field
		{
			  event	: "click"
			, target: "#Actclose"
			, fn	:  komplexinput.actclose
		}
	];
//	getMezoadatok('t_cikkek _1');
	var partnergridContainer = '#gridContainer';
	var partnerMezoadatok =  komplexgrid.getadatok('t_partnerek_1',[
            { dataField: 'prt_id', caption: 'prt_id',visible: false, enable: false},
            { dataField: 'prt_nev', caption: 'prt_nev'},
			{ dataField: 'prt_cegkod', caption: 'prt_cegkod'},
            { dataField: 'prt_alap_teljescime', caption: 'prt_alap_teljescime'}, //sortOrder: 'asc', sortIndex: 0},
            { dataField: 'prt_fztm_megnevezes', caption: 'prt_fztm_megnevezes'},
            { dataField: 'prt_utalasi_nap', caption: 'prt_utalasi_nap'},
			{ dataField: 'prt_bsz_bankszamlaszam', caption: 'prt_bsz_bankszamlaszam'},
            { dataField: 'prt_prtelhtsg_mobilszam', caption: 'prt_prtelhtsg_mobilszam'}
			]);
    		var orders = komplexgrid.getgridorders(partnergridContainer,'/allpartner');
			var gridDataSource = komplexgrid.setgridDataSource('/allpartner');
    var starteditevent=function(e) {
			DevExpress.ui.notify("EditingStart-->"+e.data.prt_id, "info", 2000);
			window.location.href = '/FrmInputpartner/'+e.data.prt_id;
            //logEvent("EditingStart");

        }
     var mastergrid = null;
    return {
        init				: init,
        partnerMezoadatok	: partnerMezoadatok,
        starteditevent		: starteditevent,
        mastergrid			: mastergrid
    }

    function init () {
       $("#menusor").hide();
        komplexinput.loading ();
        initEvents.forEach(function(ev){
                $(document).on(ev.event, ev.target, ev.fn);
            });
        komplexgrid.setmastergrid(partnergridContainer,orders,partnerMezoadatok,1,1,1,starteditevent );
         mastergrid = $(partnergridContainer).dxDataGrid('instance');
         komplexinput.loaded();
    }
    function destruct () {
	 	initEvents.forEach(function (ev) {
			$(document).off(ev.event, ev.target);
		});
	}
})();

