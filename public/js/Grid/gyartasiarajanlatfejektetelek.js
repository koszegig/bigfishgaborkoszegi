var gyartasiarajanlatfejektetelekGrid = (function () {
    	var initEvents = [
		//Add field
		{
			  event	: "click"
			, target: "#Actclose"
			, fn	:  komplexinput.actclose
		}
	];

	var gyartasiarajanlatfejektetelekgridContainer = '#gridContainer';
	var gyartasiarajanlatfejekMezoadatok = komplexgrid.getadatok('t_gyartasi_arajanlatfejek_1',[
            { dataField: 'gyajkf_id', caption: 'bizf_id',allowEditing: false, dataType: 'number', sortOrder: 'desc',visible: false,alignment: 'center', enable: false},
            { dataField: 'gyajkf_megnevezes', caption: 'gyajkf_megnevezes', dataType: 'string',alignment: 'center'},
            { dataField: 'gyajkf_prt_id',allowEditing: false,visible: false, caption: 'kelte ',alignment: 'center'},
            { dataField: 'gyajkf_prt_nev',allowEditing: false, caption: 'bizf_atado_prt_nev',alignment: 'center'}, //sortOrder: 'asc', sortIndex: 0},
			{ dataField: 'gyajkf_vsz_prt_id',allowEditing: false, caption: 'bizf_fizmod_kod_nev ',alignment: 'center'},
			{ dataField: 'gyajkf_vsz_prt_nev',allowEditing: false,visible: false, caption: 'bizf_telj_datum ',alignment: 'center'},
			{ dataField: 'gyajkf_ugynok_prt_id',allowEditing: false,visible: false, caption: 'bizf_kif_datuma ',alignment: 'center'},
			{ dataField: 'gyajkf_ugynok_prt_nev',allowEditing: false, caption: 'bizf_kifizetes_datuma ',alignment: 'center'},
            { dataField: 'gyajkf_status_kod_nev',allowEditing: false, caption: 'bizf_netto_ert' ,alignment: 'center'},
            { dataField: 'gyajkf_ervenyes',allowEditing: false, caption: 'bizf_afa_ertek' ,alignment: 'center',format: 'currency'},
            { dataField: 'gyajkf_nettoertek',allowEditing: false, caption: 'bizf_maradekosszeg,' ,visoble: false,alignment: 'center',format: 'currency'},
			{ dataField: 'gyajkf_bruttoertek',allowEditing: false, caption: 'bizf_penznem_nev' ,alignment: 'center',format: 'currency'},
			{ dataField: 'gyajkf_afa_nev',allowEditing: false, caption: 'bizf_feldolg_status_kod' ,alignment: 'center'},
			{ dataField: 'gyajkf_penznem_nev',allowEditing: false, caption: 'bizf_sztorno_bizf_id' ,alignment: 'center'},
			{ dataField: 'gyajkf_azon',allowEditing: false, caption: 'bizf_sztornozott_bizf_id' ,alignment: 'center'},
			{ dataField: 'gyajkf_kifizetett_brutto_ert',allowEditing: false, caption: 'bizf_azon_biztip_kod' ,alignment: 'center'},
			{ dataField: 'gyajkf_kifizetes_datuma',allowEditing: false, caption: 'bizf_ukoto_prt_nev' ,alignment: 'center'}
			]);
	var gyartasiarajanlattetelekMezoadatok = komplexgrid.getadatok('t_gyartasi_arajanlattetelek_1',[
			{ dataField: 'gyajkt_id', caption: 'gyajkt_id',alignment: 'center',visible: false, enable: false},
			{ dataField: 'gyajkt_ttrmkf_megnevezes', caption: 'bizf_id',alignment: 'center', enable: false}

			]);
			var orders = komplexgrid.getgridorders(gyartasiarajanlatfejektetelekgridContainer,'/GyartasiarajanlatfejAll');
			var gridDataSource = komplexgrid.setgridDataSource('/GyartasiarajanlatfejAll');
			var mastersummery ={
            totalItems: [{
                column: "gyajkf_id",
                summaryType: "count",
				displayFormat: "Tételszám: {0}"
            }, {
                column: "gyajkf_nettoertek",
                summaryType: "sum",
				displayFormat: "Összesen: {0}"

            },{
                column: "gyajkf_bruttoertek",
                summaryType: "sum",
				displayFormat: "Összesen: {0}"

            }]
        };
		var detailsummary= {
							totalItems: [{
								column: "bizt_id",
								summaryType: "count",
								displayFormat: "Tételszám: {0}"
							}, {
								column: "bizt_elad_ar",
								summaryType: "sum",
                                valueFormat: "Number",
								displayFormat: "Összesen: {0}"

							},{
								column: "bizt_brutto_elad_ar",
								summaryType: "sum",
                                valueFormat: "Number",
								displayFormat: "Összesen: {0}"

							}]
						};
    var masterdetailgrid =null;
return {
		  init			: init
		, destruct		: destruct

	}
    function init () {
        $("#menusor").hide();
        komplexinput.loading ();
        initEvents.forEach(function(ev){
                $(document).on(ev.event, ev.target, ev.fn);
            });
        komplexgrid.setmasterdetailgrid(gyartasiarajanlatfejektetelekgridContainer,
                                        orders,
                                        gyartasiarajanlatfejekMezoadatok,
                                        gyartasiarajanlattetelekMezoadatok,
                                        mastersummery,
                                        detailsummary,
                                        0,1,1);
        masterdetailgrid = $(gyartasiarajanlatfejektetelekgridContainer).dxDataGrid('instance');

         komplexinput.loaded();
    }
    function destruct () {
	 	initEvents.forEach(function (ev) {
			$(document).off(ev.event, ev.target);
		});
	}
})();
