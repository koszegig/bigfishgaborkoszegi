var bizonylatfejmozgasnemgrid = (function () {
    	var initEvents = [
		//Add field
		{
			  event	: "click"
			, target: "#Actclose"
			, fn	:  komplexinput.actclose
		}
	];
	var bizonylatfejmozgasnemgridContainer = '#gridbizonylatfejmozgasnemContainer';
	var bizonylatfejmozgasnemMezoadatok =  komplexgrid.getadatok('t_bizonylatfejmozgasnem_1',[
            { dataField: 'bizfmgsn_id', caption: 'bizfmgsn_id', enable: false},
            { dataField: 'bizfmgsn_rovidites', caption: 'bizfmgsn_rovidites'},
			{ dataField: 'bizfmgsn_nev', caption: 'bizfmgsn_nev'},
            { dataField: 'bizfmgsn_aktiv', caption: 'bizfmgsn_aktiv'}
			]);
    		var orders = komplexgrid.getgridorders(bizonylatfejmozgasnemgridContainer,'/bizonylatfejmozgasnem');
			var gridDataSource = komplexgrid.setgridDataSource('/bizonylatfejmozgasnem');
     var mastergrid = null;
    return {
        init				            : init,
        bizonylatfejmozgasnemMezoadatok	: bizonylatfejmozgasnemMezoadatok,
        mastergrid			            : mastergrid
    }

    function init () {
        $("#menusor").hide();
          komplexinput.loading ();
        initEvents.forEach(function(ev){
                $(document).on(ev.event, ev.target, ev.fn);
            });
        komplexgrid.setmastergrid(bizonylatfejmozgasnemgridContainer,orders,bizonylatfejmozgasnemMezoadatok,1,1,1 );
        mastergrid = $(bizonylatfejmozgasnemgridContainer).dxDataGrid('instance');
         komplexinput.loaded();
    }
    function destruct () {
	 	initEvents.forEach(function (ev) {
			$(document).off(ev.event, ev.target);
		});
	}
})();

