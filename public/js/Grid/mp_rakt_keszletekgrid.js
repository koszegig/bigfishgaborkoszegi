var mp_rakt_keszletekgrid = (function () {
    	var initEvents = [
		//Add field
		{
			  event	: "click"
			, target: "#Actclose"
			, fn	:  komplexinput.actclose
		}
	];
//    getMezoadatok('t_cikkek_1');
	var mp_rakt_keszletekgridContainer = '#gridContainer';
    var mp_rakt_keszletekMezoadatok = komplexgrid.getadatok('t_mp_rakt_keszletek_1',[
            { dataField: 'mpraktkeszl_id',visible: false, caption: 'cik_id', enable: false},
            { dataField: 'mpraktkeszl_cik_id', visible: false,caption: 'Kiskernetto'},
            { dataField: 'mpraktkeszl_cik_aktiv', caption: 'cik_nev'}, //sortOrder: 'asc', sortIndex: 0},
            { dataField: 'mpraktkeszl_vonal_azon', caption: 'cik_nev'}, //sortOrder: 'asc', sortIndex: 0},
            { dataField: 'mpraktkeszl_cik_kod', caption: 'cik_nev'}, //sortOrder: 'asc', sortIndex: 0},
            { dataField: 'mpraktkeszl_cik_nev', caption: 'cik_nev'}, //sortOrder: 'asc', sortIndex: 0},
            { dataField: 'mpraktkeszl_rakt_nev', caption: 'Kiskernetto'},
            { dataField: 'mpraktkeszl_akt_keszl', caption: 'Bruttó ár'},
			{ dataField: 'mpraktkeszl_fogl_keszl', caption: 'Bruttó ár'},
			{ dataField: 'mpraktkeszl_min_keszl', caption: 'Bruttó ár'},
			{ dataField: 'mpraktkeszl_max_keszl', caption: 'Bruttó ár'},
			{ dataField: 'mpraktkeszl_nyilv_ar', caption: 'Bruttó ár'},
			{ dataField: 'mpraktkeszl_brutto_nyilv_ar', caption: 'Bruttó ár'},
			{ dataField: 'mpraktkeszl_ut_besz_dat', caption: 'Bruttó ár'},
			{ dataField: 'mpraktkeszl_ut_besz_ar', caption: 'Bruttó ár'},
			{ dataField: 'mpraktkeszl_ut_elad_dat', caption: 'Bruttó ár'},
			{ dataField: 'mpraktkeszl_ut_elad_ar', caption: 'Bruttó ár'},
			{ dataField: 'mpraktkesz_cik_jovedeki_termek', caption: 'Bruttó ár'},
			{ dataField: 'mpraktkeszl_1_akt_keszl', caption: 'Bruttó ár'},
			{ dataField: 'mpraktkeszl_2_akt_keszl', caption: 'Bruttó ár'},
			{ dataField: 'mpraktkeszl_3_akt_keszl', caption: 'Bruttó ár'},
			{ dataField: 'mpraktkeszl_4_akt_keszl', caption: 'Bruttó ár'},
			{ dataField: 'mpraktkeszl_5_akt_keszl', caption: 'Bruttó ár'},
			{ dataField: 'mpraktkeszl_6_akt_keszl', caption: 'Bruttó ár'},
			{ dataField: 'mpraktkeszl_7_akt_keszl', caption: 'Bruttó ár'},
			{ dataField: 'mpraktkeszl_8_akt_keszl', caption: 'Bruttó ár'},
			{ dataField: 'mpraktkeszl_9_akt_keszl', caption: 'Bruttó ár'},
			{ dataField: 'mpraktkeszl_10_akt_keszl', caption: 'Bruttó ár'},
			{ dataField: 'mpraktkeszl_11_akt_keszl', caption: 'Bruttó ár'},
			{ dataField: 'mpraktkeszl_12_akt_keszl', caption: 'Bruttó ár'},
			{ dataField: 'mpraktkeszl_el_besz_ar', caption: 'Bruttó ár'},
			{ dataField: 'mpraktkeszl_el_besz_dat', caption: 'Bruttó ár'},
			{ dataField: 'mpraktkeszl_el_keszlet_ertek', caption: 'Bruttó ár'},
			{ dataField: 'mpraktkeszl_ut_keszlet_ertek', caption: 'Bruttó ár'},
			{ dataField: 'mpraktkeszl_ut_brutto_keszlet_ertek', caption: 'Bruttó ár'}
            ]);
            var orders = komplexgrid.getgridorders(mp_rakt_keszletekgridContainer,'/allraktkeszletek');
            var gridDataSource = komplexgrid.setgridDataSource('/allraktkeszletek');
    var starteditevent=function(e) {
            ///DevExpress.ui.notify("EditingStart-->"+e.data.cik_id, "info", 2000);
            //window.location.href = '/Inputcikkedit/'+e.data.cik_id;
            //logEvent("EditingStart");
        }
     var mastergrid = null;
  return {
        init            				: init,
        mp_rakt_keszletekMezoadatok 	: mp_rakt_keszletekMezoadatok ,
        starteditevent	             	: starteditevent,
        mastergrid		            	: mastergrid
    }
   function init () {
       $("#menusor").hide();
          komplexinput.loading ();
        initEvents.forEach(function(ev){
                $(document).on(ev.event, ev.target, ev.fn);
            });
    komplexgrid.setmastergrid(mp_rakt_keszletekgridContainer,orders,mp_rakt_keszletekMezoadatok,0,0,0,starteditevent );
      mastergrid = $(mp_rakt_keszletekgridContainer).dxDataGrid('instance');
         komplexinput.loaded();
    }
    function destruct () {
	 	initEvents.forEach(function (ev) {
			$(document).off(ev.event, ev.target);
		});
	}
})();

