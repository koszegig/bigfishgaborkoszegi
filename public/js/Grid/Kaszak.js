var Kaszakgrid = (function () {
    	var initEvents = [
		//Add field
		{
			  event	: "click"
			, target: "#Actclose"
			, fn	:  komplexinput.actclose
		}
	];

	var KaszakgridContainer = '#gridKaszakcontainer';
	var KaszakMezoadatok =  komplexgrid.getadatok('t_kaszak_1',[
            { dataField: 'kszk_id', caption: 'kszk_id',visible: false, enable: false},
            { dataField: 'kszk_kod', caption: 'pnzn_rovidites'},
			{ dataField: 'kszk_nev', caption: 'kszk_nev'},
            { dataField: 'kszk_aktiv', caption: 'kszk_aktiv'},
            { dataField: 'kszk_default', caption: 'kszk_default'}
			]);
    		var orders = komplexgrid.getgridorders(KaszakgridContainer,'/allkasszak');
			var gridDataSource = komplexgrid.setgridDataSource('/allkasszak');
     var mastergrid = null;
    return {
        init				: init,
        KaszakMezoadatok	: KaszakMezoadatok,
        mastergrid			: mastergrid
    }

    function init () {
       $("#menusor").hide();
          komplexinput.loading ();
        initEvents.forEach(function(ev){
                $(document).on(ev.event, ev.target, ev.fn);
            });
        komplexgrid.setmastergrid(KaszakgridContainer,orders,KaszakMezoadatok,1,1,1 );
        mastergrid = $(KaszakgridContainer).dxDataGrid('instance');
         komplexinput.loaded();
    }
    function destruct () {
	 	initEvents.forEach(function (ev) {
			$(document).off(ev.event, ev.target);
		});
	}
})();
