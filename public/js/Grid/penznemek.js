var penznemekgrid = (function () {
    	var initEvents = [
		//Add field
		{
			  event	: "click"
			, target: "#Actclose"
			, fn	:  komplexinput.actclose
		}
	];

	var penznemekgridContainer = '#gridpenznemekContainer';
	var penznemekMezoadatok =  komplexgrid.getadatok('t_penznem_1',[
            { dataField: 'pnzn_id', caption: 'pnzn_id',visible: false, enable: false},
            { dataField: 'pnzn_rovidites', caption: 'pnzn_rovidites'},
			{ dataField: 'pnzn_nev', caption: 'pnzn_nev'},
            { dataField: 'pnz_aktiv', caption: 'pnz_aktiv'},
            { dataField: 'pnzn_kerekit', caption: 'pnzn_kerekit'},
            { dataField: 'pnzn_szamlaszam', caption: 'pnzn_szamlaszam'},
			{ dataField: 'pnzn_jel', caption: 'pnzn_jel'}
			]);
    		var orders = komplexgrid.getgridorders(penznemekgridContainer,'/allpenznemek');
			var gridDataSource = komplexgrid.setgridDataSource('/allpenznemek');
     var mastergrid = null;
    return {
        init				: init,
        penznemekMezoadatok	: penznemekMezoadatok,
        mastergrid			: mastergrid
    }

    function init () {
        $("#menusor").hide();
        komplexinput.loading ();
        initEvents.forEach(function(ev){
                $(document).on(ev.event, ev.target, ev.fn);
            });
        komplexgrid.setmastergrid(penznemekgridContainer,orders,penznemekMezoadatok,1,1,1 );
        mastergrid = $(penznemekgridContainer).dxDataGrid('instance');
        komplexinput.loaded();
    }
    function destruct () {
	 	initEvents.forEach(function (ev) {
			$(document).off(ev.event, ev.target);
		});
	}
})();
