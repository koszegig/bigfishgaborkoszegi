var azonositokGrid = (function () {
    	var initEvents = [
		//Add field
		{
			  event	: "click"
			, target: "#Actclose"
			, fn	:  komplexinput.actclose
		}
	];

	var azonositokgridContainer = '#gridazonositokContainer';
	var azonositokMezoadatok =  komplexgrid.getadatok('t_azonositok_1',[
            { dataField: 'azon_id', caption: 'azon_id',visible: false, enable: false},
            { dataField: 'azon_biztip_kod_nev', caption: 'azon_biztip_kod_nev'},
			{ dataField: 'azon_ev', caption: 'azon_ev'},
            { dataField: 'azon_nyito', caption: 'azon_nyito'}, //sortOrder: 'asc', sortIndex: 0},
            { dataField: 'azon_kezdo', caption: 'azon_kezdo'},
            { dataField: 'azon_befejezo', caption: 'azon_befejezo'},
			{ dataField: 'azon_atadott', caption: 'azon_atadott'},
            { dataField: 'azon_default', caption: 'azon_default'},
            { dataField: 'azon_nyilvar_hatas_in_kod_nev', caption: 'azon_nyilvar_hatas_in_kod_nev'}
			]);
    		var orders = komplexgrid.getgridorders(azonositokgridContainer,'/allazonositok');
			var gridDataSource = komplexgrid.setgridDataSource('/allazonositok');
     var mastergrid = null;
    return {
        init				: init,
        azonositokMezoadatok	: azonositokMezoadatok,
        mastergrid			: mastergrid
    }

return {
		  init			: init
		, destruct		: destruct

	}
    function init () {
        $("#menusor").hide();
        komplexinput.loading ();
        initEvents.forEach(function(ev){
                $(document).on(ev.event, ev.target, ev.fn);
            });
        komplexgrid.setmastergrid(azonositokgridContainer,orders,azonositokMezoadatok,1,1,1 );
        mastergrid = $(azonositokgridContainer).dxDataGrid('instance');
         komplexinput.loaded();
    }
 function destruct () {
	 	initEvents.forEach(function (ev) {
			$(document).off(ev.event, ev.target);
		});
	}
})();

