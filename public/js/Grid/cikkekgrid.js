var cikkekgrid = (function () {
    	var initEvents = [
		//Add field
		{
			  event	: "click"
			, target: "#Actclose"
			, fn	:  komplexinput.actclose
		}
	];
//	getMezoadatok('t_cikkek _1');
	var cikkgridContainer = '#gridContainer';
	var cikkekMezoadatok =  komplexgrid.getadatok('t_cikkek_1',[
            { dataField: 'cik_id', caption: 'cik_id',visible: false, enable: false},
            { dataField: 'cikkek_cikcsop_nev', caption: 'cikkek_cikcsop_nev'},
			{ dataField: 'cik_kod', caption: 'Cikkszám'},
            { dataField: 'cik_nev', caption: 'cik_nev'}, //sortOrder: 'asc', sortIndex: 0},
            { dataField: 'cik_me_rovidites', caption: 'cik_me_rovidites'},
            { dataField: 'cik_beszerzersinettoar', caption: 'cik_beszerzersinettoar'},
			{ dataField: 'cik_beszerzersibruttoar', caption: 'cik_beszerzersibruttoar'},
            { dataField: 'cik_kiskernettoar', caption: 'Kiskernetto'},
			{ dataField: 'cik_kiskerbruttoar', caption: 'Bruttó ár'},
			{ dataField: 'cik_forditottafa_kod_nev', caption: 'cik_forditottafa_kod_nev'},
			{ dataField: 'cik_beszmodidopontja', caption: 'cik_beszmodidopontja'},
			{ dataField: 'cik_vonal_azon', caption: 'cik_vonal_azon'},
			{ dataField: 'cik_aktiv', caption: 'cik_aktiv'}
			]);
    		var orders = komplexgrid.getgridorders(cikkgridContainer,'/allcikkek');
			var gridDataSource = komplexgrid.setgridDataSource('/allcikkek');

    var starteditevent=function(e) {
			//DevExpress.ui.notify("EditingStart-->"+e.data.cik_id, "info", 2000);
			window.location.href = '/Inputcikkedit/'+e.data.cik_id;
            //logEvent("EditingStart");

        }
     var mastergrid = null;
    return {
        init				: init,
        cikkekMezoadatok	: cikkekMezoadatok,
        starteditevent		: starteditevent,
        mastergrid			: mastergrid
    }

    function init () {
       $("#menusor").hide();
          komplexinput.loading ();
        initEvents.forEach(function(ev){
                $(document).on(ev.event, ev.target, ev.fn);
            });
        /*var gridView = komplexgrid.getGridView($("#menu_id").val());
        if ( gridView.vw_master_fields!=null ) {
            console.log(gridView.vw_master_fields);
        }*/

        komplexgrid.setmastergrid(cikkgridContainer,orders,cikkekMezoadatok,1,1,1,starteditevent );
        mastergrid = $(cikkgridContainer).dxDataGrid('instance');
         komplexinput.loaded();
    }
    function destruct () {
	 	initEvents.forEach(function (ev) {
			$(document).off(ev.event, ev.target);
		});
	}
})();
