var Mobilleltarivgrid = (function () {
    	var initEvents = [
		//Add field
		{
			  event	: "click"
			, target: "#Actclose"
			, fn	:  komplexinput.actclose
		}
	];

	var MobilleltarivgridContainer = '#gridcontainer';
	var MobilleltarivMezoadatok =  komplexgrid.getadatok('t_leltarivpda_1',[
            { dataField: 'livfpda_id', caption: 'kszk_id',visible: false, enable: false},
            { dataField: 'livfpda_rtmbl_nev', caption: 'pnzn_rovidites'},
            { dataField: 'livfpda_cik_id', caption: 'pnzn_rovidites'},
            { dataField: 'livfpda_cik_vonal_azon', caption: 'pnzn_rovidites'},
            { dataField: 'livfpda_cik_kod', caption: 'pnzn_rovidites'},
            { dataField: 'livfpda_cik_nev', caption: 'pnzn_rovidites'},
            { dataField: 'livfpda_cik_meny', caption: 'pnzn_rovidites'},
            { dataField: 'livfpda_cik_meny_szorzo', caption: 'pnzn_rovidites'}

			]);
    		var orders = komplexgrid.getgridorders(MobilleltarivgridContainer,'/mobilleltarivAll');
			var gridDataSource = komplexgrid.setgridDataSource('/mobilleltarivAlls');
     var mastergrid = null;
    return {
        init				: init,
        MobilleltarivMezoadatok	: MobilleltarivMezoadatok,
        mastergrid			: mastergrid
    }

    function init () {
       $("#menusor").hide();
          komplexinput.loading ();
        initEvents.forEach(function(ev){
                $(document).on(ev.event, ev.target, ev.fn);
            });
        komplexgrid.setmastergrid(MobilleltarivgridContainer,orders,MobilleltarivMezoadatok,0,0,0 );
        mastergrid = $(MobilleltarivgridContainer).dxDataGrid('instance');
         komplexinput.loaded();
    }
    function destruct () {
	 	initEvents.forEach(function (ev) {
			$(document).off(ev.event, ev.target);
		});
	}
})();
