var leltarfejekGrid = (function () {
    	var initEvents = [
		//Add field
		{
			  event	: "click"
			, target: "#Actclose"
			, fn	:  komplexinput.actclose
		}
	];

	var leltarfejekgridContainer = '#gridContainer';
	var leltarfejekMezoadatok = komplexgrid.getadatok('t_leltar_fejek_1',[
            { dataField: 'leltf_id', caption: 'leltf_id',allowEditing: false, dataType: 'number', sortOrder: 'desc',visible: false,alignment: 'center', enable: false},
            { dataField: 'leltf_ertekelesi_sorszam', caption: 'leltf_ertekelesi_sorszam', dataType: 'string',alignment: 'center'},
            { dataField: 'leltf_ertekelesi_kod_nev',allowEditing: false, caption: 'leltf_ertekelesi_kod_nev',alignment: 'center'},
			{ dataField: 'leltf_datuma',allowEditing: false, caption: 'leltf_datuma',alignment: 'center'},
			{ dataField: 'leltf_rakt_nev',allowEditing: false,caption: 'leltf_rakt_nev',alignment: 'center'},
			{ dataField: 'leltf_felvetel_eszkoz_kod_nev',allowEditing: false,caption: 'leltf_felvetel_eszkoz_kod_nev',alignment: 'center'},
			{ dataField: 'leltf_teljes_kod_nev',allowEditing: false, caption: 'leltf_teljes_kod_nev',alignment: 'center'},
            { dataField: 'leltf_status_kod_nev',allowEditing: false, caption: 'leltf_status_kod_nev' ,alignment: 'center'},
            { dataField: 'leltf_lezaras_idopontja',allowEditing: false, caption: 'leltf_lezaras_idopontja' ,alignment: 'center',format: 'currency'},
            { dataField: 'leltf_netto',allowEditing: false, caption: 'leltf_netto' ,visoble: false,alignment: 'center',format: 'currency'},
			{ dataField: 'leltf_brutto',allowEditing: false, caption: 'leltf_brutto' ,alignment: 'center',format: 'currency'},
			{ dataField: 'leltf_netto_hiany',allowEditing: false, caption: 'leltf_netto_hiany' ,alignment: 'center',format: 'currency'},
			{ dataField: 'leltf_brutto_hiany',allowEditing: false, caption: 'leltf_brutto_hiany' ,alignment: 'center',format: 'currency'},
			{ dataField: 'leltf_netto_tobblet',allowEditing: false, caption: 'leltf_netto_tobblet' ,alignment: 'center',format: 'currency'},
			{ dataField: 'leltf_brutto_tobblet',allowEditing: false, caption: 'leltf_brutto_tobblet' ,alignment: 'center',format: 'currency'},
			{ dataField: 'leltf_netto_konyszerinti_eladas',allowEditing: false, caption: 'leltf_netto_konyszerinti_eladas' ,alignment: 'center',format: 'currency'},
			{ dataField: 'leltf_brutto_konyszerinti_eladas',allowEditing: false, caption: 'leltf_brutto_konyszerinti_eladas' ,alignment: 'center',format: 'currency'},
			{ dataField: 'leltf_netto_konyszerinti_atlagbeszerzes',allowEditing: false, caption: 'leltf_netto_konyszerinti_atlagbeszerzes' ,alignment: 'center',format: 'currency'},
			{ dataField: 'leltf_brutto_konyszerinti_atlagbeszerzes',allowEditing: false, caption: 'leltf_brutto_konyszerinti_atlagbeszerzes' ,alignment: 'center',format: 'currency'},
			{ dataField: 'leltf_netto_konyszerinti_beszerzes',allowEditing: false, caption: 'leltf_netto_konyszerinti_beszerzes' ,alignment: 'center',format: 'currency'},
			{ dataField: 'leltf_brutto_konyszerinti_beszerzes',allowEditing: false, caption: 'leltf_brutto_konyszerinti_beszerzes' ,alignment: 'center',format: 'currency'},
			{ dataField: 'leltf_letre_felh_nev',allowEditing: false, caption: 'leltf_brutto_konyszerinti_beszerzes' ,alignment: 'center'},
			{ dataField: 'leltf_letre_dat',allowEditing: false, caption: 'leltf_letre_dat' ,alignment: 'center'},
			{ dataField: 'leltf_raktkeszlet_tipusa_kod_nev',allowEditing: false, caption: 'leltf_raktkeszlet_tipusa_kod_nev' ,alignment: 'center'}
			]);
	var leltarfejektetelekMezoadatok = komplexgrid.getadatok('t_leltar_kiertekelesek_1',[
			{ dataField: 'leltk_id', caption: 'leltk_id',alignment: 'center',visible: false, enable: false},
            { dataField: 'leltk_ertekelesi_sorszam', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'leltk_cik_kod', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'leltk_cik_nev', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'leltk_vtsz_szam', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'leltk_afa_nev', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'leltk_me_rovidites', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'leltk_konyv_keszl', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'leltk_ivek', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'leltk_felvett_menny', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'leltk_egyseg_ar', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'leltk_beszerzesi_ar', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'leltk_afa_szorzo', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'leltk_arfolyam', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'leltk_alap_ar', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'leltk_tobblet_menny', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'leltk_hiany_menny', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'leltk_elteres_menny', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'leltk_felvett_elad_ertek', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'leltk_tobblet_elad_ertek', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'leltk_hiany_elad_ertek', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'leltk_elteres_elad_ertek', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'leltk_felvett_beszerzesi_ertek', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'leltk_tobblet_beszerzesi_ertek', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'leltk_hiany_beszerzesi_ertek', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'leltk_elteres_beszerzesi_ertek', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'leltk_felvett_alap_ertek', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'leltk_tobblet_alap_ertek', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'leltk_hiany_alap_ertek', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'leltk_elteres_alap_ertek', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'leltk_elteres_menny_szazalek', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'leltk_elteres_elad_ertek_szazalek', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'leltk_tobblet_menny_szazalek', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'leltk_hiany_menny_szazalek', caption: 'bizf_id',alignment: 'center'},
            { dataField: 'leltk_elteres_beszerzesi_ertek_szazalek', caption: 'bizf_id',alignment: 'center'}
			]);
			var orders = komplexgrid.getgridorders(leltarfejekgridContainer,'/leltarfejekAll');
			var gridDataSource = komplexgrid.setgridDataSource('/leltarfejekAll');
			var mastersummery ={
            totalItems: [{
                column: "leltf_id'",
                summaryType: "count",
				displayFormat: "Tételszám: {0}"
            }]
        };
		var detailsummary= {
							totalItems: [{
								column: "leltk_id",
								summaryType: "count",
								displayFormat: "Tételszám: {0}"
							},{
								 column: "leltk_felvett_elad_ertek",
								summaryType: "sum",
                                valueFormat: "Number",
								displayFormat: "Összesen: {0}"

							}]
						};
    var masterdetailgrid =null;
return {
		  init			: init
		, destruct		: destruct

	}
    function init () {
        $("#menusor").hide();
        komplexinput.loading ();
        initEvents.forEach(function(ev){
                $(document).on(ev.event, ev.target, ev.fn);
            });
        komplexgrid.setmasterdetailgrid(leltarfejekgridContainer,
                                        orders,
                                        leltarfejekMezoadatok,
                                        leltarfejektetelekMezoadatok,
                                        mastersummery,
                                        detailsummary,
                                        0,1,1);
        masterdetailgrid = $(leltarfejekgridContainer).dxDataGrid('instance');

         komplexinput.loaded();
    }
    function destruct () {
	 	initEvents.forEach(function (ev) {
			$(document).off(ev.event, ev.target);
		});
	}
})();
