var szamlalekerdezesGrid = (function () {
    	var initEvents = [
		//Add field
		{
			  event	: "click"
			, target: "#Actclose"
			, fn	:  komplexinput.actclose
		}
	];

	var szamlagridContainer = '#gridContainer';
	var BizonylatMezoadatok = komplexgrid.getadatok('t_biz_fejek_1',[
            { dataField: 'bizf_id', caption: 'bizf_id',allowEditing: false, dataType: 'number', sortOrder: 'desc',visible: false,alignment: 'center', enable: false},
            { dataField: 'bizf_azon', caption: 'Bizonylat szám ', dataType: 'string',alignment: 'center'},
            { dataField: 'bizf_esed_datum',allowEditing: false, caption: 'kelte ',alignment: 'center'},
            { dataField: 'bizf_atvevo_prt_nev',allowEditing: false, caption: 'bizf_atado_prt_nev',alignment: 'center'}, //sortOrder: 'asc', sortIndex: 0},
			{ dataField: 'bizf_fizmod_kod_nev',allowEditing: false, caption: 'bizf_fizmod_kod_nev ',alignment: 'center'},
			{ dataField: 'bizf_telj_datum',allowEditing: false, caption: 'bizf_telj_datum ',alignment: 'center'},
			{ dataField: 'bizf_kif_datuma',allowEditing: false, caption: 'bizf_kif_datuma ',alignment: 'center'},
			{ dataField: 'bizf_kifizetes_datuma',allowEditing: false, caption: 'bizf_kifizetes_datuma ',alignment: 'center'},
            { dataField: 'bizf_netto_ert',allowEditing: false, caption: 'bizf_netto_ert' ,alignment: 'center',format: 'currency'},
            { dataField: 'bizf_fizetendo_brutto_ert',allowEditing: false, caption: 'bizf_fizetendo_brutto_ert' ,alignment: 'center',format: 'currency'},
            { dataField: 'bizf_afa_ertek',allowEditing: false, caption: 'bizf_afa_ertek' ,alignment: 'center',format: 'currency'},
            { dataField: 'bizf_maradekosszeg',allowEditing: false, caption: 'bizf_maradekosszeg,' ,visoble: false,alignment: 'center',format: 'currency'},
			{ dataField: 'bizf_penznem_nev',allowEditing: false, caption: 'bizf_penznem_nev' ,alignment: 'center',format: 'currency'},
			{ dataField: 'bizf_feldolg_status_kod',allowEditing: false,visible: false, caption: 'bizf_feldolg_status_kod' ,alignment: 'center'},
			{ dataField: 'bizf_sztorno_bizf_id',allowEditing: false,visible: false, caption: 'bizf_sztorno_bizf_id' ,alignment: 'center'},
			{ dataField: 'bizf_sztornozott_bizf_id',allowEditing: false,visible: false, caption: 'bizf_sztornozott_bizf_id' ,alignment: 'center'},
			{ dataField: 'bizf_eredeti_kinyomtatva',allowEditing: false,visible: false, caption: 'bizf_eredeti_kinyomtatva' ,alignment: 'center'},
			{ dataField: 'bizf_maradekosszeg',allowEditing: false,visible: false, caption: 'bizf_maradekosszeg' ,alignment: 'center'},
			{ dataField: 'bizf_beszerzesi_maradekosszeg',allowEditing: false, visible: false, caption: 'bizf_beszerzesi_maradekosszeg' ,alignment: 'center'},
			{ dataField: 'bizf_azon_biztip_kod',allowEditing: false,visible: false, caption: 'bizf_azon_biztip_kod' ,alignment: 'center'},
			{ dataField: 'bizf_ukoto_prt_nev',allowEditing: false, caption: 'bizf_ukoto_prt_nev' ,alignment: 'center'}
			]);
	var BizonylattetelMezoadatok = komplexgrid.getadatok('t_biz_tetelek_1',[
			{ dataField: 'bizt_id', caption: 'biz1_id',alignment: 'center',visible: false, enable: false},
			{ dataField: 'bizt_cik_id', caption: 'bizf_id',alignment: 'center', enable: false},
			{ dataField: 'bizt_cik_nev', caption: 'bizf_id',alignment: 'center', enable: false},
			{ dataField: 'bizt_cik_kod', caption: 'bizt_cik_kod',alignment: 'center', enable: false},
            { dataField: 'bizt_menny', caption: 'bizt_menny ',alignment: 'center'},
            { dataField: 'bizt_me_rovidites', caption: 'bizt_me_rovidites',alignment: 'center'},
            { dataField: 'bizt_nyilv_ar', caption: 'bizt_nyilv_ar',alignment: 'center'},
            { dataField: 'bizt_brutto_nyilv_ar', caption: 'bizt_brutto_nyilv_ar',alignment: 'center'},
            { dataField: 'bizt_elad_ar', caption: 'bizt_elad_ar',alignment: 'center'},
            { dataField: 'bizt_brutto_elad_ar', caption: 'bizt_brutto_elad_ar',alignment: 'center'}

			]);
			var orders = komplexgrid.getgridorders(szamlagridContainer,'/allszamla');
			var gridDataSource = komplexgrid.setgridDataSource('/allszamla');
			var mastersummery ={
            totalItems: [{
                column: "bizf_id",
                summaryType: "count",
				displayFormat: "Tételszám: {0}"
            }, {
                column: "bizf_netto_ert",
                summaryType: "sum",
				displayFormat: "Összesen: {0}"

            },{
                column: "bizf_fizetendo_brutto_ert",
                summaryType: "sum",
				displayFormat: "Összesen: {0}"

            }]
        };
		var detailsummary= {
							totalItems: [{
								column: "bizt_id",
								summaryType: "count",
								displayFormat: "Tételszám: {0}"
							}, {
								column: "bizt_elad_ar",
								summaryType: "sum",
                                valueFormat: "Number",
								displayFormat: "Összesen: {0}"

							},{
								column: "bizt_brutto_elad_ar",
								summaryType: "sum",
                                valueFormat: "Number",
								displayFormat: "Összesen: {0}"

							}]
						};
    var masterdetailgrid =null;
return {
		  init			: init
		, destruct		: destruct

	}
    function init () {
        $("#menusor").hide();
        komplexinput.loading ();
        initEvents.forEach(function(ev){
                $(document).on(ev.event, ev.target, ev.fn);
            });
        var starteditevent=function(e) {
            if (e.data) {
                if (e.data.bizf_feldolg_status_kod=='1') {
                    e.cancel = true;
                } else {
                   // DevExpress.ui.notify("EditingStart-->"+e.data.bizf_id, "info", 2000);
                    window.location.href = '/frmInputConBizonylatedit/'+$("#menu_id").val()+'/'+e.data.bizf_id;
                    //logEvent("EditingStart");
                }
            }

        };

        var onRowPrepared = function (info) {
            if (info.data) {
                ///console.log(info.data.bizf_sztorno_bizf_id);

                if (info.data.bizf_sztornozott_bizf_id != null) {
                    info.rowElement.addClass("clsztornobizonylat");
                } else if (info.data.bizf_sztorno_bizf_id != null) {
                    info.rowElement.addClass("clsztornobizonylat");
                } else if (info.data.bizf_maradekosszeg>0 && komplexinput.in_array(info.data.bizf_azon_biztip_kod,['S']) && info.data.bizf_feldolg_status_kod=='1') {
                        info.rowElement.addClass("clkinemfizetettszamla");
                } else if (info.data.bizf_beszerzesi_maradekosszeg>0 && komplexinput.in_array(info.data.bizf_azon_biztip_kod,['B'])  && info.data.bizf_feldolg_status_kod=='1') {
                        info.rowElement.addClass("clkinemfizetettszamla");
                } else if (info.data.bizf_feldolg_status_kod=='0') {
                        info.rowElement.addClass("lezaratlan");
                } else if (info.data.bizf_eredeti_kinyomtatva=='N') {
                        info.rowElement.addClass("clNincseredetikinyomtatva");
                }


            }

        };
     var deleted=function(e) {
            if (e.data.bizf_feldolg_status_kod=='1') {
                e.cancel = true;
            }else {
                var inputbizf = {
                                 'bizf_id'	: e.data.bizf_id
                                };
                 komplexinput.post('/bizfejdeleted', inputbizf, function(result){
                   if (masterdetailgrid) {
                        masterdetailgrid.refresh();
                   }
                });
            }
     };
     var inserted=function(e) {
			//DevExpress.ui.notify("start edit"+e.data.bizt_id, "info", 2000);
			//window.location.href = '/Inputcikkedit/'+e.data.cik_id;
        };
        var selected=function(e) {
			//DevExpress.ui.notify("start edit"+e.data.bizt_id, "info", 2000);
			//window.location.href = '/Inputcikkedit/'+e.data.cik_id;
        };	         masterdetailgrid=komplexgrid.setmasterdetailgrid(szamlagridContainer,orders,BizonylatMezoadatok,BizonylattetelMezoadatok,mastersummery,detailsummary,0,1,1,starteditevent);
        masterdetailgrid = $(szamlagridContainer).dxDataGrid('instance');
        masterdetailgrid.option('onRowPrepared',onRowPrepared);
        masterdetailgrid.option('onRowRemoving',deleted);
         komplexinput.loaded();
    }
    function destruct () {
	 	initEvents.forEach(function (ev) {
			$(document).off(ev.event, ev.target);
		});
	}
})();
