function getColumnFieldName(dataGridInstance, getter) {
    var column,
        i;

    if($.isFunction(getter)) {
        for(i = 0;i < dataGridInstance.columnCount() ;i++) {
            column = dataGridInstance.columnOption(i);
            if(column.selector === getter) {
                return column.dataField;
            }
        }
    }
    else {
        return getter;
    }
};

function getProcessFilter() {
    var dataGrid = $("#gridContainer").dxDataGrid("instance");
    var filter = dataGrid.getCombinedFilter();
    var gridFilterText = '';

    function processFilter(dataGridInstance, filter) {
        if (!($.isArray(filter))) {
            gridFilterText = gridFilterText + filter;
        }
        else {
            gridFilterText = gridFilterText + '(';
        }
        if ($.isArray(filter)) {
            if ($.isFunction(filter[0])) {
                filter[0] = getColumnFieldName(dataGridInstance, filter[0]);
            }
            else {
                for (var i = 0; i < filter.length; i++) {
                    processFilter(dataGridInstance, filter[i]);
                    }
                }
            }
        if (($.isArray(filter))) {
            gridFilterText = gridFilterText + ')';
        }
    }

    processFilter(dataGrid, filter);

    return gridFilterText;
}

$(function(){
    var orders = new DevExpress.data.CustomStore({
        load: function (loadOptions) {
            var deferred = $.Deferred(),
                args = {};

            var dataGrid = $("#gridContainer").dxDataGrid("instance");
            var filter = dataGrid.getCombinedFilter();
            var j = 0;

            //processFilter(dataGrid, filter, fText);

            console.log(getProcessFilter());

            var filterOptions = loadOptions.filter ? loadOptions.filter.join(",") : "";
            var sortOptions = loadOptions.sort ? JSON.stringify(loadOptions.sort) : "";  //Getting sort options

            if (filterOptions != null) {
               //alert(filterOptions);
            };

            if (loadOptions.sort) {
                var sortOptions = loadOptions.sort; // ? JSON.stringify(loadOptions.sort) : "";

                var orderby = '';
                if (sortOptions != null) {

                    for (var i = 0; i < sortOptions.length; i++) {
                        var obj = sortOptions[i];
                        var order = '';
                        console.log(obj);

                        order = obj.selector;
                        if (obj.desc)
                            order = order + ' desc';

                        if (orderby.length > 0)
                            orderby = orderby + ', ';
                        orderby = orderby + order;
                    }
                    ;
                    console.log(orderby);

                    args.orderby = orderby;
                }
                ;
            }

            args.skip = loadOptions.skip || 0;
            args.take = loadOptions.take || 24;

            $.getJSON('/allfelhasznalo', {
                args : args,
                // ...
            }).done(function(result) {
                deferred.resolve(result.items, {
                    totalCount: result.totalCount
                });
            }).fail(deferred.reject);
            return deferred.promise();
        }
    });

    var gridDataSource = {
        load: function (loadOptions) {
            var d = $.Deferred();
            $.getJSON('/allfelhasznalo', {
                groupConfig: loadOptions.group,
                // ...
            }).done(function(result) {
                d.resolve(result.items, {
                    totalCount: result.totalCount
                });
            }).fail(d.reject);
            return d.promise();
        }
    };

    $("#gridContainer").dxDataGrid({
        dataSource: {store: orders},
        //dataSource: gridDataSource,
        remoteOperations: {
            filtering: true,
            sorting: true,
            paging: true
        },
        paging: {
            pageSize: 24
        },
        height: 700,
        pager: {
            //showPageSizeSelector: true,
            //allowedPageSizes: [8, 12, 20]
        },
        sorting: { mode: 'multiple' },
        filterRow: {
            visible: true,
            applyFilter: "auto"
        },
        headerFilter: {
            visible: true
        },
        selection: {
            mode: 'single'
        },
        columns: [{
            allowFiltering: false,
            filterType: 'exclude'
        },

            { dataField: 'felh_userid', caption: 'felh_userid', enable: false},
            { dataField: 'felh_felhnev', caption: 'Falhasználó név'},
            { dataField: 'felh_prt_nev', caption: 'Felhasznaló neve'}, //sortOrder: 'asc', sortIndex: 0},
            { dataField: 'felh_users_email', caption: 'email'}
        ],
        columnAutoWidth: true,
        hoverStateEnabled: true,
        editing: {
            mode: "form",
            form: {
                maxColWidth: 170,
                colCount: 5,
                hoverStateEnable: true,
                items: [{
                    itemType: "group",
                    caption: "Megnevezés",
                    items: ["felh_userid", "felh_prt_nev"]
                }, {
                    itemType: "group",
                    caption: "Cím",
                        items: [{
                            itemType: "group",
                            caption: "Helység",
                            items: ["helys_irsz", "helys_nev"]
                        },{
                        dataField: "prt_utca"
                    }]
                    }],
                onFieldDataChanged: function () {
                    DevExpress.ui.notify("Az adatok megváltoztak", "info", 2000);
                }
            },
            allowAdding: true,
            allowDeleting: true,
            allowUpdating: true,
            height: 540,
            colCount: 3,
            texts: {
                addRow: "Új partner",
                cancelRowChanges: "Mégse",
                confirmDeleteMessage: "Biztos, hogy törli az adatot?",
                confirmDeleteTitle: "Törli?",
                deleteRow: "Töröl",
                editRow: "Szerkeszt",
                saveRowChanges: "Mentés"
            }
        }
    }).dxDataGrid("instance");
});
